<?php

$options['context-path'] = array (
  'site' => 'sites/metastasis-research.org/drushrc.php',
);
$options['db_type'] = 'mysql';
$options['db_host'] = 'vs27';
$options['db_passwd'] = '5ycd9DJyCA';
$options['db_name'] = 'site_3180';
$options['db_user'] = 'site_3180';
$options['installed'] = true;
$options['aliases'] = 'metastasis--research-org.mcnutility.com,www.metastasis-research.org';
$options['site_url'] = 'metastasis-research.org';
$options['site_id'] = '3180';
$options['client_email'] = 'joe@musiccitynetworks.com';
$options['packages'] = array (
  'platforms' => 
  array (
    'drupal' => 
    array (
      'short_name' => 'drupal',
      'version' => '7.23',
      'description' => 'This platform is running Drupal 7.23',
    ),
  ),
  'profiles' => 
  array (
    'base_profile' => 
    array (
      'name' => 'base_profile',
      'filename' => '/data/aegir/platforms/base-130814/profiles/base_profile/base_profile.profile',
      'project' => '',
      'info' => 
      array (
        'version' => NULL,
        'languages' => 
        array (
          0 => 'en',
        ),
      ),
      'version' => '7.23',
      'status' => 1,
    ),
  ),
  'modules' => 
  array (
    'aaa_update_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/update/tests/aaa_update_test.module',
      'name' => 'aaa_update_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'AAA Update test',
        'description' => 'Support module for update module testing.',
        'package' => 'Testing',
        'core' => '7.x',
        'hidden' => true,
        'version' => '7.23',
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'actions_loop_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/actions_loop_test.module',
      'name' => 'actions_loop_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Actions loop test',
        'description' => 'Support module for action loop testing.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'actions_permissions' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/views_bulk_operations/actions_permissions.module',
      'name' => 'actions_permissions',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Actions permissions (VBO)',
        'description' => 'Provides permission-based access control for actions. Used by Views Bulk Operations.',
        'package' => 'Administration',
        'core' => '7.x',
        'version' => '7.x-3.1',
        'project' => 'views_bulk_operations',
        'datestamp' => '1354500015',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'views_bulk_operations',
      'version' => '7.x-3.1',
    ),
    'admin_devel' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/admin_menu/admin_devel/admin_devel.module',
      'name' => 'admin_devel',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Administration Development tools',
        'description' => 'Administration and debugging functionality for developers and site builders.',
        'package' => 'Administration',
        'core' => '7.x',
        'scripts' => 
        array (
          0 => 'admin_devel.js',
        ),
        'version' => '7.x-3.0-rc4',
        'project' => 'admin_menu',
        'datestamp' => '1359651687',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'admin_menu',
      'version' => '7.x-3.0-rc4',
    ),
    'admin_menu' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/admin_menu/admin_menu.module',
      'name' => 'admin_menu',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7304',
      'weight' => '100',
      'info' => 
      array (
        'name' => 'Administration menu',
        'description' => 'Provides a dropdown menu to most administrative tasks and other common destinations (to users with the proper permissions).',
        'package' => 'Administration',
        'core' => '7.x',
        'configure' => 'admin/config/administration/admin_menu',
        'dependencies' => 
        array (
          0 => 'system (>7.10)',
        ),
        'files' => 
        array (
          0 => 'tests/admin_menu.test',
        ),
        'version' => '7.x-3.0-rc4',
        'project' => 'admin_menu',
        'datestamp' => '1359651687',
        'php' => '5.2.4',
      ),
      'project' => 'admin_menu',
      'version' => '7.x-3.0-rc4',
    ),
    'admin_menu_toolbar' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/admin_menu/admin_menu_toolbar/admin_menu_toolbar.module',
      'name' => 'admin_menu_toolbar',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '6300',
      'weight' => '101',
      'info' => 
      array (
        'name' => 'Administration menu Toolbar style',
        'description' => 'A better Toolbar.',
        'package' => 'Administration',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'admin_menu',
        ),
        'version' => '7.x-3.0-rc4',
        'project' => 'admin_menu',
        'datestamp' => '1359651687',
        'php' => '5.2.4',
      ),
      'project' => 'admin_menu',
      'version' => '7.x-3.0-rc4',
    ),
    'advertisement_banners' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/features/advertisement_banners/advertisement_banners.module',
      'name' => 'advertisement_banners',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Advertisement Banners',
        'description' => 'Installs Banner Content Type, Sidebar Top and Sidebar Bottom Banner Blocks',
        'core' => '7.x',
        'package' => 'Base MCN Features',
        'php' => '5.2.4',
        'version' => '7.x-1.4',
        'project' => 'advertisement_banners',
        'dependencies' => 
        array (
          0 => 'context',
          1 => 'ctools',
          2 => 'features',
          3 => 'field_group',
          4 => 'image',
          5 => 'link',
          6 => 'list',
          7 => 'mcn_base_feature',
          8 => 'media',
          9 => 'node',
          10 => 'options',
          11 => 'strongarm',
          12 => 'taxonomy',
          13 => 'views',
        ),
        'features' => 
        array (
          'context' => 
          array (
            0 => 'banner_blocks',
          ),
          'ctools' => 
          array (
            0 => 'context:context:3',
            1 => 'field_group:field_group:1',
            2 => 'strongarm:strongarm:1',
            3 => 'views:views_default:3.0',
          ),
          'features_api' => 
          array (
            0 => 'api:2',
          ),
          'field_base' => 
          array (
            0 => 'field_banner_image',
            1 => 'field_banner_link',
            2 => 'field_banner_space',
            3 => 'field_target',
          ),
          'field_group' => 
          array (
            0 => 'group_banner_link|node|banner|form',
          ),
          'field_instance' => 
          array (
            0 => 'node-banner-field_banner_image',
            1 => 'node-banner-field_banner_link',
            2 => 'node-banner-field_banner_space',
            3 => 'node-banner-field_target',
          ),
          'node' => 
          array (
            0 => 'banner',
          ),
          'taxonomy' => 
          array (
            0 => 'banner_space',
          ),
          'user_permission' => 
          array (
            0 => 'create banner content',
            1 => 'delete any banner content',
            2 => 'delete own banner content',
            3 => 'edit any banner content',
            4 => 'edit own banner content',
          ),
          'variable' => 
          array (
            0 => 'comment_anonymous_banner',
            1 => 'comment_banner',
            2 => 'comment_default_mode_banner',
            3 => 'comment_default_per_page_banner',
            4 => 'comment_form_location_banner',
            5 => 'comment_preview_banner',
            6 => 'comment_subject_field_banner',
            7 => 'menu_options_banner',
            8 => 'menu_parent_banner',
            9 => 'node_options_banner',
            10 => 'node_preview_banner',
            11 => 'node_submitted_banner',
            12 => 'scheduler_publish_enable_banner',
            13 => 'scheduler_publish_touch_banner',
            14 => 'scheduler_unpublish_enable_banner',
          ),
          'views_view' => 
          array (
            0 => 'banners',
          ),
        ),
        'project status url' => 'http://features.devmcn.com/fserver',
      ),
      'project' => 'advertisement_banners',
      'version' => '7.x-1.4',
    ),
    'aggregator' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/aggregator/aggregator.module',
      'name' => 'aggregator',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Aggregator',
        'description' => 'Aggregates syndicated content (RSS, RDF, and Atom feeds).',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'aggregator.test',
        ),
        'configure' => 'admin/config/services/aggregator/settings',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'aggregator.css',
          ),
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'aggregator_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/aggregator/tests/aggregator_test.module',
      'name' => 'aggregator_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Aggregator module tests',
        'description' => 'Support module for aggregator related testing.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'ajax_forms_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/ajax_forms_test.module',
      'name' => 'ajax_forms_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'AJAX form test mock module',
        'description' => 'Test for AJAX form calls.',
        'core' => '7.x',
        'package' => 'Testing',
        'version' => '7.23',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'ajax_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/ajax_test.module',
      'name' => 'ajax_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'AJAX Test',
        'description' => 'Support module for AJAX framework tests.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'auto_nodetitle' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/auto_nodetitle/auto_nodetitle.module',
      'name' => 'auto_nodetitle',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Automatic Nodetitles',
        'description' => 'Allows hiding of the content title field and automatic title creation.',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'auto_nodetitle.install',
          1 => 'auto_nodetitle.module',
          2 => 'auto_nodetitle.js',
        ),
        'version' => '7.x-1.0',
        'project' => 'auto_nodetitle',
        'datestamp' => '1307449915',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'auto_nodetitle',
      'version' => '7.x-1.0',
    ),
    'backup_migrate' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/backup_migrate/backup_migrate.module',
      'name' => 'backup_migrate',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Backup and Migrate',
        'description' => 'Backup or migrate the Drupal Database quickly and without unnecessary data.',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'backup_migrate.module',
          1 => 'backup_migrate.install',
          2 => 'includes/destinations.inc',
          3 => 'includes/profiles.inc',
          4 => 'includes/schedules.inc',
        ),
        'configure' => 'admin/config/system/backup_migrate',
        'version' => '7.x-2.7',
        'project' => 'backup_migrate',
        'datestamp' => '1368729920',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'backup_migrate',
      'version' => '7.x-2.7',
    ),
    'base_profile' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/profiles/base_profile/base_profile.profile',
      'name' => 'base_profile',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '1000',
      'info' => 
      array (
        'name' => 'Base Profile',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'base_profile.install',
          1 => 'base_profile.make',
          2 => 'base_profile.profile',
        ),
        'dependencies' => 
        array (
          0 => 'block',
          1 => 'contextual',
          2 => 'dblog',
          3 => 'field_ui',
          4 => 'file',
          5 => 'help',
          6 => 'image',
          7 => 'list',
          8 => 'menu',
          9 => 'number',
          10 => 'options',
          11 => 'overlay',
          12 => 'path',
          13 => 'rdf',
          14 => 'shortcut',
          15 => 'taxonomy',
          16 => 'toolbar',
          17 => 'ctools',
          18 => 'features',
          19 => 'strongarm',
          20 => 'mcn_base_feature',
          21 => 'mcn_wysiwyg',
          22 => 'basic_page',
        ),
        'variables' => 
        array (
          'admin_theme' => 'seven',
          'block_cache' => '1',
          'configurable_timezones' => '0',
          'date_default_timezone' => 'America/Chicago',
          'date_first_day' => '0',
          'error_level' => '0',
          'node_admin_theme' => '1',
          'page_compression' => '0',
          'pathauto_node_pattern' => '[node:title]',
          'site_default_country' => 'US',
          'site_mail' => 'noreply@mcninteractive.com',
          'update_check_frequency' => '7',
          'update_notification_threshold' => 'security',
          'update_notify_emails' => 
          array (
            0 => 'sysadmin@mcninteractive.com',
          ),
        ),
        'users' => 
        array (
          'joe' => 
          array (
            'uid' => '2',
            'name' => 'joe',
            'mail' => 'joe.stewart@mcninteractive.com',
            'roles' => 'developer,administrator',
            'status' => '1',
          ),
          'chris' => 
          array (
            'uid' => '3',
            'name' => 'chris',
            'mail' => 'christopher.smith@mcninteractive.com',
            'roles' => 'administrator',
            'status' => '1',
          ),
          'martin' => 
          array (
            'uid' => '4',
            'name' => 'martin',
            'mail' => 'martin.rote@mcninteractive.com',
            'roles' => 'developer',
            'status' => '1',
          ),
          'stephen' => 
          array (
            'uid' => '5',
            'name' => 'stephen',
            'mail' => 'stephen.smith@mcninteractive.com',
            'roles' => 'developer',
            'status' => '1',
          ),
          'mike' => 
          array (
            'uid' => '6',
            'name' => 'mike',
            'mail' => 'mike.mclean@mcninteractive.com',
            'roles' => 'developer',
            'status' => '1',
          ),
          'dreea' => 
          array (
            'uid' => '7',
            'name' => 'dreea',
            'mail' => 'dreea.stancu@mcninteractive.com',
            'roles' => 'developer',
            'status' => '1',
          ),
          'Jeff Frazier' => 
          array (
            'uid' => '8',
            'name' => 'Jeff Frazier',
            'mail' => 'jeff.frazier@mcninteractive.com',
            'roles' => 'developer',
            'status' => '1',
          ),
          'brittany' => 
          array (
            'uid' => '9',
            'name' => 'brittany',
            'mail' => 'brittany.easterling@mcninteractive.com',
            'roles' => 'developer',
            'status' => '1',
          ),
          'tommy' => 
          array (
            'uid' => '10',
            'name' => 'tommy',
            'mail' => 'tommy.stalknecht@mcninteractive.com',
            'roles' => 'account manager',
            'status' => '1',
          ),
          'hagler' => 
          array (
            'uid' => '11',
            'name' => 'hagler',
            'mail' => 'mike.hagler@mcninteractive.com',
            'roles' => 'account manager',
            'status' => '1',
          ),
          'jaymie' => 
          array (
            'uid' => '12',
            'name' => 'jaymie',
            'mail' => 'jaymie.guillory@mcninteractive.com',
            'roles' => 'account manager',
            'status' => '1',
          ),
          'intern' => 
          array (
            'uid' => '13',
            'name' => 'intern',
            'mail' => 'intern@mcninteractive.com',
            'roles' => 'client',
            'status' => '1',
          ),
        ),
        'nodes' => 
        array (
          'front' => 
          array (
            'type' => 'page',
            'title' => 'Home',
            'uid' => '1',
          ),
        ),
        'description' => '',
        'version' => NULL,
        'php' => '5.2.4',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'basic_page' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/features/basic_page/basic_page.module',
      'name' => 'basic_page',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Basic Page',
        'description' => 'Adds the Basic Page functionality and permissions',
        'core' => '7.x',
        'package' => 'Base MCN Features',
        'php' => '5.2.4',
        'version' => '7.x-1.1',
        'project' => 'basic_page',
        'dependencies' => 
        array (
          0 => 'features',
          1 => 'mcn_base_feature',
          2 => 'mcn_wysiwyg',
          3 => 'node',
          4 => 'strongarm',
          5 => 'text',
        ),
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'strongarm:strongarm:1',
          ),
          'features_api' => 
          array (
            0 => 'api:2',
          ),
          'field_base' => 
          array (
            0 => 'body',
          ),
          'field_instance' => 
          array (
            0 => 'node-page-body',
          ),
          'node' => 
          array (
            0 => 'page',
          ),
          'user_permission' => 
          array (
            0 => 'create page content',
            1 => 'delete any page content',
            2 => 'delete own page content',
            3 => 'edit any page content',
            4 => 'edit own page content',
          ),
          'variable' => 
          array (
            0 => 'comment_page',
            1 => 'node_options_page',
            2 => 'node_submitted_page',
          ),
        ),
        'project path' => '/tmp',
        'project status url' => 'http://features.devmcn.com/fserver',
      ),
      'project' => 'basic_page',
      'version' => '7.x-1.1',
    ),
    'batch_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/batch_test.module',
      'name' => 'batch_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Batch API test',
        'description' => 'Support module for Batch API tests.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'bbb_update_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/update/tests/bbb_update_test.module',
      'name' => 'bbb_update_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'BBB Update test',
        'description' => 'Support module for update module testing.',
        'package' => 'Testing',
        'core' => '7.x',
        'hidden' => true,
        'version' => '7.23',
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'better_exposed_filters' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/better_exposed_filters/better_exposed_filters.module',
      'name' => 'better_exposed_filters',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Better Exposed Filters',
        'description' => 'Allow the use of checkboxes or radio buttons for exposed Views filters',
        'core' => '7.x',
        'package' => 'Views',
        'dependencies' => 
        array (
          0 => 'views',
        ),
        'files' => 
        array (
          0 => 'better_exposed_filters.module',
          1 => 'better_exposed_filters.views.inc',
          2 => 'better_exposed_filters_exposed_form_plugin.inc',
          3 => 'better_exposed_filters.theme',
        ),
        'version' => '7.x-3.0-beta3',
        'project' => 'better_exposed_filters',
        'datestamp' => '1349281866',
        'php' => '5.2.4',
      ),
      'project' => 'better_exposed_filters',
      'version' => '7.x-3.0-beta3',
    ),
    'better_formats' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/better_formats/better_formats.module',
      'name' => 'better_formats',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '100',
      'info' => 
      array (
        'name' => 'Better Formats',
        'description' => 'Enhances the core input format system by managing input format defaults and settings.',
        'core' => '7.x',
        'configure' => 'admin/config/content/formats',
        'version' => '7.x-1.0-beta1',
        'project' => 'better_formats',
        'datestamp' => '1343262404',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'better_formats',
      'version' => '7.x-1.0-beta1',
    ),
    'block' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/block/block.module',
      'name' => 'block',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7008',
      'weight' => '-5',
      'info' => 
      array (
        'name' => 'Block',
        'description' => 'Controls the visual building blocks a page is constructed with. Blocks are boxes of content rendered into an area, or region, of a web page.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'block.test',
        ),
        'configure' => 'admin/structure/block',
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'block_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/block/tests/block_test.module',
      'name' => 'block_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Block test',
        'description' => 'Provides test blocks.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'blog' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/blog/blog.module',
      'name' => 'blog',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Blog',
        'description' => 'Enables multi-user blogs.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'blog.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'book' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/book/book.module',
      'name' => 'book',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Book',
        'description' => 'Allows users to create and organize related content in an outline.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'book.test',
        ),
        'configure' => 'admin/content/book/settings',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'book.css',
          ),
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'boxes' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/boxes/boxes.module',
      'name' => 'boxes',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Boxes',
        'description' => 'Provides exports for custom blocks and spaces integration.',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'ctools',
          1 => 'block',
        ),
        'files' => 
        array (
          0 => 'boxes.module',
          1 => 'boxes.admin.inc',
          2 => 'plugins/boxes_box.inc',
          3 => 'plugins/boxes_simple.inc',
          4 => 'plugins/spaces_controller_boxes.inc',
          5 => 'tests/boxes.test',
        ),
        'version' => '7.x-1.1',
        'project' => 'boxes',
        'datestamp' => '1358977511',
        'php' => '5.2.4',
      ),
      'project' => 'boxes',
      'version' => '7.x-1.1',
    ),
    'bulk_export' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib-patched/ctools/bulk_export/bulk_export.module',
      'name' => 'bulk_export',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Bulk Export',
        'description' => 'Performs bulk exporting of data objects known about by Chaos tools.',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'package' => 'Chaos tool suite',
        'version' => '7.x-1.3',
        'project' => 'ctools',
        'datestamp' => '1365013512',
        'php' => '5.2.4',
      ),
      'project' => 'ctools',
      'version' => '7.x-1.3',
    ),
    'bulk_media_upload' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/bulk_media_upload/bulk_media_upload.module',
      'name' => 'bulk_media_upload',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Bulk Media Upload',
        'core' => '7.x',
        'package' => 'Media',
        'description' => 'With Bulk Media Upload you can upload a bunch of media files and a new node will be created for every file.',
        'dependencies' => 
        array (
          0 => 'entity',
          1 => 'plupload',
          2 => 'token',
        ),
        'version' => '7.x-1.x-dev',
        'project' => 'bulk_media_upload',
        'datestamp' => '1345507686',
        'php' => '5.2.4',
      ),
      'project' => 'bulk_media_upload',
      'version' => '7.x-1.x-dev',
    ),
    'calendar' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/calendar/calendar.module',
      'name' => 'calendar',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Calendar',
        'description' => 'Views plugin to display views containing dates as Calendars.',
        'dependencies' => 
        array (
          0 => 'views',
          1 => 'date_api',
          2 => 'date_views',
        ),
        'package' => 'Date/Time',
        'core' => '7.x',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'css/calendar_multiday.css',
          ),
        ),
        'files' => 
        array (
          0 => 'calendar.install',
          1 => 'calendar.module',
          2 => 'includes/calendar.views.inc',
          3 => 'includes/calendar_plugin_style.inc',
          4 => 'includes/calendar_plugin_row.inc',
          5 => 'includes/calendar.views_template.inc',
          6 => 'theme/theme.inc',
          7 => 'theme/calendar-style.tpl.php',
        ),
        'version' => '7.x-3.4',
        'project' => 'calendar',
        'datestamp' => '1337429753',
        'php' => '5.2.4',
      ),
      'project' => 'calendar',
      'version' => '7.x-3.4',
    ),
    'captcha' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/captcha/captcha.module',
      'name' => 'captcha',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'CAPTCHA',
        'description' => 'Base CAPTCHA module for adding challenges to arbitrary forms.',
        'package' => 'Spam control',
        'core' => '7.x',
        'configure' => 'admin/config/people/captcha',
        'files' => 
        array (
          0 => 'captcha.module',
          1 => 'captcha.inc',
          2 => 'captcha.admin.inc',
          3 => 'captcha.install',
          4 => 'captcha.test',
        ),
        'version' => '7.x-1.0-beta2',
        'project' => 'captcha',
        'datestamp' => '1325504137',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'captcha',
      'version' => '7.x-1.0-beta2',
    ),
    'ccc_update_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/update/tests/ccc_update_test.module',
      'name' => 'ccc_update_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'CCC Update test',
        'description' => 'Support module for update module testing.',
        'package' => 'Testing',
        'core' => '7.x',
        'hidden' => true,
        'version' => '7.23',
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'ckeditor_link' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/ckeditor_link/ckeditor_link.module',
      'name' => 'ckeditor_link',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '1',
      'info' => 
      array (
        'name' => 'CKEditor Link',
        'description' => 'Easily create links to Drupal internal paths through CKEditor.',
        'package' => 'User interface',
        'core' => '7.x',
        'configure' => 'admin/config/content/ckeditor_link',
        'version' => '7.x-2.3',
        'project' => 'ckeditor_link',
        'datestamp' => '1358413212',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'ckeditor_link',
      'version' => '7.x-2.3',
    ),
    'color' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/color/color.module',
      'name' => 'color',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Color',
        'description' => 'Allows administrators to change the color scheme of compatible themes.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'color.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'colorbox' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/colorbox/colorbox.module',
      'name' => 'colorbox',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7202',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Colorbox',
        'description' => 'A light-weight, customizable lightbox plugin for jQuery 1.4.3+.',
        'dependencies' => 
        array (
          0 => 'libraries (2.x)',
        ),
        'core' => '7.x',
        'configure' => 'admin/config/media/colorbox',
        'files' => 
        array (
          0 => 'views/colorbox_handler_field_colorbox.inc',
        ),
        'version' => '7.x-2.4',
        'project' => 'colorbox',
        'datestamp' => '1365452412',
        'php' => '5.2.4',
      ),
      'project' => 'colorbox',
      'version' => '7.x-2.4',
    ),
    'comment' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/comment/comment.module',
      'name' => 'comment',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Comment',
        'description' => 'Allows users to comment on and discuss published content.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'text',
        ),
        'files' => 
        array (
          0 => 'comment.module',
          1 => 'comment.test',
        ),
        'configure' => 'admin/content/comment',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'comment.css',
          ),
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'comments' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/features/comments/comments.module',
      'name' => 'comments',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Comments',
        'description' => 'Add Commenting functionality to a site.',
        'core' => '7.x',
        'package' => 'Base MCN Features',
        'php' => '5.2.4',
        'version' => '7.x-1.0-beta2',
        'project' => 'comments',
        'dependencies' => 
        array (
          0 => 'captcha',
          1 => 'comment',
          2 => 'comment_notify',
          3 => 'features',
          4 => 'recaptcha',
          5 => 'strongarm',
        ),
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'strongarm:strongarm:1',
          ),
          'features_api' => 
          array (
            0 => 'api:2',
          ),
          'user_permission' => 
          array (
            0 => 'access comments',
            1 => 'administer CAPTCHA settings',
            2 => 'administer comment notify',
            3 => 'administer comments',
            4 => 'administer recaptcha',
            5 => 'edit own comments',
            6 => 'post comments',
            7 => 'skip CAPTCHA',
            8 => 'skip comment approval',
            9 => 'subscribe to comments',
          ),
          'variable' => 
          array (
            0 => 'captcha_default_challenge',
            1 => 'captcha_persistence',
            2 => 'recaptcha_private_key',
            3 => 'recaptcha_public_key',
          ),
        ),
        'project status url' => 'http://features.devmcn.com/fserver',
      ),
      'project' => 'comments',
      'version' => '7.x-1.0-beta2',
    ),
    'comment_notify' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/comment_notify/comment_notify.module',
      'name' => 'comment_notify',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Comment Notify',
        'description' => 'Comment follow-up e-mail notification for anonymous as well as registered users.',
        'dependencies' => 
        array (
          0 => 'comment',
          1 => 'token',
        ),
        'core' => '7.x',
        'configure' => 'admin/config/people/comment_notify',
        'files' => 
        array (
          0 => 'comment_notify.install',
          1 => 'comment_notify.module',
          2 => 'comment_notify.migrate.inc',
          3 => 'comment_notify.tokens.inc',
          4 => 'comment_notify.inc',
          5 => 'comment_notify.test',
        ),
        'version' => '7.x-1.2',
        'project' => 'comment_notify',
        'datestamp' => '1363700412',
        'php' => '5.2.4',
      ),
      'project' => 'comment_notify',
      'version' => '7.x-1.2',
    ),
    'common_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/common_test.module',
      'name' => 'common_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Common Test',
        'description' => 'Support module for Common tests.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'common_test.css',
          ),
          'print' => 
          array (
            0 => 'common_test.print.css',
          ),
        ),
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'common_test_cron_helper' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/common_test_cron_helper.module',
      'name' => 'common_test_cron_helper',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Common Test Cron Helper',
        'description' => 'Helper module for CronRunTestCase::testCronExceptions().',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'compact_forms' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/compact_forms/compact_forms.module',
      'name' => 'compact_forms',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Compact Forms',
        'description' => 'Overlays form fields with their respective labels using JavaScript.',
        'package' => 'User interface',
        'core' => '7.x',
        'configure' => 'admin/config/user-interface/compact_forms',
        'version' => '7.x-1.0',
        'project' => 'compact_forms',
        'datestamp' => '1294554354',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'compact_forms',
      'version' => '7.x-1.0',
    ),
    'conditional_styles' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/conditional_styles/conditional_styles.module',
      'name' => 'conditional_styles',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Conditional Stylesheets',
        'description' => 'Allows themes to add conditional stylesheets.',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'conditional_styles.install',
          1 => 'conditional_styles.module',
          2 => 'tests/conditional_styles.test',
        ),
        'version' => '7.x-2.2',
        'project' => 'conditional_styles',
        'datestamp' => '1373559072',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'conditional_styles',
      'version' => '7.x-2.2',
    ),
    'conditional_styles_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/conditional_styles/tests/conditional_styles_test.module',
      'name' => 'conditional_styles_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Conditional Stylesheets Tests',
        'package' => 'Testing',
        'core' => '7.x',
        'hidden' => true,
        'dependencies' => 
        array (
          0 => 'conditional_styles',
        ),
        'files' => 
        array (
          0 => 'conditional_styles_test.module',
        ),
        'version' => '7.x-2.2',
        'project' => 'conditional_styles',
        'datestamp' => '1373559072',
        'description' => '',
        'php' => '5.2.4',
      ),
      'project' => 'conditional_styles',
      'version' => '7.x-2.2',
    ),
    'contact' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/contact/contact.module',
      'name' => 'contact',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Contact',
        'description' => 'Enables the use of both personal and site-wide contact forms.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'contact.test',
        ),
        'configure' => 'admin/structure/contact',
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'context' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/context/context.module',
      'name' => 'context',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7000',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Context',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'description' => 'Provide modules with a cache that lasts for a single page request.',
        'package' => 'Context',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'tests/context.test',
          1 => 'tests/context.conditions.test',
          2 => 'tests/context.reactions.test',
        ),
        'version' => '7.x-3.0-beta7',
        'project' => 'context',
        'datestamp' => '1375065368',
        'php' => '5.2.4',
      ),
      'project' => 'context',
      'version' => '7.x-3.0-beta7',
    ),
    'contextual' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/contextual/contextual.module',
      'name' => 'contextual',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Contextual links',
        'description' => 'Provides contextual links to perform actions related to elements on a page.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'contextual.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'context_layouts' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/context/context_layouts/context_layouts.module',
      'name' => 'context_layouts',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Context layouts',
        'description' => 'Allow theme layer to provide multiple region layouts and integrate with context.',
        'dependencies' => 
        array (
          0 => 'context',
        ),
        'package' => 'Context',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'plugins/context_layouts_reaction_block.inc',
        ),
        'version' => '7.x-3.0-beta7',
        'project' => 'context',
        'datestamp' => '1375065368',
        'php' => '5.2.4',
      ),
      'project' => 'context',
      'version' => '7.x-3.0-beta7',
    ),
    'context_ui' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/context/context_ui/context_ui.module',
      'name' => 'context_ui',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '6004',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Context UI',
        'description' => 'Provides a simple UI for settings up a site structure using Context.',
        'dependencies' => 
        array (
          0 => 'context',
        ),
        'package' => 'Context',
        'core' => '7.x',
        'configure' => 'admin/structure/context',
        'files' => 
        array (
          0 => 'context.module',
          1 => 'tests/context_ui.test',
        ),
        'version' => '7.x-3.0-beta7',
        'project' => 'context',
        'datestamp' => '1375065368',
        'php' => '5.2.4',
      ),
      'project' => 'context',
      'version' => '7.x-3.0-beta7',
    ),
    'cross_session_message' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/custom/cross_session_message/cross_session_message.module',
      'name' => 'cross_session_message',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Cross-session Messaging',
        'description' => 'Invoke drupal_set_message on another user\'s session',
        'core' => '7.x',
        'package' => 'Base MCN Features',
        'dependencies' => 
        array (
          0 => 'rules',
        ),
        'version' => NULL,
        'php' => '5.2.4',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'ctools' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib-patched/ctools/ctools.module',
      'name' => 'ctools',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '6008',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Chaos tools',
        'description' => 'A library of helpful tools by Merlin of Chaos.',
        'core' => '7.x',
        'package' => 'Chaos tool suite',
        'files' => 
        array (
          0 => 'includes/context.inc',
          1 => 'includes/math-expr.inc',
          2 => 'includes/stylizer.inc',
        ),
        'version' => '7.x-1.3',
        'project' => 'ctools',
        'datestamp' => '1365013512',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'ctools',
      'version' => '7.x-1.3',
    ),
    'ctools_access_ruleset' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib-patched/ctools/ctools_access_ruleset/ctools_access_ruleset.module',
      'name' => 'ctools_access_ruleset',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Custom rulesets',
        'description' => 'Create custom, exportable, reusable access rulesets for applications like Panels.',
        'core' => '7.x',
        'package' => 'Chaos tool suite',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'version' => '7.x-1.3',
        'project' => 'ctools',
        'datestamp' => '1365013512',
        'php' => '5.2.4',
      ),
      'project' => 'ctools',
      'version' => '7.x-1.3',
    ),
    'ctools_ajax_sample' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib-patched/ctools/ctools_ajax_sample/ctools_ajax_sample.module',
      'name' => 'ctools_ajax_sample',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Chaos Tools (CTools) AJAX Example',
        'description' => 'Shows how to use the power of Chaos AJAX.',
        'package' => 'Chaos tool suite',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'core' => '7.x',
        'version' => '7.x-1.3',
        'project' => 'ctools',
        'datestamp' => '1365013512',
        'php' => '5.2.4',
      ),
      'project' => 'ctools',
      'version' => '7.x-1.3',
    ),
    'ctools_custom_content' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib-patched/ctools/ctools_custom_content/ctools_custom_content.module',
      'name' => 'ctools_custom_content',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Custom content panes',
        'description' => 'Create custom, exportable, reusable content panes for applications like Panels.',
        'core' => '7.x',
        'package' => 'Chaos tool suite',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'version' => '7.x-1.3',
        'project' => 'ctools',
        'datestamp' => '1365013512',
        'php' => '5.2.4',
      ),
      'project' => 'ctools',
      'version' => '7.x-1.3',
    ),
    'ctools_export_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib-patched/ctools/tests/ctools_export_test/ctools_export_test.module',
      'name' => 'ctools_export_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'CTools export test',
        'description' => 'CTools export test module',
        'core' => '7.x',
        'package' => 'Chaos tool suite',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'hidden' => true,
        'files' => 
        array (
          0 => 'ctools_export.test',
        ),
        'version' => '7.x-1.3',
        'project' => 'ctools',
        'datestamp' => '1365013512',
        'php' => '5.2.4',
      ),
      'project' => 'ctools',
      'version' => '7.x-1.3',
    ),
    'ctools_plugin_example' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib-patched/ctools/ctools_plugin_example/ctools_plugin_example.module',
      'name' => 'ctools_plugin_example',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Chaos Tools (CTools) Plugin Example',
        'description' => 'Shows how an external module can provide ctools plugins (for Panels, etc.).',
        'package' => 'Chaos tool suite',
        'dependencies' => 
        array (
          0 => 'ctools',
          1 => 'panels',
          2 => 'page_manager',
          3 => 'advanced_help',
        ),
        'core' => '7.x',
        'version' => '7.x-1.3',
        'project' => 'ctools',
        'datestamp' => '1365013512',
        'php' => '5.2.4',
      ),
      'project' => 'ctools',
      'version' => '7.x-1.3',
    ),
    'ctools_plugin_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib-patched/ctools/tests/ctools_plugin_test.module',
      'name' => 'ctools_plugin_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Chaos tools plugins test',
        'description' => 'Provides hooks for testing ctools plugins.',
        'package' => 'Chaos tool suite',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'files' => 
        array (
          0 => 'ctools.plugins.test',
          1 => 'object_cache.test',
          2 => 'css.test',
          3 => 'context.test',
          4 => 'math_expression.test',
          5 => 'math_expression_stack.test',
        ),
        'hidden' => true,
        'version' => '7.x-1.3',
        'project' => 'ctools',
        'datestamp' => '1365013512',
        'php' => '5.2.4',
      ),
      'project' => 'ctools',
      'version' => '7.x-1.3',
    ),
    'dashboard' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/dashboard/dashboard.module',
      'name' => 'dashboard',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Dashboard',
        'description' => 'Provides a dashboard page in the administrative interface for organizing administrative tasks and tracking information within your site.',
        'core' => '7.x',
        'package' => 'Core',
        'version' => '7.23',
        'files' => 
        array (
          0 => 'dashboard.test',
        ),
        'dependencies' => 
        array (
          0 => 'block',
        ),
        'configure' => 'admin/dashboard/customize',
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'database_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/database_test.module',
      'name' => 'database_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Database Test',
        'description' => 'Support module for Database layer tests.',
        'core' => '7.x',
        'package' => 'Testing',
        'version' => '7.23',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'date' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/date/date.module',
      'name' => 'date',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date',
        'description' => 'Makes date/time fields available.',
        'dependencies' => 
        array (
          0 => 'date_api',
        ),
        'package' => 'Date/Time',
        'core' => '7.x',
        'php' => '5.2',
        'files' => 
        array (
          0 => 'tests/date_api.test',
          1 => 'tests/date.test',
          2 => 'tests/date_field.test',
          3 => 'tests/date_validation.test',
          4 => 'tests/date_timezone.test',
        ),
        'version' => '7.x-2.6',
        'project' => 'date',
        'datestamp' => '1344850024',
      ),
      'project' => 'date',
      'version' => '7.x-2.6',
    ),
    'date_all_day' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/date/date_all_day/date_all_day.module',
      'name' => 'date_all_day',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date All Day',
        'description' => 'Adds \'All Day\' functionality to date fields, including an \'All Day\' theme and \'All Day\' checkboxes for the Date select and Date popup widgets.',
        'dependencies' => 
        array (
          0 => 'date_api',
          1 => 'date',
        ),
        'package' => 'Date/Time',
        'core' => '7.x',
        'version' => '7.x-2.6',
        'project' => 'date',
        'datestamp' => '1344850024',
        'php' => '5.2.4',
      ),
      'project' => 'date',
      'version' => '7.x-2.6',
    ),
    'date_api' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/date/date_api/date_api.module',
      'name' => 'date_api',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date API',
        'description' => 'A Date API that can be used by other modules.',
        'package' => 'Date/Time',
        'core' => '7.x',
        'php' => '5.2',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'date.css',
          ),
        ),
        'files' => 
        array (
          0 => 'date_api.module',
          1 => 'date_api_sql.inc',
        ),
        'version' => '7.x-2.6',
        'project' => 'date',
        'datestamp' => '1344850024',
        'dependencies' => 
        array (
        ),
      ),
      'project' => 'date',
      'version' => '7.x-2.6',
    ),
    'date_context' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/date/date_context/date_context.module',
      'name' => 'date_context',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date Context',
        'description' => 'Adds an option to the Context module to set a context condition based on the value of a date field.',
        'package' => 'Date/Time',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'date',
          1 => 'context',
        ),
        'files' => 
        array (
          0 => 'date_context.module',
          1 => 'plugins/date_context_date_condition.inc',
        ),
        'version' => '7.x-2.6',
        'project' => 'date',
        'datestamp' => '1344850024',
        'php' => '5.2.4',
      ),
      'project' => 'date',
      'version' => '7.x-2.6',
    ),
    'date_migrate' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/date/date_migrate/date_migrate.module',
      'name' => 'date_migrate',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date Migration',
        'description' => 'Provides support for importing into date fields with the Migrate module.',
        'core' => '7.x',
        'package' => 'Date/Time',
        'dependencies' => 
        array (
          0 => 'migrate',
          1 => 'date',
        ),
        'files' => 
        array (
          0 => 'date.migrate.inc',
          1 => 'date_migrate.test',
        ),
        'version' => '7.x-2.6',
        'project' => 'date',
        'datestamp' => '1344850024',
        'php' => '5.2.4',
      ),
      'project' => 'date',
      'version' => '7.x-2.6',
    ),
    'date_migrate_example' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/date/date_migrate/date_migrate_example/date_migrate_example.module',
      'name' => 'date_migrate_example',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'date',
          1 => 'date_repeat',
          2 => 'date_repeat_field',
          3 => 'date_migrate',
          4 => 'features',
          5 => 'migrate',
        ),
        'description' => 'Examples of migrating with the Date module',
        'features' => 
        array (
          'field' => 
          array (
            0 => 'node-date_migrate_example-body',
            1 => 'node-date_migrate_example-field_date',
            2 => 'node-date_migrate_example-field_date_range',
            3 => 'node-date_migrate_example-field_date_repeat',
            4 => 'node-date_migrate_example-field_datestamp',
            5 => 'node-date_migrate_example-field_datestamp_range',
            6 => 'node-date_migrate_example-field_datetime',
            7 => 'node-date_migrate_example-field_datetime_range',
          ),
          'node' => 
          array (
            0 => 'date_migrate_example',
          ),
        ),
        'files' => 
        array (
          0 => 'date_migrate_example.migrate.inc',
        ),
        'name' => 'Date Migration Example',
        'package' => 'Features',
        'project' => 'date',
        'version' => '7.x-2.6',
        'datestamp' => '1344850024',
        'php' => '5.2.4',
      ),
      'project' => 'date',
      'version' => '7.x-2.6',
    ),
    'date_popup' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/date/date_popup/date_popup.module',
      'name' => 'date_popup',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date Popup',
        'description' => 'Enables jquery popup calendars and time entry widgets for selecting dates and times.',
        'dependencies' => 
        array (
          0 => 'date_api',
        ),
        'package' => 'Date/Time',
        'core' => '7.x',
        'configure' => 'admin/config/date/date_popup',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'themes/datepicker.1.7.css',
          ),
        ),
        'version' => '7.x-2.6',
        'project' => 'date',
        'datestamp' => '1344850024',
        'php' => '5.2.4',
      ),
      'project' => 'date',
      'version' => '7.x-2.6',
    ),
    'date_repeat' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/date/date_repeat/date_repeat.module',
      'name' => 'date_repeat',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date Repeat API',
        'description' => 'A Date Repeat API to calculate repeating dates and times from iCal rules.',
        'dependencies' => 
        array (
          0 => 'date_api',
        ),
        'package' => 'Date/Time',
        'core' => '7.x',
        'php' => '5.2',
        'files' => 
        array (
          0 => 'tests/date_repeat.test',
          1 => 'tests/date_repeat_form.test',
        ),
        'version' => '7.x-2.6',
        'project' => 'date',
        'datestamp' => '1344850024',
      ),
      'project' => 'date',
      'version' => '7.x-2.6',
    ),
    'date_repeat_field' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/date/date_repeat_field/date_repeat_field.module',
      'name' => 'date_repeat_field',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date Repeat Field',
        'description' => 'Creates the option of Repeating date fields and manages Date fields that use the Date Repeat API.',
        'dependencies' => 
        array (
          0 => 'date_api',
          1 => 'date',
          2 => 'date_repeat',
        ),
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'date_repeat_field.css',
          ),
        ),
        'package' => 'Date/Time',
        'core' => '7.x',
        'version' => '7.x-2.6',
        'project' => 'date',
        'datestamp' => '1344850024',
        'php' => '5.2.4',
      ),
      'project' => 'date',
      'version' => '7.x-2.6',
    ),
    'date_tools' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/date/date_tools/date_tools.module',
      'name' => 'date_tools',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date Tools',
        'description' => 'Tools to import and auto-create dates and calendars.',
        'dependencies' => 
        array (
          0 => 'date',
        ),
        'package' => 'Date/Time',
        'core' => '7.x',
        'configure' => 'admin/config/date/tools',
        'files' => 
        array (
          0 => 'tests/date_tools.test',
        ),
        'version' => '7.x-2.6',
        'project' => 'date',
        'datestamp' => '1344850024',
        'php' => '5.2.4',
      ),
      'project' => 'date',
      'version' => '7.x-2.6',
    ),
    'date_views' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/date/date_views/date_views.module',
      'name' => 'date_views',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Date Views',
        'description' => 'Views integration for date fields and date functionality.',
        'package' => 'Date/Time',
        'dependencies' => 
        array (
          0 => 'date_api',
          1 => 'views',
        ),
        'core' => '7.x',
        'php' => '5.2',
        'files' => 
        array (
          0 => 'includes/date_views_argument_handler.inc',
          1 => 'includes/date_views_argument_handler_simple.inc',
          2 => 'includes/date_views_filter_handler.inc',
          3 => 'includes/date_views_filter_handler_simple.inc',
          4 => 'includes/date_views.views_default.inc',
          5 => 'includes/date_views.views.inc',
          6 => 'includes/date_views_plugin_pager.inc',
        ),
        'version' => '7.x-2.6',
        'project' => 'date',
        'datestamp' => '1344850024',
      ),
      'project' => 'date',
      'version' => '7.x-2.6',
    ),
    'dblog' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/dblog/dblog.module',
      'name' => 'dblog',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '1',
      'schema_version' => '7002',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Database logging',
        'description' => 'Logs and records system events to the database.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'dblog.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'demo' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/demo/demo.module',
      'name' => 'demo',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Demonstration site',
        'description' => 'Create snapshots and reset the site for demonstration or testing purposes.',
        'package' => 'Development',
        'core' => '7.x',
        'configure' => 'admin/structure/demo',
        'files' => 
        array (
          0 => 'demo.module',
          1 => 'demo.admin.inc',
          2 => 'database_mysql_dump.inc',
        ),
        'version' => '7.x-1.0+6-dev',
        'project' => 'demo',
        'datestamp' => '1338034161',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'demo',
      'version' => '7.x-1.0+6-dev',
    ),
    'demo_reset' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/demo/demo_reset.module',
      'name' => 'demo_reset',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Demonstration site reset',
        'description' => 'Allows to reset the site on cron runs and to <strong>reset the site WITHOUT CONFIRMATION.</strong> Only supposed to be used on public demonstration sites. Use at your own risk.',
        'package' => 'Development',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'demo',
        ),
        'files' => 
        array (
          0 => 'demo_reset.module',
        ),
        'version' => '7.x-1.0+6-dev',
        'project' => 'demo',
        'datestamp' => '1338034161',
        'php' => '5.2.4',
      ),
      'project' => 'demo',
      'version' => '7.x-1.0+6-dev',
    ),
    'devel' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/devel/devel.module',
      'name' => 'devel',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Devel',
        'description' => 'Various blocks, pages, and functions for developers.',
        'package' => 'Development',
        'core' => '7.x',
        'configure' => 'admin/config/development/devel',
        'tags' => 
        array (
          0 => 'developer',
        ),
        'files' => 
        array (
          0 => 'devel.test',
          1 => 'devel.mail.inc',
        ),
        'version' => '7.x-1.3',
        'project' => 'devel',
        'datestamp' => '1338940281',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'devel',
      'version' => '7.x-1.3',
    ),
    'devel_generate' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/devel/devel_generate/devel_generate.module',
      'name' => 'devel_generate',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Devel generate',
        'description' => 'Generate dummy users, nodes, and taxonomy terms.',
        'package' => 'Development',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'devel',
        ),
        'tags' => 
        array (
          0 => 'developer',
        ),
        'configure' => 'admin/config/development/generate',
        'version' => '7.x-1.3',
        'project' => 'devel',
        'datestamp' => '1338940281',
        'php' => '5.2.4',
      ),
      'project' => 'devel',
      'version' => '7.x-1.3',
    ),
    'devel_node_access' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/devel/devel_node_access.module',
      'name' => 'devel_node_access',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Devel node access',
        'description' => 'Developer blocks and page illustrating relevant node_access records.',
        'package' => 'Development',
        'dependencies' => 
        array (
          0 => 'menu',
        ),
        'core' => '7.x',
        'configure' => 'admin/config/development/devel',
        'tags' => 
        array (
          0 => 'developer',
        ),
        'version' => '7.x-1.3',
        'project' => 'devel',
        'datestamp' => '1338940281',
        'php' => '5.2.4',
      ),
      'project' => 'devel',
      'version' => '7.x-1.3',
    ),
    'diff' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/diff/diff.module',
      'name' => 'diff',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Diff',
        'description' => 'Show differences between content revisions.',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'DiffEngine.php',
        ),
        'version' => '7.x-3.2',
        'project' => 'diff',
        'datestamp' => '1352784357',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'diff',
      'version' => '7.x-3.2',
    ),
    'directory' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/features/person_directory/directory.module',
      'name' => 'directory',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Directory',
        'description' => 'Allow for a staff, faculty, team, etc.. listing on the site, with an optional featured person block and index block.',
        'core' => '7.x',
        'package' => 'MCN Business',
        'php' => '5.2.4',
        'version' => '7.x-1.0alpha',
        'project' => 'directory',
        'dependencies' => 
        array (
          0 => 'auto_nodetitle',
          1 => 'basic_page',
          2 => 'ctools',
          3 => 'email',
          4 => 'features',
          5 => 'field_group',
          6 => 'image',
          7 => 'location_cck',
          8 => 'media',
          9 => 'options',
          10 => 'strongarm',
          11 => 'taxonomy',
        ),
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'field_group:field_group:1',
            1 => 'strongarm:strongarm:1',
            2 => 'views:views_default:3.0',
          ),
          'features_api' => 
          array (
            0 => 'api:2',
          ),
          'field_base' => 
          array (
            0 => 'field_dp_email_address',
            1 => 'field_dp_first',
            2 => 'field_dp_hours',
            3 => 'field_dp_job_role',
            4 => 'field_dp_last',
            5 => 'field_dp_location',
            6 => 'field_dp_middle',
            7 => 'field_dp_photo',
            8 => 'field_dp_type',
          ),
          'field_group' => 
          array (
            0 => 'group_dp_biography|node|directory_person|form',
            1 => 'group_dp_contact|node|directory_person|form',
            2 => 'group_dp_container|node|directory_person|form',
            3 => 'group_dp_general|node|directory_person|form',
            4 => 'group_dp_name|node|directory_person|form',
          ),
          'field_instance' => 
          array (
            0 => 'node-directory_person-body',
            1 => 'node-directory_person-field_dp_email_address',
            2 => 'node-directory_person-field_dp_first',
            3 => 'node-directory_person-field_dp_hours',
            4 => 'node-directory_person-field_dp_job_role',
            5 => 'node-directory_person-field_dp_last',
            6 => 'node-directory_person-field_dp_location',
            7 => 'node-directory_person-field_dp_middle',
            8 => 'node-directory_person-field_dp_photo',
            9 => 'node-directory_person-field_dp_type',
          ),
          'node' => 
          array (
            0 => 'directory_person',
          ),
          'taxonomy' => 
          array (
            0 => 'directory_person_type',
          ),
          'user_permission' => 
          array (
            0 => 'create directory_person content',
            1 => 'delete any directory_person content',
            2 => 'delete own directory_person content',
            3 => 'edit any directory_person content',
            4 => 'edit own directory_person content',
          ),
          'variable' => 
          array (
            0 => 'ant_directory_person',
            1 => 'ant_pattern_directory_person',
            2 => 'ant_php_directory_person',
            3 => 'menu_options_directory_person',
            4 => 'menu_parent_directory_person',
            5 => 'node_options_directory_person',
            6 => 'node_preview_directory_person',
            7 => 'node_submitted_directory_person',
            8 => 'pathauto_node_directory_person_pattern',
          ),
          'views_view' => 
          array (
            0 => 'directory_persons',
          ),
        ),
      ),
      'project' => 'directory',
      'version' => '7.x-1.0alpha',
    ),
    'discography' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/features/discography/discography.module',
      'name' => 'discography',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Discography',
        'description' => 'Adds Albums and Tracks content types and associated views, including the site audio player.',
        'core' => '7.x',
        'package' => 'Base MCN Features',
        'version' => '7.x-2.0',
        'project' => 'discography',
        'dependencies' => 
        array (
          0 => 'basic_page',
          1 => 'colorbox',
          2 => 'context',
          3 => 'features',
          4 => 'fences',
          5 => 'field_group',
          6 => 'jplayer',
          7 => 'link',
          8 => 'list',
          9 => 'mcn_base_feature',
          10 => 'mcn_wysiwyg',
          11 => 'media',
          12 => 'mediaelement',
          13 => 'multiselect',
          14 => 'node_reference',
          15 => 'strongarm',
          16 => 'views_feature',
        ),
        'features' => 
        array (
          'context' => 
          array (
            0 => 'album_block',
          ),
          'ctools' => 
          array (
            0 => 'context:context:3',
            1 => 'field_group:field_group:1',
            2 => 'strongarm:strongarm:1',
            3 => 'views:views_default:3.0',
          ),
          'features_api' => 
          array (
            0 => 'api:2',
          ),
          'field_base' => 
          array (
            0 => 'field_album_art',
            1 => 'field_audio_checkbox',
            2 => 'field_buy_now',
            3 => 'field_mp3',
            4 => 'field_site_audio_sort',
            5 => 'field_sort',
            6 => 'field_track_reference',
          ),
          'field_group' => 
          array (
            0 => 'group_website_audio_player|node|mp3_track|form',
          ),
          'field_instance' => 
          array (
            0 => 'node-album-body',
            1 => 'node-album-field_album_art',
            2 => 'node-album-field_buy_now',
            3 => 'node-album-field_sort',
            4 => 'node-album-field_track_reference',
            5 => 'node-mp3_track-body',
            6 => 'node-mp3_track-field_audio_checkbox',
            7 => 'node-mp3_track-field_mp3',
            8 => 'node-mp3_track-field_site_audio_sort',
          ),
          'image' => 
          array (
            0 => '50x50',
          ),
          'node' => 
          array (
            0 => 'album',
            1 => 'mp3_track',
          ),
          'user_permission' => 
          array (
            0 => 'create album content',
            1 => 'create mp3_track content',
            2 => 'delete any album content',
            3 => 'delete any mp3_track content',
            4 => 'delete own album content',
            5 => 'delete own mp3_track content',
            6 => 'edit any album content',
            7 => 'edit any mp3_track content',
            8 => 'edit own album content',
            9 => 'edit own mp3_track content',
          ),
          'variable' => 
          array (
            0 => 'colorbox_load',
            1 => 'comment_album',
            2 => 'comment_anonymous_album',
            3 => 'comment_anonymous_mp3_track',
            4 => 'comment_default_mode_album',
            5 => 'comment_default_mode_mp3_track',
            6 => 'comment_default_per_page_album',
            7 => 'comment_default_per_page_mp3_track',
            8 => 'comment_form_location_album',
            9 => 'comment_form_location_mp3_track',
            10 => 'comment_mp3_track',
            11 => 'comment_preview_album',
            12 => 'comment_preview_mp3_track',
            13 => 'comment_subject_field_album',
            14 => 'comment_subject_field_mp3_track',
            15 => 'jplayer_protected',
            16 => 'menu_options_album',
            17 => 'menu_options_mp3_track',
            18 => 'menu_parent_album',
            19 => 'menu_parent_mp3_track',
            20 => 'node_options_album',
            21 => 'node_options_mp3_track',
            22 => 'node_preview_album',
            23 => 'node_preview_mp3_track',
            24 => 'node_submitted_album',
            25 => 'node_submitted_mp3_track',
            26 => 'pathauto_node_album_pattern',
            27 => 'pathauto_node_mp3_track_pattern',
            28 => 'scheduler_publish_enable_album',
            29 => 'scheduler_publish_enable_mp3_track',
            30 => 'scheduler_publish_touch_album',
            31 => 'scheduler_publish_touch_mp3_track',
            32 => 'scheduler_unpublish_enable_album',
            33 => 'scheduler_unpublish_enable_mp3_track',
          ),
          'views_view' => 
          array (
            0 => 'albums_tracks',
          ),
        ),
        'project status url' => 'http://features.devmcn.com/fserver',
        'php' => '5.2.4',
      ),
      'project' => 'discography',
      'version' => '7.x-2.0',
    ),
    'document_library' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/features/document_library/document_library.module',
      'name' => 'document_library',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Document Library',
        'description' => 'Allow for a document listing on the site',
        'core' => '7.x',
        'package' => 'MCN Business',
        'php' => '5.2.4',
        'version' => '7.x-1.0alpha',
        'project' => 'document_library',
        'dependencies' => 
        array (
          0 => 'basic_page',
          1 => 'ctools',
          2 => 'features',
          3 => 'file',
          4 => 'media',
          5 => 'options',
          6 => 'strongarm',
          7 => 'taxonomy',
          8 => 'views',
        ),
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'strongarm:strongarm:1',
            1 => 'views:views_default:3.0',
          ),
          'features_api' => 
          array (
            0 => 'api:2',
          ),
          'field_base' => 
          array (
            0 => 'field_ld_file',
            1 => 'field_ld_type',
          ),
          'field_instance' => 
          array (
            0 => 'node-library_document-body',
            1 => 'node-library_document-field_ld_file',
            2 => 'node-library_document-field_ld_type',
          ),
          'node' => 
          array (
            0 => 'library_document',
          ),
          'taxonomy' => 
          array (
            0 => 'library_document_type',
          ),
          'user_permission' => 
          array (
            0 => 'create library_document content',
            1 => 'delete any library_document content',
            2 => 'delete own library_document content',
            3 => 'edit any library_document content',
            4 => 'edit own library_document content',
          ),
          'variable' => 
          array (
            0 => 'menu_options_library_document',
            1 => 'menu_parent_library_document',
            2 => 'node_options_library_document',
            3 => 'node_preview_library_document',
            4 => 'node_submitted_library_document',
            5 => 'pathauto_node_library_document_pattern',
          ),
          'views_view' => 
          array (
            0 => 'library_documents',
          ),
        ),
      ),
      'project' => 'document_library',
      'version' => '7.x-1.0alpha',
    ),
    'drupal_system_listing_compatible_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/drupal_system_listing_compatible_test/drupal_system_listing_compatible_test.module',
      'name' => 'drupal_system_listing_compatible_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Drupal system listing compatible test',
        'description' => 'Support module for testing the drupal_system_listing function.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'drupal_system_listing_incompatible_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/drupal_system_listing_incompatible_test/drupal_system_listing_incompatible_test.module',
      'name' => 'drupal_system_listing_incompatible_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Drupal system listing incompatible test',
        'description' => 'Support module for testing the drupal_system_listing function.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'ds' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/ds/ds.module',
      'name' => 'ds',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Display Suite',
        'description' => 'Extend the display options for every entity type.',
        'core' => '7.x',
        'package' => 'Display Suite',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'files' => 
        array (
          0 => 'views/views_plugin_ds_entity_view.inc',
          1 => 'views/views_plugin_ds_fields_view.inc',
          2 => 'tests/ds.base.test',
          3 => 'tests/ds.search.test',
          4 => 'tests/ds.entities.test',
          5 => 'tests/ds.exportables.test',
          6 => 'tests/ds.views.test',
          7 => 'tests/ds.forms.test',
        ),
        'configure' => 'admin/structure/ds',
        'version' => '7.x-2.4',
        'project' => 'ds',
        'datestamp' => '1371030953',
        'php' => '5.2.4',
      ),
      'project' => 'ds',
      'version' => '7.x-2.4',
    ),
    'ds_devel' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/ds/modules/ds_devel/ds_devel.module',
      'name' => 'ds_devel',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Display Suite Devel',
        'description' => 'Development functionality for Display Suite.',
        'core' => '7.x',
        'package' => 'Display Suite',
        'dependencies' => 
        array (
          0 => 'ds',
          1 => 'devel',
        ),
        'version' => '7.x-2.4',
        'project' => 'ds',
        'datestamp' => '1371030953',
        'php' => '5.2.4',
      ),
      'project' => 'ds',
      'version' => '7.x-2.4',
    ),
    'ds_exportables_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/ds/tests/ds_exportables_test/ds_exportables_test.module',
      'name' => 'ds_exportables_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Display Suite exportables test',
        'description' => 'Tests for exportables with Display Suite.',
        'package' => 'Display Suite',
        'core' => '7.x',
        'hidden' => true,
        'version' => '7.x-2.4',
        'project' => 'ds',
        'datestamp' => '1371030953',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'ds',
      'version' => '7.x-2.4',
    ),
    'ds_extras' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/ds/modules/ds_extras/ds_extras.module',
      'name' => 'ds_extras',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Display Suite Extras',
        'description' => 'Contains additional features for Display Suite.',
        'core' => '7.x',
        'package' => 'Display Suite',
        'dependencies' => 
        array (
          0 => 'ds',
        ),
        'configure' => 'admin/structure/ds/list/extras',
        'version' => '7.x-2.4',
        'project' => 'ds',
        'datestamp' => '1371030953',
        'php' => '5.2.4',
      ),
      'project' => 'ds',
      'version' => '7.x-2.4',
    ),
    'ds_format' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/ds/modules/ds_format/ds_format.module',
      'name' => 'ds_format',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Display Suite Format',
        'description' => 'Provides the Display Suite Code format filter.',
        'core' => '7.x',
        'package' => 'Display Suite',
        'dependencies' => 
        array (
          0 => 'ds',
        ),
        'configure' => 'admin/structure/ds/list/extras',
        'version' => '7.x-2.4',
        'project' => 'ds',
        'datestamp' => '1371030953',
        'php' => '5.2.4',
      ),
      'project' => 'ds',
      'version' => '7.x-2.4',
    ),
    'ds_forms' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/ds/modules/ds_forms/ds_forms.module',
      'name' => 'ds_forms',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Display Suite Forms',
        'description' => 'Manage the layout of forms in Display Suite.',
        'core' => '7.x',
        'package' => 'Display Suite',
        'dependencies' => 
        array (
          0 => 'ds',
        ),
        'version' => '7.x-2.4',
        'project' => 'ds',
        'datestamp' => '1371030953',
        'php' => '5.2.4',
      ),
      'project' => 'ds',
      'version' => '7.x-2.4',
    ),
    'ds_search' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/ds/modules/ds_search/ds_search.module',
      'name' => 'ds_search',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Display Suite Search',
        'description' => 'Extend the display options for search results for Drupal Core or Apache Solr.',
        'core' => '7.x',
        'package' => 'Display Suite',
        'dependencies' => 
        array (
          0 => 'ds',
        ),
        'configure' => 'admin/structure/ds/list/search',
        'version' => '7.x-2.4',
        'project' => 'ds',
        'datestamp' => '1371030953',
        'php' => '5.2.4',
      ),
      'project' => 'ds',
      'version' => '7.x-2.4',
    ),
    'ds_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/ds/tests/ds_test.module',
      'name' => 'ds_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Display Suite Test',
        'description' => 'Test module for Display Suite',
        'core' => '7.x',
        'package' => 'Display Suite',
        'dependencies' => 
        array (
          0 => 'ds_extras',
        ),
        'hidden' => true,
        'version' => '7.x-2.4',
        'project' => 'ds',
        'datestamp' => '1371030953',
        'php' => '5.2.4',
      ),
      'project' => 'ds',
      'version' => '7.x-2.4',
    ),
    'ds_ui' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/ds/modules/ds_ui/ds_ui.module',
      'name' => 'ds_ui',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Display Suite UI',
        'description' => 'User interface for managing fields, view modes and classes.',
        'core' => '7.x',
        'package' => 'Display Suite',
        'dependencies' => 
        array (
          0 => 'ds',
        ),
        'version' => '7.x-2.4',
        'project' => 'ds',
        'datestamp' => '1371030953',
        'php' => '5.2.4',
      ),
      'project' => 'ds',
      'version' => '7.x-2.4',
    ),
    'edge_fonts' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/fontyourface/modules/edge_fonts/edge_fonts.module',
      'name' => 'edge_fonts',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Edge Fonts',
        'description' => '@font-your-face provider for Edge Fonts.',
        'dependencies' => 
        array (
          0 => 'fontyourface',
        ),
        'package' => '@font-your-face',
        'core' => '7.x',
        'version' => '7.x-2.8',
        'project' => 'fontyourface',
        'datestamp' => '1367195415',
        'php' => '5.2.4',
      ),
      'project' => 'fontyourface',
      'version' => '7.x-2.8',
    ),
    'email' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/email/email.module',
      'name' => 'email',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Email',
        'description' => 'Defines an email field type.',
        'core' => '7.x',
        'package' => 'Fields',
        'files' => 
        array (
          0 => 'email.migrate.inc',
        ),
        'version' => '7.x-1.2',
        'project' => 'email',
        'datestamp' => '1346254131',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'email',
      'version' => '7.x-1.2',
    ),
    'entity' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/entity/entity.module',
      'name' => 'entity',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Entity API',
        'description' => 'Enables modules to work with any entity type and to provide entities.',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'entity.features.inc',
          1 => 'entity.i18n.inc',
          2 => 'entity.info.inc',
          3 => 'entity.rules.inc',
          4 => 'entity.test',
          5 => 'includes/entity.inc',
          6 => 'includes/entity.controller.inc',
          7 => 'includes/entity.ui.inc',
          8 => 'includes/entity.wrapper.inc',
          9 => 'views/entity.views.inc',
          10 => 'views/handlers/entity_views_field_handler_helper.inc',
          11 => 'views/handlers/entity_views_handler_area_entity.inc',
          12 => 'views/handlers/entity_views_handler_field_boolean.inc',
          13 => 'views/handlers/entity_views_handler_field_date.inc',
          14 => 'views/handlers/entity_views_handler_field_duration.inc',
          15 => 'views/handlers/entity_views_handler_field_entity.inc',
          16 => 'views/handlers/entity_views_handler_field_field.inc',
          17 => 'views/handlers/entity_views_handler_field_numeric.inc',
          18 => 'views/handlers/entity_views_handler_field_options.inc',
          19 => 'views/handlers/entity_views_handler_field_text.inc',
          20 => 'views/handlers/entity_views_handler_field_uri.inc',
          21 => 'views/handlers/entity_views_handler_relationship_by_bundle.inc',
          22 => 'views/handlers/entity_views_handler_relationship.inc',
          23 => 'views/plugins/entity_views_plugin_row_entity_view.inc',
        ),
        'version' => '7.x-1.1',
        'project' => 'entity',
        'datestamp' => '1367338216',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'entity',
      'version' => '7.x-1.1',
    ),
    'entity_autocomplete' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/entity_autocomplete/entity_autocomplete.module',
      'name' => 'entity_autocomplete',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Entity Autocomplete',
        'description' => 'Provides functionalities for entity fields autocompletion, like a menu callback that autocompletes any entity that defines a label, or a ready to use form element.',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'tests/entity_autocomplete.test',
        ),
        'version' => '7.x-1.0-beta3',
        'project' => 'entity_autocomplete',
        'datestamp' => '1355614650',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'entity_autocomplete',
      'version' => '7.x-1.0-beta3',
    ),
    'entity_autocomplete_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/entity_autocomplete/tests/entity_autocomplete_test.module',
      'name' => 'entity_autocomplete_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Entity Autocomplete module form element tests',
        'description' => 'Support module for Entity Autocomplete form element testing.',
        'package' => 'Testing',
        'core' => '7.x',
        'hidden' => true,
        'version' => '7.x-1.0-beta3',
        'project' => 'entity_autocomplete',
        'datestamp' => '1355614650',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'entity_autocomplete',
      'version' => '7.x-1.0-beta3',
    ),
    'entity_cache_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/entity_cache_test.module',
      'name' => 'entity_cache_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Entity cache test',
        'description' => 'Support module for testing entity cache.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'entity_cache_test_dependency',
        ),
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'entity_cache_test_dependency' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/entity_cache_test_dependency.module',
      'name' => 'entity_cache_test_dependency',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Entity cache test dependency',
        'description' => 'Support dependency module for testing entity cache.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'entity_crud_hook_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/entity_crud_hook_test.module',
      'name' => 'entity_crud_hook_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Entity CRUD Hooks Test',
        'description' => 'Support module for CRUD hook tests.',
        'core' => '7.x',
        'package' => 'Testing',
        'version' => '7.23',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'entity_feature' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/entity/tests/entity_feature.module',
      'name' => 'entity_feature',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Entity feature module',
        'description' => 'Provides some entities in code.',
        'version' => '7.x-1.1',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'entity_feature.module',
        ),
        'dependencies' => 
        array (
          0 => 'entity_test',
        ),
        'hidden' => true,
        'project' => 'entity',
        'datestamp' => '1367338216',
        'php' => '5.2.4',
      ),
      'project' => 'entity',
      'version' => '7.x-1.1',
    ),
    'entity_query_access_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/entity_query_access_test.module',
      'name' => 'entity_query_access_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Entity query access test',
        'description' => 'Support module for checking entity query results.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'entity_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/entity/tests/entity_test.module',
      'name' => 'entity_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Entity CRUD test module',
        'description' => 'Provides entity types based upon the CRUD API.',
        'version' => '7.x-1.1',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'entity_test.module',
          1 => 'entity_test.install',
        ),
        'dependencies' => 
        array (
          0 => 'entity',
        ),
        'hidden' => true,
        'project' => 'entity',
        'datestamp' => '1367338216',
        'php' => '5.2.4',
      ),
      'project' => 'entity',
      'version' => '7.x-1.1',
    ),
    'entity_test_i18n' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/entity/tests/entity_test_i18n.module',
      'name' => 'entity_test_i18n',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Entity-test type translation',
        'description' => 'Allows translating entity-test types.',
        'dependencies' => 
        array (
          0 => 'entity_test',
          1 => 'i18n_string',
        ),
        'package' => 'Multilingual - Internationalization',
        'core' => '7.x',
        'hidden' => true,
        'version' => '7.x-1.1',
        'project' => 'entity',
        'datestamp' => '1367338216',
        'php' => '5.2.4',
      ),
      'project' => 'entity',
      'version' => '7.x-1.1',
    ),
    'entity_token' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/entity/entity_token.module',
      'name' => 'entity_token',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Entity tokens',
        'description' => 'Provides token replacements for all properties that have no tokens and are known to the entity API.',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'entity_token.tokens.inc',
          1 => 'entity_token.module',
        ),
        'dependencies' => 
        array (
          0 => 'entity',
        ),
        'version' => '7.x-1.1',
        'project' => 'entity',
        'datestamp' => '1367338216',
        'php' => '5.2.4',
      ),
      'project' => 'entity',
      'version' => '7.x-1.1',
    ),
    'error_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/error_test.module',
      'name' => 'error_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Error test',
        'description' => 'Support module for error and exception testing.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'eva' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/eva/eva.module',
      'name' => 'eva',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Eva',
        'description' => 'Provides a Views display type that can be attached to entities.',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'views',
        ),
        'package' => 'Views',
        'files' => 
        array (
          0 => 'eva_plugin_display_entity.inc',
        ),
        'version' => '7.x-1.2+5-dev',
        'project' => 'eva',
        'datestamp' => '1370263696',
        'php' => '5.2.4',
      ),
      'project' => 'eva',
      'version' => '7.x-1.2+5-dev',
    ),
    'events' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/features/events/events.module',
      'name' => 'events',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Events',
        'description' => 'Adds Basic Event Content Type and Views',
        'core' => '7.x',
        'package' => 'Base MCN Features',
        'php' => '5.2.4',
        'version' => '7.x-3.1',
        'project' => 'events',
        'dependencies' => 
        array (
          0 => 'basic_page',
          1 => 'calendar',
          2 => 'context',
          3 => 'ctools',
          4 => 'date',
          5 => 'date_api',
          6 => 'date_popup',
          7 => 'date_repeat',
          8 => 'date_repeat_field',
          9 => 'date_views',
          10 => 'eva',
          11 => 'features',
          12 => 'link',
          13 => 'mcn_base_feature',
          14 => 'mcn_wysiwyg',
          15 => 'node',
          16 => 'performance_event_field',
          17 => 'strongarm',
          18 => 'text',
          19 => 'views',
          20 => 'views_feature',
        ),
        'features' => 
        array (
          'context' => 
          array (
            0 => 'events_block',
          ),
          'ctools' => 
          array (
            0 => 'context:context:3',
            1 => 'strongarm:strongarm:1',
            2 => 'views:views_default:3.0',
          ),
          'features_api' => 
          array (
            0 => 'api:2',
          ),
          'field_base' => 
          array (
            0 => 'field_date',
            1 => 'field_ticket_link',
            2 => 'field_time',
          ),
          'field_instance' => 
          array (
            0 => 'node-event-body',
            1 => 'node-event-field_date',
            2 => 'node-event-field_ticket_link',
            3 => 'node-event-field_time',
          ),
          'node' => 
          array (
            0 => 'event',
          ),
          'user_permission' => 
          array (
            0 => 'create event content',
            1 => 'delete any event content',
            2 => 'delete own event content',
            3 => 'edit any event content',
            4 => 'edit own event content',
            5 => 'view date repeats',
          ),
          'variable' => 
          array (
            0 => 'comment_anonymous_event',
            1 => 'comment_default_mode_event',
            2 => 'comment_default_per_page_event',
            3 => 'comment_event',
            4 => 'comment_form_location_event',
            5 => 'comment_preview_event',
            6 => 'comment_subject_field_event',
            7 => 'menu_options_event',
            8 => 'menu_parent_event',
            9 => 'node_options_event',
            10 => 'node_preview_event',
            11 => 'node_submitted_event',
            12 => 'pathauto_node_event_pattern',
            13 => 'scheduler_publish_enable_event',
            14 => 'scheduler_publish_touch_event',
            15 => 'scheduler_unpublish_enable_event',
          ),
          'views_view' => 
          array (
            0 => 'calendar',
            1 => 'events',
          ),
        ),
        'project status url' => 'http://features.devmcn.com/fserver',
      ),
      'project' => 'events',
      'version' => '7.x-3.1',
    ),
    'faq' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/features/faq/faq.module',
      'name' => 'faq',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'FAQ',
        'description' => 'Allow for a sortable FAQ listing on the site, with an index block.',
        'core' => '7.x',
        'package' => 'MCN Business',
        'php' => '5.2.4',
        'version' => '7.x-1.0alpha',
        'project' => 'faq',
        'dependencies' => 
        array (
          0 => 'basic_page',
          1 => 'ctools',
          2 => 'features',
          3 => 'strongarm',
          4 => 'views',
        ),
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'strongarm:strongarm:1',
            1 => 'views:views_default:3.0',
          ),
          'features_api' => 
          array (
            0 => 'api:2',
          ),
          'field_instance' => 
          array (
            0 => 'node-faq-body',
          ),
          'node' => 
          array (
            0 => 'faq',
          ),
          'user_permission' => 
          array (
            0 => 'create faq content',
            1 => 'delete any faq content',
            2 => 'delete own faq content',
            3 => 'edit any faq content',
            4 => 'edit own faq content',
          ),
          'variable' => 
          array (
            0 => 'menu_options_faq',
            1 => 'menu_parent_faq',
            2 => 'node_options_faq',
            3 => 'node_preview_faq',
            4 => 'node_submitted_faq',
            5 => 'pathauto_node_faq_pattern',
          ),
          'views_view' => 
          array (
            0 => 'faqs',
          ),
        ),
      ),
      'project' => 'faq',
      'version' => '7.x-1.0alpha',
    ),
    'fb_social' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/fb_social/fb_social.module',
      'name' => 'fb_social',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'fb social',
        'description' => 'Provides integration with Facebook social plugins',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'package' => 'Facebook Social Plugins',
        'core' => '7.x',
        'configure' => 'admin/structure/fbsocial',
        'files' => 
        array (
          0 => 'plugins/views/fb_social_handler_field.inc',
          1 => 'plugins/export_ui/fb_social_presets_ui.inc',
        ),
        'version' => '7.x-2.0-beta4',
        'project' => 'fb_social',
        'datestamp' => '1327205167',
        'php' => '5.2.4',
      ),
      'project' => 'fb_social',
      'version' => '7.x-2.0-beta4',
    ),
    'features' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/features/features.module',
      'name' => 'features',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '6101',
      'weight' => '20',
      'info' => 
      array (
        'name' => 'Features',
        'description' => 'Provides feature management for Drupal.',
        'core' => '7.x',
        'package' => 'Features',
        'files' => 
        array (
          0 => 'tests/features.test',
        ),
        'configure' => 'admin/structure/features/settings',
        'version' => '7.x-2.0-rc2+1-dev',
        'project' => 'features',
        'datestamp' => '1376011162',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'features',
      'version' => '7.x-2.0-rc2+1-dev',
    ),
    'features_override' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/features_override/features_override.module',
      'name' => 'features_override',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Features Override',
        'description' => 'Allows exported Features to be overridden',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'ctools',
          1 => 'features',
        ),
        'package' => 'Features',
        'version' => '7.x-2.0-beta3',
        'project' => 'features_override',
        'datestamp' => '1375464667',
        'php' => '5.2.4',
      ),
      'project' => 'features_override',
      'version' => '7.x-2.0-beta3',
    ),
    'features_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/features/tests/features_test/features_test.module',
      'name' => 'features_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Features Tests',
        'description' => 'Test module for Features testing.',
        'core' => '7.x',
        'package' => 'Testing',
        'php' => '5.2.0',
        'dependencies' => 
        array (
          0 => 'features',
          1 => 'image',
          2 => 'strongarm',
          3 => 'taxonomy',
          4 => 'views',
        ),
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'strongarm:strongarm:1',
            1 => 'views:views_default:3.0',
          ),
          'features_api' => 
          array (
            0 => 'api:2',
          ),
          'field_base' => 
          array (
            0 => 'field_features_test',
          ),
          'field_instance' => 
          array (
            0 => 'node-features_test-field_features_test',
          ),
          'filter' => 
          array (
            0 => 'features_test',
          ),
          'image' => 
          array (
            0 => 'features_test',
          ),
          'node' => 
          array (
            0 => 'features_test',
          ),
          'taxonomy' => 
          array (
            0 => 'taxonomy_features_test',
          ),
          'user_permission' => 
          array (
            0 => 'create features_test content',
          ),
          'views_view' => 
          array (
            0 => 'features_test',
          ),
        ),
        'hidden' => '1',
        'version' => '7.x-2.0-rc2+1-dev',
        'project' => 'features',
        'datestamp' => '1376011162',
      ),
      'project' => 'features',
      'version' => '7.x-2.0-rc2+1-dev',
    ),
    'feeds' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/feeds/feeds.module',
      'name' => 'feeds',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Feeds',
        'description' => 'Aggregates RSS/Atom/RDF feeds, imports CSV files and more.',
        'package' => 'Feeds',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'ctools',
          1 => 'job_scheduler',
        ),
        'files' => 
        array (
          0 => 'includes/FeedsConfigurable.inc',
          1 => 'includes/FeedsImporter.inc',
          2 => 'includes/FeedsSource.inc',
          3 => 'libraries/ParserCSV.inc',
          4 => 'libraries/http_request.inc',
          5 => 'libraries/PuSHSubscriber.inc',
          6 => 'plugins/FeedsCSVParser.inc',
          7 => 'plugins/FeedsFetcher.inc',
          8 => 'plugins/FeedsFileFetcher.inc',
          9 => 'plugins/FeedsHTTPFetcher.inc',
          10 => 'plugins/FeedsNodeProcessor.inc',
          11 => 'plugins/FeedsOPMLParser.inc',
          12 => 'plugins/FeedsParser.inc',
          13 => 'plugins/FeedsPlugin.inc',
          14 => 'plugins/FeedsProcessor.inc',
          15 => 'plugins/FeedsSimplePieParser.inc',
          16 => 'plugins/FeedsSitemapParser.inc',
          17 => 'plugins/FeedsSyndicationParser.inc',
          18 => 'plugins/FeedsTermProcessor.inc',
          19 => 'plugins/FeedsUserProcessor.inc',
          20 => 'tests/feeds.test',
          21 => 'tests/feeds_date_time.test',
          22 => 'tests/feeds_mapper_date.test',
          23 => 'tests/feeds_mapper_date_multiple.test',
          24 => 'tests/feeds_mapper_field.test',
          25 => 'tests/feeds_mapper_file.test',
          26 => 'tests/feeds_mapper_path.test',
          27 => 'tests/feeds_mapper_profile.test',
          28 => 'tests/feeds_mapper.test',
          29 => 'tests/feeds_mapper_config.test',
          30 => 'tests/feeds_fetcher_file.test',
          31 => 'tests/feeds_processor_node.test',
          32 => 'tests/feeds_processor_term.test',
          33 => 'tests/feeds_processor_user.test',
          34 => 'tests/feeds_scheduler.test',
          35 => 'tests/feeds_mapper_link.test',
          36 => 'tests/feeds_mapper_taxonomy.test',
          37 => 'tests/parser_csv.test',
          38 => 'views/feeds_views_handler_argument_importer_id.inc',
          39 => 'views/feeds_views_handler_field_importer_name.inc',
          40 => 'views/feeds_views_handler_field_log_message.inc',
          41 => 'views/feeds_views_handler_field_severity.inc',
          42 => 'views/feeds_views_handler_field_source.inc',
          43 => 'views/feeds_views_handler_filter_severity.inc',
        ),
        'version' => '7.x-2.0-alpha8',
        'project' => 'feeds',
        'datestamp' => '1366671911',
        'php' => '5.2.4',
      ),
      'project' => 'feeds',
      'version' => '7.x-2.0-alpha8',
    ),
    'feeds_import' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/feeds/feeds_import/feeds_import.module',
      'name' => 'feeds_import',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'feeds',
        ),
        'description' => 'An example of a node importer and a user importer.',
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'feeds:feeds_importer_default:1',
          ),
          'feeds_importer' => 
          array (
            0 => 'node',
            1 => 'user',
          ),
        ),
        'files' => 
        array (
          0 => 'feeds_import.test',
        ),
        'name' => 'Feeds Import',
        'package' => 'Feeds',
        'php' => '5.2.4',
        'version' => '7.x-2.0-alpha8',
        'project' => 'feeds',
        'datestamp' => '1366671911',
      ),
      'project' => 'feeds',
      'version' => '7.x-2.0-alpha8',
    ),
    'feeds_news' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/feeds/feeds_news/feeds_news.module',
      'name' => 'feeds_news',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'features',
          1 => 'feeds',
          2 => 'views',
        ),
        'description' => 'A news aggregator built with feeds, creates nodes from imported feed items. With OPML import.',
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'feeds:feeds_importer_default:1',
            1 => 'views:views_default:3.0',
          ),
          'feeds_importer' => 
          array (
            0 => 'feed',
            1 => 'opml',
          ),
          'field' => 
          array (
            0 => 'node-feed_item-field_feed_item_description',
          ),
          'node' => 
          array (
            0 => 'feed',
            1 => 'feed_item',
          ),
          'views_view' => 
          array (
            0 => 'feeds_defaults_feed_items',
          ),
        ),
        'files' => 
        array (
          0 => 'feeds_news.module',
          1 => 'feeds_news.test',
        ),
        'name' => 'Feeds News',
        'package' => 'Feeds',
        'php' => '5.2.4',
        'version' => '7.x-2.0-alpha8',
        'project' => 'feeds',
        'datestamp' => '1366671911',
      ),
      'project' => 'feeds',
      'version' => '7.x-2.0-alpha8',
    ),
    'feeds_tests' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/feeds/tests/feeds_tests.module',
      'name' => 'feeds_tests',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Feeds module tests',
        'description' => 'Support module for Feeds related testing.',
        'package' => 'Testing',
        'version' => '7.x-2.0-alpha8',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'feeds_test.module',
        ),
        'hidden' => true,
        'project' => 'feeds',
        'datestamp' => '1366671911',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'feeds',
      'version' => '7.x-2.0-alpha8',
    ),
    'feeds_ui' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/feeds/feeds_ui/feeds_ui.module',
      'name' => 'feeds_ui',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Feeds Admin UI',
        'description' => 'Administrative UI for Feeds module.',
        'package' => 'Feeds',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'feeds',
        ),
        'configure' => 'admin/structure/feeds',
        'files' => 
        array (
          0 => 'feeds_ui.test',
        ),
        'version' => '7.x-2.0-alpha8',
        'project' => 'feeds',
        'datestamp' => '1366671911',
        'php' => '5.2.4',
      ),
      'project' => 'feeds',
      'version' => '7.x-2.0-alpha8',
    ),
    'fences' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib-patched/fences/fences.module',
      'name' => 'fences',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Fences',
        'description' => 'Configurable field wrappers',
        'package' => 'Fields',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'field',
        ),
        'configure' => 'admin/config/content/fences',
        'version' => '7.x-1.0',
        'project' => 'fences',
        'datestamp' => '1335373578',
        'php' => '5.2.4',
      ),
      'project' => 'fences',
      'version' => '7.x-1.0',
    ),
    'field' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/field/field.module',
      'name' => 'field',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7003',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Field',
        'description' => 'Field API to add fields to entities like nodes and users.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'field.module',
          1 => 'field.attach.inc',
          2 => 'field.info.class.inc',
          3 => 'tests/field.test',
        ),
        'dependencies' => 
        array (
          0 => 'field_sql_storage',
        ),
        'required' => true,
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'theme/field.css',
          ),
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'field_collection' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib-patched/field_collection/field_collection.module',
      'name' => 'field_collection',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Field collection',
        'description' => 'Provides a field collection field, to which any number of fields can be attached.',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'entity',
        ),
        'files' => 
        array (
          0 => 'field_collection.test',
          1 => 'field_collection.info.inc',
          2 => 'views/field_collection_handler_relationship.inc',
        ),
        'configure' => 'admin/structure/field-collections',
        'package' => 'Fields',
        'version' => '7.x-1.0-beta5+1-dev',
        'project' => 'field_collection',
        'datestamp' => '1365772112',
        'php' => '5.2.4',
      ),
      'project' => 'field_collection',
      'version' => '7.x-1.0-beta5+1-dev',
    ),
    'field_group' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/field_group/field_group.module',
      'name' => 'field_group',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7003',
      'weight' => '1',
      'info' => 
      array (
        'name' => 'Fieldgroup',
        'description' => 'Fieldgroup',
        'package' => 'Fields',
        'dependencies' => 
        array (
          0 => 'field',
          1 => 'ctools',
        ),
        'core' => '7.x',
        'files' => 
        array (
          0 => 'field_group.install',
          1 => 'field_group.module',
          2 => 'field_group.field_ui.inc',
          3 => 'field_group.form.inc',
          4 => 'field_group.features.inc',
          5 => 'field_group.test',
        ),
        'version' => '7.x-1.1',
        'project' => 'field_group',
        'datestamp' => '1319051133',
        'php' => '5.2.4',
      ),
      'project' => 'field_group',
      'version' => '7.x-1.1',
    ),
    'field_sql_storage' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/field/modules/field_sql_storage/field_sql_storage.module',
      'name' => 'field_sql_storage',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7002',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Field SQL storage',
        'description' => 'Stores field data in an SQL database.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'field',
        ),
        'files' => 
        array (
          0 => 'field_sql_storage.test',
        ),
        'required' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'field_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/field/tests/field_test.module',
      'name' => 'field_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Field API Test',
        'description' => 'Support module for the Field API tests.',
        'core' => '7.x',
        'package' => 'Testing',
        'files' => 
        array (
          0 => 'field_test.entity.inc',
        ),
        'version' => '7.23',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'field_ui' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/field_ui/field_ui.module',
      'name' => 'field_ui',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Field UI',
        'description' => 'User interface for the Field API.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'field',
        ),
        'files' => 
        array (
          0 => 'field_ui.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'file' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/file/file.module',
      'name' => 'file',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'File',
        'description' => 'Defines a file field type.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'field',
        ),
        'files' => 
        array (
          0 => 'tests/file.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'file_entity' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/file_entity/file_entity.module',
      'name' => 'file_entity',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7214',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'File entity',
        'description' => 'Extends Drupal file entities to be fieldable and viewable.',
        'package' => 'Media',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'field',
          1 => 'file',
          2 => 'ctools',
          3 => 'system (>=7.9)',
        ),
        'files' => 
        array (
          0 => 'views/views_handler_argument_file_type.inc',
          1 => 'views/views_handler_field_file_rendered.inc',
          2 => 'views/views_handler_field_file_type.inc',
          3 => 'views/views_handler_filter_file_type.inc',
          4 => 'views/views_handler_field_file_filename.inc',
          5 => 'views/views_handler_field_file_link.inc',
          6 => 'views/views_handler_field_file_link_edit.inc',
          7 => 'views/views_handler_field_file_link_delete.inc',
          8 => 'views/views_handler_field_file_link_download.inc',
          9 => 'views/views_handler_field_file_link_usage.inc',
          10 => 'views/views_plugin_row_file_rss.inc',
          11 => 'views/views_plugin_row_file_view.inc',
          12 => 'file_entity.test',
        ),
        'configure' => 'admin/structure/file-types',
        'version' => '7.x-2.0-alpha1+2-dev',
        'project' => 'file_entity',
        'datestamp' => '1376356803',
        'php' => '5.2.4',
      ),
      'project' => 'file_entity',
      'version' => '7.x-2.0-alpha1+2-dev',
    ),
    'file_entity_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/file_entity/tests/file_entity_test.module',
      'name' => 'file_entity_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'File Entity Test',
        'description' => 'Support module for File Entity tests.',
        'package' => 'Testing',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'file_entity',
        ),
        'hidden' => true,
        'version' => '7.x-2.0-alpha1+2-dev',
        'project' => 'file_entity',
        'datestamp' => '1376356803',
        'php' => '5.2.4',
      ),
      'project' => 'file_entity',
      'version' => '7.x-2.0-alpha1+2-dev',
    ),
    'file_module_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/file/tests/file_module_test.module',
      'name' => 'file_module_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'File test',
        'description' => 'Provides hooks for testing File module functionality.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'file_styles' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/styles/contrib/file_styles/file_styles.module',
      'name' => 'file_styles',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'File Styles',
        'description' => 'Allows preset transformation of files and media for display.',
        'package' => 'Styles',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'file_styles.module',
          1 => 'file_styles.admin.inc',
          2 => 'includes/styles/FileStyles.inc',
        ),
        'dependencies' => 
        array (
          0 => 'styles',
          1 => 'image',
        ),
        'version' => '7.x-2.0-alpha8',
        'project' => 'styles',
        'datestamp' => '1306964517',
        'php' => '5.2.4',
      ),
      'project' => 'styles',
      'version' => '7.x-2.0-alpha8',
    ),
    'file_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/file_test.module',
      'name' => 'file_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'File test',
        'description' => 'Support module for file handling tests.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'file_test.module',
        ),
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'filter' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/filter/filter.module',
      'name' => 'filter',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7010',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Filter',
        'description' => 'Filters content in preparation for display.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'filter.test',
        ),
        'required' => true,
        'configure' => 'admin/config/content/formats',
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'filter_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/filter_test.module',
      'name' => 'filter_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Filter test module',
        'description' => 'Tests filter hooks and functions.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'flag' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/flag/flag.module',
      'name' => 'flag',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Flag',
        'description' => 'Create customized flags that users can set on content.',
        'core' => '7.x',
        'package' => 'Flags',
        'configure' => 'admin/structure/flags',
        'files' => 
        array (
          0 => 'flag.inc',
          1 => 'flag.rules.inc',
          2 => 'includes/flag_handler_argument_content_id.inc',
          3 => 'includes/flag_handler_field_ops.inc',
          4 => 'includes/flag_handler_filter_flagged.inc',
          5 => 'includes/flag_handler_sort_flagged.inc',
          6 => 'includes/flag_handler_relationships.inc',
          7 => 'includes/flag_plugin_argument_validate_flaggability.inc',
          8 => 'tests/flag.test',
        ),
        'version' => '7.x-2.1',
        'project' => 'flag',
        'datestamp' => '1368081620',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'flag',
      'version' => '7.x-2.1',
    ),
    'flag_actions' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/flag/flag_actions.module',
      'name' => 'flag_actions',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Flag actions',
        'description' => 'Execute actions on Flag events.',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'flag',
        ),
        'package' => 'Flags',
        'configure' => 'admin/structure/webform/flags',
        'files' => 
        array (
          0 => 'flag.install',
          1 => 'flag_actions.module',
        ),
        'version' => '7.x-2.1',
        'project' => 'flag',
        'datestamp' => '1368081620',
        'php' => '5.2.4',
      ),
      'project' => 'flag',
      'version' => '7.x-2.1',
    ),
    'flexslider' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/flexslider/flexslider.module',
      'name' => 'flexslider',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'FlexSlider',
        'description' => 'Integrate the FlexSlider 2 library with Drupal',
        'core' => '7.x',
        'package' => 'FlexSlider',
        'dependencies' => 
        array (
          0 => 'libraries (>=2.x)',
          1 => 'ctools',
          2 => 'image',
        ),
        'files' => 
        array (
          0 => 'theme/flexslider.theme.inc',
          1 => 'flexslider.test',
        ),
        'configure' => 'admin/config/media/flexslider',
        'version' => '7.x-2.0-alpha1+13-dev',
        'project' => 'flexslider',
        'datestamp' => '1376356867',
        'php' => '5.2.4',
      ),
      'project' => 'flexslider',
      'version' => '7.x-2.0-alpha1+13-dev',
    ),
    'flexslider_example' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/flexslider/flexslider_example/flexslider_example.module',
      'name' => 'flexslider_example',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'FlexSlider Example',
        'description' => 'Sample implementation of FlexSlider',
        'core' => '7.x',
        'package' => 'FlexSlider',
        'php' => '5.2.4',
        'dependencies' => 
        array (
          0 => 'context',
          1 => 'features',
          2 => 'flexslider',
          3 => 'flexslider_fields',
          4 => 'flexslider_views',
          5 => 'image',
          6 => 'menu',
          7 => 'strongarm',
          8 => 'views',
        ),
        'features' => 
        array (
          'context' => 
          array (
            0 => 'flexslider_example_thumbnail',
          ),
          'ctools' => 
          array (
            0 => 'context:context:3',
            1 => 'flexslider:flexslider_default_preset:1',
            2 => 'strongarm:strongarm:1',
            3 => 'views:views_default:3.0',
          ),
          'features_api' => 
          array (
            0 => 'api:1',
          ),
          'field' => 
          array (
            0 => 'node-flexslider_example-field_flexslider_example_image',
            1 => 'node-flexslider_example-field_flexslider_example_slidesh',
          ),
          'flexslider_optionset' => 
          array (
            0 => 'flexslider_carousel_with_min_and_max_ranges',
            1 => 'flexslider_default_basic_carousel',
            2 => 'flexslider_default_slider_thumbnail_controlnav',
            3 => 'flexslider_default_slider_w_thumbnail_slider',
            4 => 'flexslider_default_thumbnail_slider',
          ),
          'menu_custom' => 
          array (
            0 => 'menu-flexslider-example',
          ),
          'node' => 
          array (
            0 => 'flexslider_example',
          ),
          'variable' => 
          array (
            0 => 'field_bundle_settings_node__flexslider_example',
          ),
          'views_view' => 
          array (
            0 => 'flexslider_views_example',
          ),
        ),
        'version' => '7.x-2.0-alpha1+13-dev',
        'project' => 'flexslider',
        'datestamp' => '1376356867',
      ),
      'project' => 'flexslider',
      'version' => '7.x-2.0-alpha1+13-dev',
    ),
    'flexslider_fields' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/flexslider/flexslider_fields/flexslider_fields.module',
      'name' => 'flexslider_fields',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'FlexSlider Fields',
        'description' => 'Adds a display mode in Fields for FlexSlider',
        'core' => '7.x',
        'package' => 'FlexSlider',
        'dependencies' => 
        array (
          0 => 'flexslider',
        ),
        'version' => '7.x-2.0-alpha1+13-dev',
        'project' => 'flexslider',
        'datestamp' => '1376356867',
        'php' => '5.2.4',
      ),
      'project' => 'flexslider',
      'version' => '7.x-2.0-alpha1+13-dev',
    ),
    'flexslider_views' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/flexslider/flexslider_views/flexslider_views.module',
      'name' => 'flexslider_views',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'FlexSlider Views Style',
        'description' => 'Adds a Views style plugin for FlexSlider',
        'core' => '7.x',
        'package' => 'FlexSlider',
        'dependencies' => 
        array (
          0 => 'views',
          1 => 'flexslider',
        ),
        'files' => 
        array (
          0 => 'flexslider_views.module',
          1 => 'flexslider_views.views.inc',
          2 => 'flexslider_views_plugin_style_flexslider.inc',
        ),
        'version' => '7.x-2.0-alpha1+13-dev',
        'project' => 'flexslider',
        'datestamp' => '1376356867',
        'php' => '5.2.4',
      ),
      'project' => 'flexslider',
      'version' => '7.x-2.0-alpha1+13-dev',
    ),
    'fontdeck' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/fontyourface/modules/fontdeck/fontdeck.module',
      'name' => 'fontdeck',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Fontdeck',
        'description' => '@font-your-face provider of fonts from Fontdeck.com.',
        'dependencies' => 
        array (
          0 => 'fontyourface',
        ),
        'package' => '@font-your-face',
        'core' => '7.x',
        'php' => '5.2.0',
        'files' => 
        array (
          0 => 'fontdeck.install',
          1 => 'fontdeck.module',
        ),
        'version' => '7.x-2.8',
        'project' => 'fontyourface',
        'datestamp' => '1367195415',
      ),
      'project' => 'fontyourface',
      'version' => '7.x-2.8',
    ),
    'fontsquirrel' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/fontyourface/modules/fontsquirrel/fontsquirrel.module',
      'name' => 'fontsquirrel',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Font Squirrel API',
        'description' => '@font-your-face provider with Font Squirrel fonts.',
        'dependencies' => 
        array (
          0 => 'fontyourface',
        ),
        'package' => '@font-your-face',
        'core' => '7.x',
        'php' => '5.2.0',
        'version' => '7.x-2.8',
        'project' => 'fontyourface',
        'datestamp' => '1367195415',
      ),
      'project' => 'fontyourface',
      'version' => '7.x-2.8',
    ),
    'fonts_com' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/fontyourface/modules/fonts_com/fonts_com.module',
      'name' => 'fonts_com',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Fonts.com',
        'description' => '@font-your-face provider of fonts from Fonts.com.',
        'dependencies' => 
        array (
          0 => 'fontyourface',
        ),
        'package' => '@font-your-face',
        'core' => '7.x',
        'php' => '5.2.0',
        'files' => 
        array (
          0 => 'api.inc',
        ),
        'version' => '7.x-2.8',
        'project' => 'fontyourface',
        'datestamp' => '1367195415',
      ),
      'project' => 'fontyourface',
      'version' => '7.x-2.8',
    ),
    'fontyourface' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/fontyourface/fontyourface.module',
      'name' => 'fontyourface',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => '@font-your-face',
        'description' => 'Manages web fonts.',
        'package' => '@font-your-face',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'fontyourface.test',
        ),
        'version' => '7.x-2.8',
        'project' => 'fontyourface',
        'datestamp' => '1367195415',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'fontyourface',
      'version' => '7.x-2.8',
    ),
    'fontyourface_ui' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/fontyourface/modules/fontyourface_ui/fontyourface_ui.module',
      'name' => 'fontyourface_ui',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => '@font-your-face UI',
        'description' => 'Administrative interface for managing fonts.',
        'package' => '@font-your-face',
        'dependencies' => 
        array (
          0 => 'fontyourface',
          1 => 'views',
        ),
        'configure' => 'admin/config/user-interface/fontyourface',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'views/fontyourface.views_default.inc',
          1 => 'views/views_handler_field_fontyourface_font.inc',
          2 => 'views/views_handler_field_fontyourface_foundry.inc',
          3 => 'views/views_handler_field_fontyourface_license.inc',
          4 => 'views/views_handler_field_fontyourface_provider.inc',
          5 => 'views/views_handler_field_fontyourface_tag_font_tid.inc',
          6 => 'views/views_handler_filter_tag_font_tid.inc',
          7 => 'views/views_handler_relationship_fontyourface_tag.inc',
          8 => 'views/views_handler_field_fontyourface_preview.inc',
          9 => 'views/views_handler_field_fontyourface_enable_disable.inc',
          10 => 'views/views_handler_field_fontyourface_enabled_yes_no.inc',
          11 => 'views/views_handler_filter_fontyourface_provider.inc',
          12 => 'views/views_handler_filter_fontyourface_foundry.inc',
        ),
        'version' => '7.x-2.8',
        'project' => 'fontyourface',
        'datestamp' => '1367195415',
        'php' => '5.2.4',
      ),
      'project' => 'fontyourface',
      'version' => '7.x-2.8',
    ),
    'fontyourface_wysiwyg' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/fontyourface/modules/fontyourface_wysiwyg/fontyourface_wysiwyg.module',
      'name' => 'fontyourface_wysiwyg',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'WYSIWYG for @font-your-face',
        'description' => 'Adds list of enabled fonts to WYSIWYG.',
        'dependencies' => 
        array (
          0 => 'fontyourface',
          1 => 'wysiwyg',
        ),
        'package' => '@font-your-face',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'fontyourface_wysiwyg.module',
        ),
        'version' => '7.x-2.8',
        'project' => 'fontyourface',
        'datestamp' => '1367195415',
        'php' => '5.2.4',
      ),
      'project' => 'fontyourface',
      'version' => '7.x-2.8',
    ),
    'font_reference' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/fontyourface/modules/font_reference/font_reference.module',
      'name' => 'font_reference',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Font Reference',
        'description' => 'Defines a field type for referencing a font from a node.',
        'package' => 'Fields',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'fontyourface',
          1 => 'field',
          2 => 'options',
        ),
        'version' => '7.x-2.8',
        'project' => 'fontyourface',
        'datestamp' => '1367195415',
        'php' => '5.2.4',
      ),
      'project' => 'fontyourface',
      'version' => '7.x-2.8',
    ),
    'form_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/form_test.module',
      'name' => 'form_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'FormAPI Test',
        'description' => 'Support module for Form API tests.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'forum' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/forum/forum.module',
      'name' => 'forum',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Forum',
        'description' => 'Provides discussion forums.',
        'dependencies' => 
        array (
          0 => 'taxonomy',
          1 => 'comment',
        ),
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'forum.test',
        ),
        'configure' => 'admin/structure/forum',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'forum.css',
          ),
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'ga_login' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/ga_login/ga_login.module',
      'name' => 'ga_login',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'GA Login',
        'description' => 'This modules allows two step logins using Google Authenticator.',
        'core' => '7.x',
        'configure' => 'admin/config/people/ga_login',
        'files' => 
        array (
          0 => 'ga4php.php',
          1 => 'ga_login.class.php',
          2 => 'ga_login.install',
          3 => 'ga_login.module',
        ),
        'version' => '7.x-1.4',
        'project' => 'ga_login',
        'datestamp' => '1368626419',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'ga_login',
      'version' => '7.x-1.4',
    ),
    'geocoder' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/geocoder/geocoder.module',
      'name' => 'geocoder',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Geocoder',
        'description' => 'An API and widget to geocode various known data into other GIS data types.',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'geophp',
          1 => 'ctools',
        ),
        'version' => '7.x-1.2',
        'project' => 'geocoder',
        'datestamp' => '1346083034',
        'php' => '5.2.4',
      ),
      'project' => 'geocoder',
      'version' => '7.x-1.2',
    ),
    'geofield' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/geofield/geofield.module',
      'name' => 'geofield',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Geofield',
        'description' => 'Stores geographic and location data (points, lines, and polygons).',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'geophp (>=1.7)',
          1 => 'ctools',
        ),
        'package' => 'Fields',
        'files' => 
        array (
          0 => 'geofield.module',
          1 => 'geofield.install',
          2 => 'geofield.elements.inc',
          3 => 'geofield.widgets.inc',
          4 => 'geofield.formatters.inc',
          5 => 'geofield.openlayers.inc',
          6 => 'geofield.feeds.inc',
          7 => 'tests/geofield.test',
          8 => 'views/geofield.views.inc',
          9 => 'views/handlers/geofield_handler_sort.inc',
          10 => 'views/handlers/geofield_handler_field.inc',
          11 => 'views/handlers/geofield_handler_filter.inc',
          12 => 'views/handlers/geofield_handler_argument_proximity.inc',
          13 => 'views/proximity_plugins/geofieldProximityBase.inc',
          14 => 'views/proximity_plugins/geofieldProximityManual.inc',
          15 => 'views/proximity_plugins/geofieldProximityGeocoder.inc',
          16 => 'views/proximity_plugins/geofieldProximityEntityURL.inc',
          17 => 'views/proximity_plugins/geofieldProximityOtherGeofield.inc',
          18 => 'views/proximity_plugins/geofieldProximityCurrentUser.inc',
          19 => 'views/proximity_plugins/geofieldProximityExposedFilter.inc',
          20 => 'views/proximity_plugins/geofieldProximityContextualFilter.inc',
        ),
        'version' => '7.x-2.0-beta1+6-dev',
        'project' => 'geofield',
        'datestamp' => '1376097325',
        'php' => '5.2.4',
      ),
      'project' => 'geofield',
      'version' => '7.x-2.0-beta1+6-dev',
    ),
    'geofield_map' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/geofield/modules/geofield_map/geofield_map.module',
      'name' => 'geofield_map',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Geofield Map',
        'description' => 'Provides a basic mapping interface for Geofields.',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'geofield',
        ),
        'files' => 
        array (
          0 => 'includes/geofield_map.views.inc',
          1 => 'includes/geofield_map_plugin_style_map.inc',
        ),
        'version' => '7.x-2.0-beta1+6-dev',
        'project' => 'geofield',
        'datestamp' => '1376097325',
        'php' => '5.2.4',
      ),
      'project' => 'geofield',
      'version' => '7.x-2.0-beta1+6-dev',
    ),
    'geofield_openlayers_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/geofield/tests/geofield_openlayers_test.module',
      'name' => 'geofield_openlayers_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Geofield/Openlayers Integration tests',
        'description' => 'Integration tests for Geofield and Openlayers',
        'core' => '7.x',
        'hidden' => true,
        'package' => 'Testing',
        'dependencies' => 
        array (
          0 => 'geofield',
          1 => 'openlayers',
        ),
        'files' => 
        array (
          0 => 'geofield_openlayers.test',
        ),
        'version' => '7.x-2.0-beta1+6-dev',
        'project' => 'geofield',
        'datestamp' => '1376097325',
        'php' => '5.2.4',
      ),
      'project' => 'geofield',
      'version' => '7.x-2.0-beta1+6-dev',
    ),
    'geofield_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/geofield/tests/geofield_test.module',
      'name' => 'geofield_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Geofield test',
        'description' => 'Support module for the Geofield tests.',
        'core' => '7.x',
        'hidden' => true,
        'package' => 'Testing',
        'dependencies' => 
        array (
          0 => 'geocoder',
          1 => 'geofield',
        ),
        'version' => '7.x-2.0-beta1+6-dev',
        'project' => 'geofield',
        'datestamp' => '1376097325',
        'php' => '5.2.4',
      ),
      'project' => 'geofield',
      'version' => '7.x-2.0-beta1+6-dev',
    ),
    'geophp' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/geophp/geophp.module',
      'name' => 'geophp',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'geoPHP',
        'description' => 'Wraps the geoPHP library: advanced geometry operations in PHP',
        'core' => '7.x',
        'version' => '7.x-1.7',
        'project' => 'geophp',
        'datestamp' => '1352084822',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'geophp',
      'version' => '7.x-1.7',
    ),
    'getid3' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/getid3/getid3.module',
      'name' => 'getid3',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'getID3()',
        'description' => 'getID3() extracts useful information from multimedia file formats.',
        'core' => '7.x',
        'configure' => 'admin/config/media/getid3',
        'files' => 
        array (
          0 => 'getid3.admin.inc',
          1 => 'getid3.install',
          2 => 'getid3.module',
        ),
        'version' => '7.x-1.0',
        'project' => 'getid3',
        'datestamp' => '1314210417',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'getid3',
      'version' => '7.x-1.0',
    ),
    'googleanalytics' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/google_analytics/googleanalytics.module',
      'name' => 'googleanalytics',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7007',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Google Analytics',
        'description' => 'Allows your site to be tracked by Google Analytics by adding a Javascript tracking code to every page.',
        'core' => '7.x',
        'package' => 'Statistics',
        'configure' => 'admin/config/system/googleanalytics',
        'files' => 
        array (
          0 => 'googleanalytics.test',
        ),
        'version' => '7.x-1.3',
        'project' => 'google_analytics',
        'datestamp' => '1351810914',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'google_analytics',
      'version' => '7.x-1.3',
    ),
    'google_fonts_api' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/fontyourface/modules/google_fonts_api/google_fonts_api.module',
      'name' => 'google_fonts_api',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Google Fonts API',
        'description' => '@font-your-face provider with Google fonts.',
        'dependencies' => 
        array (
          0 => 'fontyourface',
        ),
        'package' => '@font-your-face',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'views/google_fonts_api.views_default.inc',
        ),
        'version' => '7.x-2.8',
        'project' => 'fontyourface',
        'datestamp' => '1367195415',
        'php' => '5.2.4',
      ),
      'project' => 'fontyourface',
      'version' => '7.x-2.8',
    ),
    'google_plusone' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/google_plusone/google_plusone.module',
      'name' => 'google_plusone',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Google Plus One +1',
        'description' => 'Adds Google +1 button to your site. Enable it per node, or as a block.',
        'core' => '7.x',
        'configure' => 'admin/config/services/google-plusone',
        'version' => '7.x-1.3',
        'project' => 'google_plusone',
        'datestamp' => '1324223739',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'google_plusone',
      'version' => '7.x-1.3',
    ),
    'help' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/help/help.module',
      'name' => 'help',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Help',
        'description' => 'Manages the display of online help.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'help.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'image' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/image/image.module',
      'name' => 'image',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7005',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Image',
        'description' => 'Provides image manipulation tools.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'file',
        ),
        'files' => 
        array (
          0 => 'image.test',
        ),
        'configure' => 'admin/config/media/image-styles',
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'imagecache_actions' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/imagecache_actions/imagecache_actions.module',
      'name' => 'imagecache_actions',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Imagecache Actions',
        'description' => 'Provides a number of additional image effects.',
        'package' => 'Media',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'image',
        ),
        'files' => 
        array (
          0 => 'ImageCacheActionsModuleStreamWrapper.php',
        ),
        'version' => '7.x-1.3',
        'project' => 'imagecache_actions',
        'datestamp' => '1370376654',
        'php' => '5.2.4',
      ),
      'project' => 'imagecache_actions',
      'version' => '7.x-1.3',
    ),
    'imagecache_autorotate' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/imagecache_actions/autorotate/imagecache_autorotate.module',
      'name' => 'imagecache_autorotate',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Imagecache Autorotate',
        'description' => 'Autorotate image based on EXIF Orientation.',
        'package' => 'Media',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'image',
        ),
        'files' => 
        array (
          0 => 'imagecache_autorotate.install',
          1 => 'imagecache_autorotate.module',
        ),
        'version' => '7.x-1.3',
        'project' => 'imagecache_actions',
        'datestamp' => '1370376654',
        'php' => '5.2.4',
      ),
      'project' => 'imagecache_actions',
      'version' => '7.x-1.3',
    ),
    'imagecache_canvasactions' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/imagecache_actions/canvasactions/imagecache_canvasactions.module',
      'name' => 'imagecache_canvasactions',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Imagecache Canvas Actions',
        'description' => 'Actions for manipulating image canvases layers, including watermark and background effect. Also an aspect switcher (portrait/landscape)',
        'package' => 'Media',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'imagecache_actions',
          1 => 'image',
        ),
        'files' => 
        array (
          0 => 'canvasactions.inc',
          1 => 'imagecache_canvasactions.install',
          2 => 'imagecache_canvasactions.module',
          3 => 'rounded_corners.inc',
          4 => 'tests/cheap_dropshadow.imagecache_preset.inc',
          5 => 'tests/keyword_positioning.imagecache_preset.inc',
          6 => 'tests/positioned_underlay.imagecache_preset.inc',
          7 => 'tests/rotate_alpha.imagecache_preset.inc',
          8 => 'tests/rotate_alpha_gif.imagecache_preset.inc',
          9 => 'tests/rotate_scale.imagecache_preset.inc',
          10 => 'tests/rotate_scale_alpha.imagecache_preset.inc',
          11 => 'tests/rounded.imagecache_preset.inc',
          12 => 'tests/rounded_bl.imagecache_preset.inc',
          13 => 'tests/rounded_flattened.imagecache_preset.inc',
          14 => 'tests/watermark_100.imagecache_preset.inc',
          15 => 'tests/watermark_50.imagecache_preset.inc',
        ),
        'version' => '7.x-1.3',
        'project' => 'imagecache_actions',
        'datestamp' => '1370376654',
        'php' => '5.2.4',
      ),
      'project' => 'imagecache_actions',
      'version' => '7.x-1.3',
    ),
    'imagecache_coloractions' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/imagecache_actions/coloractions/imagecache_coloractions.module',
      'name' => 'imagecache_coloractions',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Imagecache Color Actions',
        'description' => 'Additional ImageCache actions, providing color-shifting, brightness and alpha transparency effects.',
        'package' => 'Media',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'imagecache_actions',
          1 => 'image',
        ),
        'files' => 
        array (
          0 => 'imagecache_coloractions.install',
          1 => 'imagecache_coloractions.module',
          2 => 'transparency.inc',
          3 => 'tests/green.imagecache_preset.inc',
        ),
        'version' => '7.x-1.3',
        'project' => 'imagecache_actions',
        'datestamp' => '1370376654',
        'php' => '5.2.4',
      ),
      'project' => 'imagecache_actions',
      'version' => '7.x-1.3',
    ),
    'imagecache_customactions' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/imagecache_actions/customactions/imagecache_customactions.module',
      'name' => 'imagecache_customactions',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Imagecache Custom Actions',
        'description' => 'Allow direct PHP code manipulation of imagecache images.',
        'package' => 'Media',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'imagecache_actions',
          1 => 'image',
        ),
        'files' => 
        array (
          0 => 'imagecache_customactions.install',
          1 => 'imagecache_customactions.module',
        ),
        'version' => '7.x-1.3',
        'project' => 'imagecache_actions',
        'datestamp' => '1370376654',
        'php' => '5.2.4',
      ),
      'project' => 'imagecache_actions',
      'version' => '7.x-1.3',
    ),
    'imagecache_testsuite' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/imagecache_actions/tests/imagecache_testsuite.module',
      'name' => 'imagecache_testsuite',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Imagecache_actions Test Suite',
        'description' => 'Displays a collection of demo presets.',
        'package' => 'Media',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'imagecache_actions',
        ),
        'features' => 
        array (
          'image' => 
          array (
            0 => 'corners_combo',
          ),
        ),
        'version' => '7.x-1.3',
        'project' => 'imagecache_actions',
        'datestamp' => '1370376654',
        'php' => '5.2.4',
      ),
      'project' => 'imagecache_actions',
      'version' => '7.x-1.3',
    ),
    'image_captcha' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/captcha/image_captcha/image_captcha.module',
      'name' => 'image_captcha',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Image CAPTCHA',
        'description' => 'Provides an image based CAPTCHA.',
        'package' => 'Spam control',
        'dependencies' => 
        array (
          0 => 'captcha',
        ),
        'core' => '7.x',
        'configure' => 'admin/config/people/captcha/image_captcha',
        'files' => 
        array (
          0 => 'image_captcha.install',
          1 => 'image_captcha.module',
          2 => 'image_captcha.admin.inc',
          3 => 'image_captcha.user.inc',
        ),
        'version' => '7.x-1.0-beta2',
        'project' => 'captcha',
        'datestamp' => '1325504137',
        'php' => '5.2.4',
      ),
      'project' => 'captcha',
      'version' => '7.x-1.0-beta2',
    ),
    'image_effects_text' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/imagecache_actions/image_effects_text/image_effects_text.module',
      'name' => 'image_effects_text',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Image Effects Text',
        'description' => 'Display simple or dynamic captions on images.',
        'package' => 'Media',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'image',
          1 => 'imagecache_actions',
        ),
        'version' => '7.x-1.3',
        'project' => 'imagecache_actions',
        'datestamp' => '1370376654',
        'php' => '5.2.4',
      ),
      'project' => 'imagecache_actions',
      'version' => '7.x-1.3',
    ),
    'image_effects_text_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/imagecache_actions/image_effects_text/image_effects_text_test/image_effects_text_test.module',
      'name' => 'image_effects_text_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Image Effects Text test',
        'description' => 'Image styles that test the text effect',
        'core' => '7.x',
        'package' => 'Media',
        'project' => 'imagecache_actions',
        'dependencies' => 
        array (
          0 => 'image',
          1 => 'image_effects_text',
          2 => 'imagecache_canvasactions',
          3 => 'system_stream_wrapper',
        ),
        'version' => '7.x-1.3',
        'datestamp' => '1370376654',
        'php' => '5.2.4',
      ),
      'project' => 'imagecache_actions',
      'version' => '7.x-1.3',
    ),
    'image_module_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/image/tests/image_module_test.module',
      'name' => 'image_module_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Image test',
        'description' => 'Provides hook implementations for testing Image module functionality.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'image_module_test.module',
        ),
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'image_resize_filter' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/image_resize_filter/image_resize_filter.module',
      'name' => 'image_resize_filter',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7000',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Image resize filter',
        'description' => 'Filter to automatically scale images to their height and width dimensions.',
        'core' => '7.x',
        'package' => 'Input filters',
        'configure' => 'admin/config/content/formats',
        'version' => '7.x-1.13',
        'project' => 'image_resize_filter',
        'datestamp' => '1313565117',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'image_resize_filter',
      'version' => '7.x-1.13',
    ),
    'image_styles_admin' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/imagecache_actions/image_styles_admin/image_styles_admin.module',
      'name' => 'image_styles_admin',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Image styles admin',
        'description' => 'Provides additional administrative image style functionality.',
        'package' => 'Media',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'image',
        ),
        'version' => '7.x-1.3',
        'project' => 'imagecache_actions',
        'datestamp' => '1370376654',
        'php' => '5.2.4',
      ),
      'project' => 'imagecache_actions',
      'version' => '7.x-1.3',
    ),
    'image_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/image_test.module',
      'name' => 'image_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Image test',
        'description' => 'Support module for image toolkit tests.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'jcarousel' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib-patched/jcarousel/jcarousel.module',
      'name' => 'jcarousel',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'jCarousel',
        'description' => 'Create jQuery-based carousel rotators.',
        'package' => 'User interface',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'jcarousel.module',
          1 => 'includes/jcarousel.views.inc',
          2 => 'includes/jcarousel_style_plugin.inc',
        ),
        'version' => '7.x-2.6',
        'project' => 'jcarousel',
        'datestamp' => '1322116852',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'jcarousel',
      'version' => '7.x-2.6',
    ),
    'job_opportunities' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/features/job_opportunities/job_opportunities.module',
      'name' => 'job_opportunities',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Job Opportunities',
        'description' => 'Allow for a job opportunity listing on the site.',
        'core' => '7.x',
        'package' => 'MCN Business',
        'php' => '5.2.4',
        'version' => '7.x-1.0alpha',
        'project' => 'job_opportunities',
        'dependencies' => 
        array (
          0 => 'basic_page',
          1 => 'ctools',
          2 => 'email',
          3 => 'features',
          4 => 'link',
          5 => 'location_cck',
          6 => 'strongarm',
          7 => 'views',
        ),
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'strongarm:strongarm:1',
            1 => 'views:views_default:3.0',
          ),
          'features_api' => 
          array (
            0 => 'api:2',
          ),
          'field_base' => 
          array (
            0 => 'field_jo_department',
            1 => 'field_jo_email',
            2 => 'field_jo_link',
            3 => 'field_jo_location',
          ),
          'field_instance' => 
          array (
            0 => 'node-job_opportunity-body',
            1 => 'node-job_opportunity-field_jo_department',
            2 => 'node-job_opportunity-field_jo_email',
            3 => 'node-job_opportunity-field_jo_link',
            4 => 'node-job_opportunity-field_jo_location',
          ),
          'node' => 
          array (
            0 => 'job_opportunity',
          ),
          'user_permission' => 
          array (
            0 => 'create job_opportunity content',
            1 => 'delete any job_opportunity content',
            2 => 'delete own job_opportunity content',
            3 => 'edit any job_opportunity content',
            4 => 'edit own job_opportunity content',
          ),
          'variable' => 
          array (
            0 => 'menu_options_job_opportunity',
            1 => 'menu_parent_job_opportunity',
            2 => 'node_options_job_opportunity',
            3 => 'node_preview_job_opportunity',
            4 => 'node_submitted_job_opportunity',
            5 => 'pathauto_node_job_opportunity_pattern',
          ),
          'views_view' => 
          array (
            0 => 'job_opportunities',
          ),
        ),
      ),
      'project' => 'job_opportunities',
      'version' => '7.x-1.0alpha',
    ),
    'job_scheduler' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/job_scheduler/job_scheduler.module',
      'name' => 'job_scheduler',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Job Scheduler',
        'description' => 'Scheduler API',
        'files' => 
        array (
          0 => 'job_scheduler.module',
          1 => 'job_scheduler.install',
          2 => 'JobScheduler.inc',
          3 => 'JobSchedulerCronTab.inc',
        ),
        'core' => '7.x',
        'php' => '5.2',
        'version' => '7.x-2.0-alpha3',
        'project' => 'job_scheduler',
        'datestamp' => '1336466457',
        'dependencies' => 
        array (
        ),
      ),
      'project' => 'job_scheduler',
      'version' => '7.x-2.0-alpha3',
    ),
    'job_scheduler_trigger' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/job_scheduler/modules/job_scheduler_trigger/job_scheduler_trigger.module',
      'name' => 'job_scheduler_trigger',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Job Scheduler Trigger',
        'description' => 'Creates scheduler triggers that fire up at certain days, times',
        'core' => '7.x',
        'php' => '5.2',
        'dependencies' => 
        array (
          0 => 'job_scheduler',
        ),
        'version' => '7.x-2.0-alpha3',
        'project' => 'job_scheduler',
        'datestamp' => '1336466457',
      ),
      'project' => 'job_scheduler',
      'version' => '7.x-2.0-alpha3',
    ),
    'jplayer' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib-patched/jplayer/jplayer.module',
      'name' => 'jplayer',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'jPlayer',
        'description' => 'Provides an HTML5-compatible with Flash-fallback audio player.',
        'package' => 'Media',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'file',
        ),
        'configure' => 'admin/config/media/jplayer',
        'files' => 
        array (
          0 => 'jplayer.install',
          1 => 'jplayer.module',
          2 => 'includes/jplayer.admin.inc',
          3 => 'includes/jplayer.theme.inc',
          4 => 'includes/jplayer.views.inc',
          5 => 'includes/jplayer_style_plugin.inc',
          6 => 'theme/jplayer.tpl.php',
        ),
        'version' => '7.x-2.0-beta1+10-dev',
        'project' => 'jplayer',
        'datestamp' => '1340108610',
        'php' => '5.2.4',
      ),
      'project' => 'jplayer',
      'version' => '7.x-2.0-beta1+10-dev',
    ),
    'jplayer_protect' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib-patched/jplayer/jplayer_protect/jplayer_protect.module',
      'name' => 'jplayer_protect',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'jPlayer Content Protection',
        'description' => 'Prevent direct access to media files.',
        'package' => 'Media',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'jplayer',
        ),
        'files' => 
        array (
          0 => 'jplayer_protect.install',
          1 => 'jplayer_protect.module',
        ),
        'version' => '7.x-2.0-beta1+10-dev',
        'project' => 'jplayer',
        'datestamp' => '1340108610',
        'php' => '5.2.4',
      ),
      'project' => 'jplayer',
      'version' => '7.x-2.0-beta1+10-dev',
    ),
    'jplayer_retheme' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/custom/jplayer_retheme/jplayer_retheme.module',
      'name' => 'jplayer_retheme',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'jPlayer retheme',
        'description' => 'Rethemes jPlayer\'s output to play nicely with Views',
        'package' => 'Media',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'jplayer',
        ),
        'version' => NULL,
        'php' => '5.2.4',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'libraries' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/libraries/libraries.module',
      'name' => 'libraries',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7200',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Libraries',
        'description' => 'Allows version-dependent and shared usage of external libraries.',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'tests/libraries.test',
        ),
        'version' => '7.x-2.1',
        'project' => 'libraries',
        'datestamp' => '1362848412',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'libraries',
      'version' => '7.x-2.1',
    ),
    'libraries_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/libraries/tests/libraries_test.module',
      'name' => 'libraries_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Libraries test module',
        'description' => 'Tests library detection and loading.',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'libraries',
        ),
        'hidden' => true,
        'version' => '7.x-2.1',
        'project' => 'libraries',
        'datestamp' => '1362848412',
        'php' => '5.2.4',
      ),
      'project' => 'libraries',
      'version' => '7.x-2.1',
    ),
    'link' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/link/link.module',
      'name' => 'link',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7001',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Link',
        'description' => 'Defines simple link field types.',
        'core' => '7.x',
        'package' => 'Fields',
        'files' => 
        array (
          0 => 'link.module',
          1 => 'link.migrate.inc',
          2 => 'tests/link.test',
          3 => 'tests/link.attribute.test',
          4 => 'tests/link.crud.test',
          5 => 'tests/link.crud_browser.test',
          6 => 'tests/link.token.test',
          7 => 'tests/link.validate.test',
          8 => 'views/link_views_handler_argument_target.inc',
          9 => 'views/link_views_handler_filter_protocol.inc',
        ),
        'version' => '7.x-1.1',
        'project' => 'link',
        'datestamp' => '1360444361',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'link',
      'version' => '7.x-1.1',
    ),
    'list' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/field/modules/list/list.module',
      'name' => 'list',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7002',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'List',
        'description' => 'Defines list field types. Use with Options to create selection lists.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'field',
          1 => 'options',
        ),
        'files' => 
        array (
          0 => 'tests/list.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'list_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/field/modules/list/tests/list_test.module',
      'name' => 'list_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'List test',
        'description' => 'Support module for the List module tests.',
        'core' => '7.x',
        'package' => 'Testing',
        'version' => '7.23',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'locale' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/locale/locale.module',
      'name' => 'locale',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Locale',
        'description' => 'Adds language handling functionality and enables the translation of the user interface to languages other than English.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'locale.test',
        ),
        'configure' => 'admin/config/regional/language',
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'locale_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/locale/tests/locale_test.module',
      'name' => 'locale_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Locale Test',
        'description' => 'Support module for the locale layer tests.',
        'core' => '7.x',
        'package' => 'Testing',
        'version' => '7.23',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'local_fonts' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/fontyourface/modules/local_fonts/local_fonts.module',
      'name' => 'local_fonts',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Local Fonts',
        'description' => '@font-your-face provider with fonts installed locally on the Drupal server.',
        'dependencies' => 
        array (
          0 => 'fontyourface',
        ),
        'package' => '@font-your-face',
        'core' => '7.x',
        'version' => '7.x-2.8',
        'project' => 'fontyourface',
        'datestamp' => '1367195415',
        'php' => '5.2.4',
      ),
      'project' => 'fontyourface',
      'version' => '7.x-2.8',
    ),
    'location' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/location/location.module',
      'name' => 'location',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Location',
        'package' => 'Location',
        'description' => 'The location module allows you to associate a geographic location with content and users. Users can do proximity searches by postal code.  This is useful for organizing communities that have a geographic presence.',
        'core' => '7.x',
        'configure' => 'admin/config/content/location',
        'files' => 
        array (
          0 => 'location.module',
          1 => 'location.install',
          2 => 'location.admin.inc',
          3 => 'location.georss.inc',
          4 => 'location.inc',
          5 => 'location.token.inc',
          6 => 'location.views.inc',
          7 => 'location.views_default.inc',
          8 => 'location.migrate.inc',
          9 => 'tests/location_testcase.test',
          10 => 'tests/cow.test',
          11 => 'tests/earth.test',
          12 => 'tests/google_geocoder.test',
          13 => 'tests/location_cck.test',
          14 => 'tests/location_cck2.test',
          15 => 'handlers/location_handler_argument_location_country.inc',
          16 => 'handlers/location_handler_argument_location_province.inc',
          17 => 'handlers/location_handler_argument_location_proximity.inc',
          18 => 'handlers/location_handler_field_location_address.inc',
          19 => 'handlers/location_handler_field_location_country.inc',
          20 => 'handlers/location_handler_field_location_distance.inc',
          21 => 'handlers/location_handler_field_location_province.inc',
          22 => 'handlers/location_handler_field_location_street.inc',
          23 => 'handlers/location_handler_filter_location_country.inc',
          24 => 'handlers/location_handler_filter_location_province.inc',
          25 => 'handlers/location_handler_sort_location_distance.inc',
          26 => 'handlers/location_views_handler_field_coordinates.inc',
          27 => 'handlers/location_views_handler_field_latitude.inc',
          28 => 'handlers/location_views_handler_field_longitude.inc',
          29 => 'handlers/location_views_handler_filter_proximity.inc',
          30 => 'plugins/contexts/location.inc',
          31 => 'plugins/relationships/location_from_node.inc',
        ),
        'version' => '7.x-3.0-rc2+0-dev',
        'project' => 'location',
        'datestamp' => '1372780146',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'location',
      'version' => '7.x-3.0-rc2+0-dev',
    ),
    'location_addanother' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/location/contrib/location_addanother/location_addanother.module',
      'name' => 'location_addanother',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Location Add Another',
        'description' => 'Allows you to quickly add locations directly from a node without having to click \'edit\' first.',
        'dependencies' => 
        array (
          0 => 'location',
          1 => 'location_node',
        ),
        'package' => 'Location',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'location_addanother.module',
          1 => 'location_addanother.install',
        ),
        'version' => '7.x-3.0-rc2+0-dev',
        'project' => 'location',
        'datestamp' => '1372780146',
        'php' => '5.2.4',
      ),
      'project' => 'location',
      'version' => '7.x-3.0-rc2+0-dev',
    ),
    'location_cck' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/location/contrib/location_cck/location_cck.module',
      'name' => 'location_cck',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Location CCK',
        'description' => 'Defines a Location field type.',
        'dependencies' => 
        array (
          0 => 'location',
        ),
        'package' => 'CCK',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'location_cck.module',
          1 => 'location_cck.install',
        ),
        'version' => '7.x-3.0-rc2+0-dev',
        'project' => 'location',
        'datestamp' => '1372780146',
        'php' => '5.2.4',
      ),
      'project' => 'location',
      'version' => '7.x-3.0-rc2+0-dev',
    ),
    'location_email' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/location/contrib/location_email/location_email.module',
      'name' => 'location_email',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Location Email',
        'package' => 'Location',
        'description' => 'Allows you to add an email address to a location.',
        'dependencies' => 
        array (
          0 => 'location',
        ),
        'core' => '7.x',
        'files' => 
        array (
          0 => 'location_email.module',
          1 => 'location_email.install',
          2 => 'location_email.views.inc',
        ),
        'version' => '7.x-3.0-rc2+0-dev',
        'project' => 'location',
        'datestamp' => '1372780146',
        'php' => '5.2.4',
      ),
      'project' => 'location',
      'version' => '7.x-3.0-rc2+0-dev',
    ),
    'location_entity' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/location/contrib/location_entity/location_entity.module',
      'name' => 'location_entity',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Location Entity',
        'description' => 'Provide information about Location field to Entity module.',
        'core' => '7.x',
        'package' => 'Location',
        'dependencies' => 
        array (
          0 => 'location',
        ),
        'files' => 
        array (
          0 => 'location_entity.module',
        ),
        'version' => '7.x-3.0-rc2+0-dev',
        'project' => 'location',
        'datestamp' => '1372780146',
        'php' => '5.2.4',
      ),
      'project' => 'location',
      'version' => '7.x-3.0-rc2+0-dev',
    ),
    'location_fax' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/location/contrib/location_fax/location_fax.module',
      'name' => 'location_fax',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Location Fax',
        'package' => 'Location',
        'description' => 'Allows you to add a fax number to a location.',
        'dependencies' => 
        array (
          0 => 'location',
        ),
        'core' => '7.x',
        'files' => 
        array (
          0 => 'location_fax.module',
          1 => 'location_fax.install',
          2 => 'location_fax.views.inc',
        ),
        'version' => '7.x-3.0-rc2+0-dev',
        'project' => 'location',
        'datestamp' => '1372780146',
        'php' => '5.2.4',
      ),
      'project' => 'location',
      'version' => '7.x-3.0-rc2+0-dev',
    ),
    'location_generate' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/location/contrib/location_generate/location_generate.module',
      'name' => 'location_generate',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Location Generate',
        'description' => 'Bulk assign random latitude and longitudes to nodes.',
        'package' => 'Development',
        'dependencies' => 
        array (
          0 => 'devel_generate',
          1 => 'location',
        ),
        'core' => '7.x',
        'files' => 
        array (
          0 => 'location_generate.module',
        ),
        'version' => '7.x-3.0-rc2+0-dev',
        'project' => 'location',
        'datestamp' => '1372780146',
        'php' => '5.2.4',
      ),
      'project' => 'location',
      'version' => '7.x-3.0-rc2+0-dev',
    ),
    'location_node' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/location/location_node.module',
      'name' => 'location_node',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Node Locations',
        'description' => 'Associate locations with nodes.',
        'dependencies' => 
        array (
          0 => 'location',
        ),
        'package' => 'Location',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'location_node.module',
          1 => 'location_node.install',
        ),
        'version' => '7.x-3.0-rc2+0-dev',
        'project' => 'location',
        'datestamp' => '1372780146',
        'php' => '5.2.4',
      ),
      'project' => 'location',
      'version' => '7.x-3.0-rc2+0-dev',
    ),
    'location_phone' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/location/contrib/location_phone/location_phone.module',
      'name' => 'location_phone',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Location Phone',
        'package' => 'Location',
        'description' => 'Allows you to add a phone number to a location.',
        'dependencies' => 
        array (
          0 => 'location',
        ),
        'core' => '7.x',
        'files' => 
        array (
          0 => 'location_phone.module',
          1 => 'location_phone.install',
          2 => 'location_phone.views.inc',
        ),
        'version' => '7.x-3.0-rc2+0-dev',
        'project' => 'location',
        'datestamp' => '1372780146',
        'php' => '5.2.4',
      ),
      'project' => 'location',
      'version' => '7.x-3.0-rc2+0-dev',
    ),
    'location_search' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/location/contrib/location_search/location_search.module',
      'name' => 'location_search',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Location Search',
        'package' => 'Location',
        'description' => 'Advanced search page for locations.',
        'dependencies' => 
        array (
          0 => 'search',
          1 => 'location',
        ),
        'core' => '7.x',
        'files' => 
        array (
          0 => 'location_search.module',
          1 => 'location_search.install',
          2 => 'location_search.admin.inc',
        ),
        'version' => '7.x-3.0-rc2+0-dev',
        'project' => 'location',
        'datestamp' => '1372780146',
        'php' => '5.2.4',
      ),
      'project' => 'location',
      'version' => '7.x-3.0-rc2+0-dev',
    ),
    'location_taxonomy' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/location/contrib/location_taxonomy/location_taxonomy.module',
      'name' => 'location_taxonomy',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Location Taxonomy',
        'description' => 'Associate locations with taxonomy terms.',
        'dependencies' => 
        array (
          0 => 'location',
          1 => 'taxonomy',
        ),
        'package' => 'Location',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'location_taxonomy.module',
          1 => 'location_taxonomy.install',
        ),
        'version' => '7.x-3.0-rc2+0-dev',
        'project' => 'location',
        'datestamp' => '1372780146',
        'php' => '5.2.4',
      ),
      'project' => 'location',
      'version' => '7.x-3.0-rc2+0-dev',
    ),
    'location_user' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/location/location_user.module',
      'name' => 'location_user',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'User Locations',
        'description' => 'Associate locations with users.',
        'dependencies' => 
        array (
          0 => 'location',
        ),
        'package' => 'Location',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'location_user.module',
          1 => 'location_user.install',
        ),
        'configure' => 'admin/config/people/accounts',
        'version' => '7.x-3.0-rc2+0-dev',
        'project' => 'location',
        'datestamp' => '1372780146',
        'php' => '5.2.4',
      ),
      'project' => 'location',
      'version' => '7.x-3.0-rc2+0-dev',
    ),
    'mcn_base_feature' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/features/mcn_base_feature/mcn_base_feature.module',
      'name' => 'mcn_base_feature',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'MCN Base Feature',
        'description' => 'Provides base tools for content editing and site administration.  Required by all features.',
        'core' => '7.x',
        'package' => 'Features',
        'version' => '7.x-2.0',
        'project' => 'mcn_base_feature',
        'dependencies' => 
        array (
          0 => 'admin_menu',
          1 => 'admin_menu_toolbar',
          2 => 'block',
          3 => 'colorbox',
          4 => 'context',
          5 => 'context_ui',
          6 => 'contextual',
          7 => 'ctools',
          8 => 'features',
          9 => 'field_group',
          10 => 'file_entity',
          11 => 'googleanalytics',
          12 => 'image',
          13 => 'libraries',
          14 => 'link',
          15 => 'media',
          16 => 'mediafield',
          17 => 'menu',
          18 => 'node',
          19 => 'node_reference',
          20 => 'overlay',
          21 => 'path',
          22 => 'pathauto',
          23 => 'plupload',
          24 => 'references',
          25 => 'scheduler',
          26 => 'shortcut',
          27 => 'strongarm',
          28 => 'system',
          29 => 'taxonomy',
          30 => 'token',
          31 => 'toolbar',
          32 => 'transliteration',
          33 => 'user',
        ),
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'strongarm:strongarm:1',
          ),
          'features_api' => 
          array (
            0 => 'api:2',
          ),
          'taxonomy' => 
          array (
            0 => 'tags',
          ),
          'user_permission' => 
          array (
            0 => 'access administration menu',
            1 => 'access administration pages',
            2 => 'access content',
            3 => 'access content overview',
            4 => 'access contextual links',
            5 => 'access overlay',
            6 => 'access site in maintenance mode',
            7 => 'access site reports',
            8 => 'access toolbar',
            9 => 'access user profiles',
            10 => 'administer actions',
            11 => 'administer blocks',
            12 => 'administer content types',
            13 => 'administer features',
            14 => 'administer fieldgroups',
            15 => 'administer files',
            16 => 'administer google analytics',
            17 => 'administer image styles',
            18 => 'administer menu',
            19 => 'administer modules',
            20 => 'administer nodes',
            21 => 'administer pathauto',
            22 => 'administer permissions',
            23 => 'administer scheduler',
            24 => 'administer shortcuts',
            25 => 'administer site configuration',
            26 => 'administer software updates',
            27 => 'administer taxonomy',
            28 => 'administer themes',
            29 => 'administer url aliases',
            30 => 'administer users',
            31 => 'block IP addresses',
            32 => 'bypass node access',
            33 => 'cancel account',
            34 => 'change own username',
            35 => 'create files',
            36 => 'create url aliases',
            37 => 'customize shortcut links',
            38 => 'delete terms in tags',
            39 => 'display drupal links',
            40 => 'edit terms in tags',
            41 => 'flush caches',
            42 => 'import media',
            43 => 'manage features',
            44 => 'notify of path changes',
            45 => 'opt-in or out of tracking',
            46 => 'schedule (un)publishing of nodes',
            47 => 'select account cancellation method',
            48 => 'switch shortcut sets',
            49 => 'use PHP for tracking visibility',
            50 => 'view own unpublished content',
            51 => 'view the administration theme',
          ),
          'user_role' => 
          array (
            0 => 'account manager',
            1 => 'administrator',
            2 => 'client',
            3 => 'developer',
          ),
          'variable' => 
          array (
            0 => 'configurable_timezones',
            1 => 'image_jpeg_quality',
            2 => 'image_toolkit',
            3 => 'pathauto_node_pattern',
            4 => 'site_frontpage',
            5 => 'user_register',
          ),
        ),
        'project status url' => 'http://features.devmcn.com/fserver',
        'php' => '5.2.4',
      ),
      'project' => 'mcn_base_feature',
      'version' => '7.x-2.0',
    ),
    'mcn_wysiwyg' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/features/mcn_wysiwyg/mcn_wysiwyg.module',
      'name' => 'mcn_wysiwyg',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'MCN wysiwyg',
        'description' => 'wysiwyg configuration',
        'core' => '7.x',
        'package' => 'Features',
        'php' => '5.2.4',
        'version' => '7.x-2.0',
        'project' => 'mcn_wysiwyg',
        'dependencies' => 
        array (
          0 => 'better_formats',
          1 => 'ckeditor_link',
          2 => 'features',
          3 => 'image_resize_filter',
          4 => 'mcn_base_feature',
          5 => 'media',
          6 => 'strongarm',
          7 => 'wysiwyg',
        ),
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'strongarm:strongarm:1',
          ),
          'features_api' => 
          array (
            0 => 'api:2',
          ),
          'filter' => 
          array (
            0 => 'full_html',
          ),
          'user_permission' => 
          array (
            0 => 'access ckeditor link',
            1 => 'administer ckeditor link',
            2 => 'administer filters',
            3 => 'show format selection for file',
            4 => 'show format selection for node',
            5 => 'show format selection for taxonomy_term',
            6 => 'show format selection for user',
            7 => 'show format tips',
            8 => 'show more format tips link',
            9 => 'use text format full_html',
          ),
          'variable' => 
          array (
            0 => 'better_formats_per_field_core',
            1 => 'ckeditor_link_type_selected',
            2 => 'filter_fallback_format',
          ),
          'wysiwyg' => 
          array (
            0 => 'full_html',
          ),
        ),
        'project status url' => 'http://features.devmcn.com/fserver',
      ),
      'project' => 'mcn_wysiwyg',
      'version' => '7.x-2.0',
    ),
    'media' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib-patched/media/media.module',
      'name' => 'media',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7212',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Media',
        'description' => 'Provides the core Media API',
        'package' => 'Media',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'file_entity',
          1 => 'image',
          2 => 'views',
        ),
        'files' => 
        array (
          0 => 'includes/MediaReadOnlyStreamWrapper.inc',
          1 => 'includes/MediaBrowserPluginInterface.inc',
          2 => 'includes/MediaBrowserPlugin.inc',
          3 => 'includes/MediaBrowserUpload.inc',
          4 => 'includes/MediaBrowserView.inc',
          5 => 'includes/MediaEntityTranslationHandler.inc',
          6 => 'includes/media_views_plugin_display_media_browser.inc',
          7 => 'includes/media_views_plugin_style_media_browser.inc',
          8 => 'tests/media.test',
          9 => 'tests/media.entity.test',
          10 => 'tests/media.file.usage.test',
        ),
        'configure' => 'admin/config/media/browser',
        'version' => '7.x-2.0-alpha1+0-dev',
        'project' => 'media',
        'datestamp' => '1376271869',
        'php' => '5.2.4',
      ),
      'project' => 'media',
      'version' => '7.x-2.0-alpha1+0-dev',
    ),
    'mediaelement' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib-patched/mediaelement/mediaelement.module',
      'name' => 'mediaelement',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'MediaElement.js',
        'description' => 'Provide MediaElement.js to be used on the entire site or just with Filefields.',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'libraries',
        ),
        'files' => 
        array (
          0 => 'mediaelement.admin.inc',
          1 => 'mediaelement.install',
          2 => 'mediaelement.module',
        ),
        'configure' => 'admin/config/media/mediaelement',
        'version' => '7.x-1.2',
        'project' => 'mediaelement',
        'datestamp' => '1326568843',
        'php' => '5.2.4',
      ),
      'project' => 'mediaelement',
      'version' => '7.x-1.2',
    ),
    'mediafield' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib-patched/media/modules/mediafield/mediafield.module',
      'name' => 'mediafield',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Media Field',
        'description' => 'Provides a field type that stores media-specific data. <em>Deprecated by the core File field type.</em>',
        'package' => 'Media',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'media',
        ),
        'version' => '7.x-2.0-alpha1+0-dev',
        'project' => 'media',
        'datestamp' => '1376271869',
        'php' => '5.2.4',
      ),
      'project' => 'media',
      'version' => '7.x-2.0-alpha1+0-dev',
    ),
    'media_browser_plus' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/media_browser_plus/media_browser_plus.module',
      'name' => 'media_browser_plus',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Media Browser Plus for Media 2.x',
        'description' => 'Provides better UX for the media browser',
        'package' => 'Media',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'media',
          1 => 'taxonomy',
          2 => 'multiform',
          3 => 'colorbox',
        ),
        'files' => 
        array (
          0 => 'includes/MediaBrowserPlusLibrary.inc',
          1 => 'tests/media_browser_plus.test',
        ),
        'version' => '7.x-2.0-dev1+11-dev',
        'project' => 'media_browser_plus',
        'datestamp' => '1373483736',
        'php' => '5.2.4',
      ),
      'project' => 'media_browser_plus',
      'version' => '7.x-2.0-dev1+11-dev',
    ),
    'media_internet' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib-patched/media/modules/media_internet/media_internet.module',
      'name' => 'media_internet',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Media Internet Sources',
        'description' => 'Provides an API for accessing media on various internet services',
        'package' => 'Media',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'media',
        ),
        'files' => 
        array (
          0 => 'includes/MediaBrowserInternet.inc',
        ),
        'version' => '7.x-2.0-alpha1+0-dev',
        'project' => 'media',
        'datestamp' => '1376271869',
        'php' => '5.2.4',
      ),
      'project' => 'media',
      'version' => '7.x-2.0-alpha1+0-dev',
    ),
    'media_vimeo' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/media_vimeo/media_vimeo.module',
      'name' => 'media_vimeo',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Media: Vimeo',
        'description' => 'Provides Vimeo support to the Media module.',
        'package' => 'Media',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'media_vimeo.module',
          1 => 'includes/MediaInternetVimeoHandler.inc',
          2 => 'includes/MediaVimeoStreamWrapper.inc',
          3 => 'includes/media_vimeo.formatters.inc',
          4 => 'includes/media_vimeo.variables.inc',
        ),
        'dependencies' => 
        array (
          0 => 'media',
          1 => 'media_internet',
        ),
        'version' => '7.x-2.0-rc1+0-dev',
        'project' => 'media_vimeo',
        'datestamp' => '1374687215',
        'php' => '5.2.4',
      ),
      'project' => 'media_vimeo',
      'version' => '7.x-2.0-rc1+0-dev',
    ),
    'media_wysiwyg_view_mode' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib-patched/media/modules/media_wysiwyg_view_mode/media_wysiwyg_view_mode.module',
      'name' => 'media_wysiwyg_view_mode',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Media WYSIWYG View Mode',
        'description' => 'Enables files inside of the WYSIWYG editor to be displayed using a separate view mode.',
        'package' => 'Media',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'media',
          1 => 'wysiwyg',
        ),
        'configure' => 'admin/config/media/wysiwyg-view-mode',
        'version' => '7.x-2.0-alpha1+0-dev',
        'project' => 'media',
        'datestamp' => '1376271869',
        'php' => '5.2.4',
      ),
      'project' => 'media',
      'version' => '7.x-2.0-alpha1+0-dev',
    ),
    'media_youtube' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/media_youtube/media_youtube.module',
      'name' => 'media_youtube',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Media: YouTube',
        'description' => 'Provides YouTube support for File Entity and the Media module.',
        'package' => 'Media',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'includes/MediaInternetYouTubeHandler.inc',
          1 => 'includes/MediaYouTubeStreamWrapper.inc',
          2 => 'includes/MediaYouTubeBrowser.inc',
        ),
        'dependencies' => 
        array (
          0 => 'media',
          1 => 'media_internet',
        ),
        'version' => '7.x-2.0-rc2+3-dev',
        'project' => 'media_youtube',
        'datestamp' => '1364001177',
        'php' => '5.2.4',
      ),
      'project' => 'media_youtube',
      'version' => '7.x-2.0-rc2+3-dev',
    ),
    'menu' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/menu/menu.module',
      'name' => 'menu',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7003',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Menu',
        'description' => 'Allows administrators to customize the site navigation menu.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'menu.test',
        ),
        'configure' => 'admin/structure/menu',
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'menu_attributes' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/menu_attributes/menu_attributes.module',
      'name' => 'menu_attributes',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Menu attributes',
        'description' => 'Allows administrators to specify custom attributes for menu items.',
        'dependencies' => 
        array (
          0 => 'menu',
        ),
        'core' => '7.x',
        'configure' => 'admin/structure/menu/settings',
        'files' => 
        array (
          0 => 'menu_attributes.test',
        ),
        'version' => '7.x-1.0-rc2',
        'project' => 'menu_attributes',
        'datestamp' => '1338541556',
        'php' => '5.2.4',
      ),
      'project' => 'menu_attributes',
      'version' => '7.x-1.0-rc2',
    ),
    'menu_block' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/menu_block/menu_block.module',
      'name' => 'menu_block',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Menu Block',
        'description' => 'Provides configurable blocks of menu items.',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'menu (>7.11)',
        ),
        'files' => 
        array (
          0 => 'menu_block.module',
          1 => 'menu_block.admin.inc',
          2 => 'menu_block.follow.inc',
          3 => 'menu_block.pages.inc',
          4 => 'menu_block.sort.inc',
          5 => 'menu_block.install',
          6 => 'plugins/content_types/menu_tree/menu_tree.inc',
        ),
        'configure' => 'admin/config/user-interface/menu-block',
        'version' => '7.x-2.3',
        'project' => 'menu_block',
        'datestamp' => '1328286646',
        'php' => '5.2.4',
      ),
      'project' => 'menu_block',
      'version' => '7.x-2.3',
    ),
    'menu_block_export' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/menu_block/menu_block_export.module',
      'name' => 'menu_block_export',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Menu Block Export',
        'description' => 'Provides export interface for Menu block module.',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'menu_block',
        ),
        'files' => 
        array (
          0 => 'menu_block_export.module',
          1 => 'menu_block_export.admin.inc',
        ),
        'configure' => 'admin/config/user-interface/menu-block/export',
        'version' => '7.x-2.3',
        'project' => 'menu_block',
        'datestamp' => '1328286646',
        'php' => '5.2.4',
      ),
      'project' => 'menu_block',
      'version' => '7.x-2.3',
    ),
    'menu_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/menu_test.module',
      'name' => 'menu_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Hook menu tests',
        'description' => 'Support module for menu hook testing.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'migrate' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/migrate/migrate.module',
      'name' => 'migrate',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Migrate',
        'description' => 'Import content from external sources',
        'package' => 'Migration',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'includes/base.inc',
          1 => 'includes/field_mapping.inc',
          2 => 'includes/migration.inc',
          3 => 'includes/destination.inc',
          4 => 'includes/exception.inc',
          5 => 'includes/group.inc',
          6 => 'includes/handler.inc',
          7 => 'includes/map.inc',
          8 => 'includes/source.inc',
          9 => 'includes/team.inc',
          10 => 'migrate.mail.inc',
          11 => 'plugins/destinations/entity.inc',
          12 => 'plugins/destinations/term.inc',
          13 => 'plugins/destinations/user.inc',
          14 => 'plugins/destinations/node.inc',
          15 => 'plugins/destinations/comment.inc',
          16 => 'plugins/destinations/file.inc',
          17 => 'plugins/destinations/path.inc',
          18 => 'plugins/destinations/fields.inc',
          19 => 'plugins/destinations/poll.inc',
          20 => 'plugins/destinations/table.inc',
          21 => 'plugins/destinations/table_copy.inc',
          22 => 'plugins/destinations/menu.inc',
          23 => 'plugins/destinations/menu_links.inc',
          24 => 'plugins/destinations/statistics.inc',
          25 => 'plugins/sources/csv.inc',
          26 => 'plugins/sources/files.inc',
          27 => 'plugins/sources/json.inc',
          28 => 'plugins/sources/list.inc',
          29 => 'plugins/sources/multiitems.inc',
          30 => 'plugins/sources/sql.inc',
          31 => 'plugins/sources/sqlmap.inc',
          32 => 'plugins/sources/mssql.inc',
          33 => 'plugins/sources/oracle.inc',
          34 => 'plugins/sources/xml.inc',
          35 => 'tests/import/options.test',
          36 => 'tests/plugins/destinations/comment.test',
          37 => 'tests/plugins/destinations/node.test',
          38 => 'tests/plugins/destinations/table.test',
          39 => 'tests/plugins/destinations/term.test',
          40 => 'tests/plugins/destinations/user.test',
          41 => 'tests/plugins/sources/xml.test',
        ),
        'version' => '7.x-2.6-rc1',
        'project' => 'migrate',
        'datestamp' => '1372276551',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'migrate',
      'version' => '7.x-2.6-rc1',
    ),
    'migrate_example' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/migrate/migrate_example/migrate_example.module',
      'name' => 'migrate_example',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Migrate Example',
        'description' => 'Example migration data.',
        'package' => 'Migration',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'taxonomy',
          1 => 'image',
          2 => 'comment',
          3 => 'migrate',
          4 => 'list',
          5 => 'number',
        ),
        'files' => 
        array (
          0 => 'migrate_example.module',
          1 => 'beer.inc',
          2 => 'wine.inc',
        ),
        'version' => '7.x-2.6-rc1',
        'project' => 'migrate',
        'datestamp' => '1372276551',
        'php' => '5.2.4',
      ),
      'project' => 'migrate',
      'version' => '7.x-2.6-rc1',
    ),
    'migrate_example_baseball' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/migrate/migrate_example_baseball/migrate_example_baseball.module',
      'name' => 'migrate_example_baseball',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'features',
          1 => 'migrate',
          2 => 'number',
        ),
        'description' => 'Import baseball box scores.',
        'features' => 
        array (
          'field' => 
          array (
            0 => 'node-migrate_example_baseball-body',
            1 => 'node-migrate_example_baseball-field_attendance',
            2 => 'node-migrate_example_baseball-field_duration',
            3 => 'node-migrate_example_baseball-field_home_batters',
            4 => 'node-migrate_example_baseball-field_home_game_number',
            5 => 'node-migrate_example_baseball-field_home_pitcher',
            6 => 'node-migrate_example_baseball-field_home_score',
            7 => 'node-migrate_example_baseball-field_home_team',
            8 => 'node-migrate_example_baseball-field_outs',
            9 => 'node-migrate_example_baseball-field_park',
            10 => 'node-migrate_example_baseball-field_start_date',
            11 => 'node-migrate_example_baseball-field_visiting_batters',
            12 => 'node-migrate_example_baseball-field_visiting_pitcher',
            13 => 'node-migrate_example_baseball-field_visiting_score',
            14 => 'node-migrate_example_baseball-field_visiting_team',
          ),
          'node' => 
          array (
            0 => 'migrate_example_baseball',
          ),
        ),
        'files' => 
        array (
          0 => 'migrate_example_baseball.migrate.inc',
        ),
        'name' => 'migrate_example_baseball',
        'package' => 'Migration',
        'php' => '5.2.4',
        'version' => '7.x-2.6-rc1',
        'project' => 'migrate',
        'datestamp' => '1372276551',
      ),
      'project' => 'migrate',
      'version' => '7.x-2.6-rc1',
    ),
    'migrate_example_oracle' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/migrate/migrate_example/migrate_example_oracle/migrate_example_oracle.module',
      'name' => 'migrate_example_oracle',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'features',
          1 => 'image',
          2 => 'migrate',
        ),
        'description' => 'Content type supporting example of Oracle migration',
        'features' => 
        array (
          'field' => 
          array (
            0 => 'node-migrate_example_oracle-body',
            1 => 'node-migrate_example_oracle-field_mainimage',
          ),
          'node' => 
          array (
            0 => 'migrate_example_oracle',
          ),
        ),
        'files' => 
        array (
          0 => 'migrate_example_oracle.migrate.inc',
        ),
        'name' => 'Migrate example - Oracle',
        'package' => 'Migration',
        'project' => 'migrate',
        'version' => '7.x-2.6-rc1',
        'datestamp' => '1372276551',
        'php' => '5.2.4',
      ),
      'project' => 'migrate',
      'version' => '7.x-2.6-rc1',
    ),
    'migrate_extras' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/migrate_extras/migrate_extras.module',
      'name' => 'migrate_extras',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Migrate Extras',
        'description' => 'Adds migrate module integration with contrib modules and other miscellaneous tweaks.',
        'core' => '7.x',
        'package' => 'Development',
        'dependencies' => 
        array (
          0 => 'migrate',
        ),
        'files' => 
        array (
          0 => 'addressfield.inc',
          1 => 'cck_phone.inc',
          2 => 'entity_api.inc',
          3 => 'flag.inc',
          4 => 'geofield.inc',
          5 => 'interval.inc',
          6 => 'media.inc',
          7 => 'name.inc',
          8 => 'pathauto.inc',
          9 => 'privatemsg.inc',
          10 => 'profile2.inc',
          11 => 'rules.inc',
          12 => 'user_relationships.inc',
          13 => 'votingapi.inc',
          14 => 'webform.inc',
          15 => 'tests/pathauto.test',
        ),
        'version' => '7.x-2.5',
        'project' => 'migrate_extras',
        'datestamp' => '1352299013',
        'php' => '5.2.4',
      ),
      'project' => 'migrate_extras',
      'version' => '7.x-2.5',
    ),
    'migrate_extras_media' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/migrate_extras/migrate_extras_examples/migrate_extras_media/migrate_extras_media.module',
      'name' => 'migrate_extras_media',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'features',
          1 => 'file',
          2 => 'media',
          3 => 'media_youtube',
          4 => 'migrate',
          5 => 'migrate_extras',
        ),
        'description' => 'Examples for migrating Media',
        'features' => 
        array (
          'field' => 
          array (
            0 => 'node-migrate_extras_media_example-body',
            1 => 'node-migrate_extras_media_example-field_document',
            2 => 'node-migrate_extras_media_example-field_media_image',
            3 => 'node-migrate_extras_media_example-field_youtube_video',
          ),
          'node' => 
          array (
            0 => 'migrate_extras_media_example',
          ),
        ),
        'files' => 
        array (
          0 => 'migrate_extras_media.migrate.inc',
        ),
        'name' => 'Migrate Extras Media',
        'package' => 'Migrate Examples',
        'version' => '7.x-2.5',
        'project' => 'migrate_extras',
        'datestamp' => '1352299013',
        'php' => '5.2.4',
      ),
      'project' => 'migrate_extras',
      'version' => '7.x-2.5',
    ),
    'migrate_extras_pathauto' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/migrate_extras/migrate_extras_examples/migrate_extras_pathauto/migrate_extras_pathauto.module',
      'name' => 'migrate_extras_pathauto',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'features',
          1 => 'migrate_extras',
          2 => 'pathauto',
        ),
        'description' => 'Examples of migrating with the Pathauto module',
        'features' => 
        array (
          'field' => 
          array (
            0 => 'node-migrate_example_pathauto-body',
          ),
          'node' => 
          array (
            0 => 'migrate_example_pathauto',
          ),
        ),
        'files' => 
        array (
          0 => 'migrate_extras_pathauto.migrate.inc',
        ),
        'name' => 'Migrate Extras Pathauto Example',
        'package' => 'Migrate Examples',
        'project' => 'migrate_extras',
        'version' => '7.x-2.5',
        'datestamp' => '1352299013',
        'php' => '5.2.4',
      ),
      'project' => 'migrate_extras',
      'version' => '7.x-2.5',
    ),
    'migrate_extras_profile2' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/migrate_extras/migrate_extras_examples/migrate_extras_profile2/migrate_extras_profile2.module',
      'name' => 'migrate_extras_profile2',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'migrate_extras',
          1 => 'profile2',
        ),
        'description' => 'Examples of migrating into Profile2 entities',
        'files' => 
        array (
          0 => 'migrate_extras_profile2.migrate.inc',
        ),
        'name' => 'Migrate Extras Profile2 Example',
        'package' => 'Migrate Examples',
        'version' => '7.x-2.5',
        'project' => 'migrate_extras',
        'datestamp' => '1352299013',
        'php' => '5.2.4',
      ),
      'project' => 'migrate_extras',
      'version' => '7.x-2.5',
    ),
    'migrate_ui' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/migrate/migrate_ui/migrate_ui.module',
      'name' => 'migrate_ui',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Migrate UI',
        'description' => 'UI for managing migration processes',
        'package' => 'Migration',
        'configure' => 'admin/content/migrate/configure',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'migrate',
        ),
        'files' => 
        array (
          0 => 'migrate_ui.wizard.inc',
        ),
        'version' => '7.x-2.6-rc1',
        'project' => 'migrate',
        'datestamp' => '1372276551',
        'php' => '5.2.4',
      ),
      'project' => 'migrate',
      'version' => '7.x-2.6-rc1',
    ),
    'module_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/module_test.module',
      'name' => 'module_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Module test',
        'description' => 'Support module for module system testing.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'mollom' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/mollom/mollom.module',
      'name' => 'mollom',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Mollom',
        'description' => 'Automatically moderates user-submitted content and protects your site from spam and profanity.',
        'core' => '7.x',
        'configure' => 'admin/config/content/mollom',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'mollom.css',
          ),
        ),
        'files' => 
        array (
          0 => 'includes/mollom.class.inc',
          1 => 'mollom.drupal.inc',
          2 => 'tests/mollom.test',
          3 => 'tests/mollom.class.test',
        ),
        'version' => '7.x-2.7',
        'project' => 'mollom',
        'datestamp' => '1372682452',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'mollom',
      'version' => '7.x-2.7',
    ),
    'mollom_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/mollom/tests/mollom_test.module',
      'name' => 'mollom_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Mollom Test',
        'description' => 'Testing module for Mollom functionality. Do not enable.',
        'core' => '7.x',
        'package' => 'Testing',
        'hidden' => true,
        'version' => '7.x-2.7',
        'project' => 'mollom',
        'datestamp' => '1372682452',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'mollom',
      'version' => '7.x-2.7',
    ),
    'mollom_test_server' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/mollom/tests/mollom_test_server.module',
      'name' => 'mollom_test_server',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Mollom Test Server',
        'description' => 'Testing Server module for Mollom functionality. Do not enable.',
        'core' => '7.x',
        'package' => 'Testing',
        'hidden' => true,
        'version' => '7.x-2.7',
        'project' => 'mollom',
        'datestamp' => '1372682452',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'mollom',
      'version' => '7.x-2.7',
    ),
    'multiform' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/multiform/multiform.module',
      'name' => 'multiform',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Multiple forms',
        'description' => 'Allows displaying and submitting multiple forms in one form',
        'files' => 
        array (
          0 => 'multiform.module',
          1 => 'tests/multiform.test',
        ),
        'core' => '7.x',
        'version' => '7.x-1.0',
        'project' => 'multiform',
        'datestamp' => '1327823749',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'multiform',
      'version' => '7.x-1.0',
    ),
    'multiform_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/multiform/tests/multiform_test.module',
      'name' => 'multiform_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Multiform test',
        'description' => 'Handle module for testing Multiform',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'multiform',
        ),
        'hidden' => true,
        'files' => 
        array (
          0 => 'multiform_test.module',
        ),
        'version' => '7.x-1.0',
        'project' => 'multiform',
        'datestamp' => '1327823749',
        'php' => '5.2.4',
      ),
      'project' => 'multiform',
      'version' => '7.x-1.0',
    ),
    'multiselect' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/multiselect/multiselect.module',
      'name' => 'multiselect',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Multiselect',
        'description' => 'Defines a CCK multiple selection field widget, to allow easier multi-selection for users.',
        'package' => 'CCK',
        'core' => '7.x',
        'version' => '7.x-1.9',
        'files' => 
        array (
          0 => 'multiselect.module',
        ),
        'project' => 'multiselect',
        'datestamp' => '1336842370',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'multiselect',
      'version' => '7.x-1.9',
    ),
    'multiselect_fapi_example' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/multiselect/multiselect_fapi_example/multiselect_fapi_example.module',
      'name' => 'multiselect_fapi_example',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Multiselect FAPI Example',
        'description' => 'Example usage of the Multiselect module via Form API calls.',
        'package' => 'Example modules',
        'core' => '7.x',
        'version' => '7.x-1.9',
        'files' => 
        array (
          0 => 'multiselect_fapi_example.module',
        ),
        'project' => 'multiselect',
        'datestamp' => '1336842370',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'multiselect',
      'version' => '7.x-1.9',
    ),
    'news' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/features/news/news.module',
      'name' => 'news',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'News',
        'description' => 'Includes News Article Content Type and a sidebar block display and news page/feed.',
        'core' => '7.x',
        'package' => 'Base MCN Features',
        'php' => '5.2.4',
        'version' => '7.x-3.0',
        'project' => 'news',
        'dependencies' => 
        array (
          0 => 'advertisement_banners',
          1 => 'basic_page',
          2 => 'context',
          3 => 'ctools',
          4 => 'eva',
          5 => 'features',
          6 => 'field_group',
          7 => 'link',
          8 => 'list',
          9 => 'mcn_base_feature',
          10 => 'mcn_wysiwyg',
          11 => 'node',
          12 => 'options',
          13 => 'strongarm',
          14 => 'taxonomy',
          15 => 'text',
          16 => 'views',
          17 => 'views_feature',
        ),
        'features' => 
        array (
          'context' => 
          array (
            0 => 'home_news_block',
            1 => 'news_block',
          ),
          'ctools' => 
          array (
            0 => 'context:context:3',
            1 => 'field_group:field_group:1',
            2 => 'strongarm:strongarm:1',
            3 => 'views:views_default:3.0',
          ),
          'features_api' => 
          array (
            0 => 'api:2',
          ),
          'field_base' => 
          array (
            0 => 'field_external_link',
            1 => 'field_tags',
          ),
          'field_instance' => 
          array (
            0 => 'node-news_article-body',
            1 => 'node-news_article-field_external_link',
            2 => 'node-news_article-field_tags',
            3 => 'node-news_article-field_target',
          ),
          'node' => 
          array (
            0 => 'news_article',
          ),
          'user_permission' => 
          array (
            0 => 'create news_article content',
            1 => 'delete any news_article content',
            2 => 'delete own news_article content',
            3 => 'edit any news_article content',
            4 => 'edit own news_article content',
          ),
          'variable' => 
          array (
            0 => 'comment_anonymous_news_article',
            1 => 'comment_default_mode_news_article',
            2 => 'comment_default_per_page_news_article',
            3 => 'comment_form_location_news_article',
            4 => 'comment_news_article',
            5 => 'comment_preview_news_article',
            6 => 'comment_subject_field_news_article',
            7 => 'menu_options_news_article',
            8 => 'menu_parent_news_article',
            9 => 'node_options_news_article',
            10 => 'node_preview_news_article',
            11 => 'node_submitted_news_article',
            12 => 'pathauto_node_news_article_pattern',
            13 => 'scheduler_publish_enable_news_article',
            14 => 'scheduler_publish_touch_news_article',
            15 => 'scheduler_unpublish_enable_news_article',
          ),
          'views_view' => 
          array (
            0 => 'news',
          ),
        ),
        'project status url' => 'http://features.devmcn.com/fserver',
      ),
      'project' => 'news',
      'version' => '7.x-3.0',
    ),
    'nice_menus' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/nice_menus/nice_menus.module',
      'name' => 'nice_menus',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Nice Menus',
        'description' => 'CSS/jQuery drop-down, drop-right and drop-left menus to be placed in blocks',
        'dependencies' => 
        array (
          0 => 'menu',
        ),
        'core' => '7.x',
        'configure' => 'admin/config/user-interface/nice_menus',
        'version' => '7.x-2.5',
        'project' => 'nice_menus',
        'datestamp' => '1373943668',
        'php' => '5.2.4',
      ),
      'project' => 'nice_menus',
      'version' => '7.x-2.5',
    ),
    'node' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/node/node.module',
      'name' => 'node',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7013',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Node',
        'description' => 'Allows content to be submitted to the site and displayed on pages.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'node.module',
          1 => 'node.test',
        ),
        'required' => true,
        'configure' => 'admin/structure/types',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'node.css',
          ),
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'nodequeue' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/nodequeue/nodequeue.module',
      'name' => 'nodequeue',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Nodequeue',
        'description' => 'Create queues which can contain nodes in arbitrary order',
        'package' => 'Nodequeue',
        'core' => '7.x',
        'configure' => 'admin/structure/nodequeue/settings',
        'files' => 
        array (
          0 => 'includes/views/nodequeue.views.inc',
          1 => 'includes/nodequeue.actions.inc',
          2 => 'includes/views/nodequeue_handler_argument_subqueue_qid.inc',
          3 => 'includes/views/nodequeue_handler_field_all_queues.inc',
          4 => 'includes/views/nodequeue_handler_field_all_subqueues.inc',
          5 => 'includes/views/nodequeue_handler_field_links.inc',
          6 => 'includes/views/nodequeue_handler_field_queue_tab.inc',
          7 => 'includes/views/nodequeue_handler_filter_in_queue.inc',
          8 => 'includes/views/nodequeue_handler_relationship_nodequeue.inc',
        ),
        'version' => '7.x-2.0-beta1',
        'project' => 'nodequeue',
        'datestamp' => '1316558104',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'nodequeue',
      'version' => '7.x-2.0-beta1',
    ),
    'nodequeue_generate' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/nodequeue/nodequeue_generate.module',
      'name' => 'nodequeue_generate',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Nodequeue generate',
        'description' => 'Bulk assign nodes into queues for quickly populating a site.',
        'package' => 'Development',
        'dependencies' => 
        array (
          0 => 'nodequeue',
        ),
        'core' => '7.x',
        'files' => 
        array (
          0 => 'nodequeue_generate.module',
        ),
        'version' => '7.x-2.0-beta1',
        'project' => 'nodequeue',
        'datestamp' => '1316558104',
        'php' => '5.2.4',
      ),
      'project' => 'nodequeue',
      'version' => '7.x-2.0-beta1',
    ),
    'nodequeue_service' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/nodequeue/addons/nodequeue_service/nodequeue_service.module',
      'name' => 'nodequeue_service',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Nodequeue Service',
        'description' => 'Provides a nodequeue service.',
        'package' => 'Nodequeue',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'services',
          1 => 'nodequeue',
        ),
        'files' => 
        array (
          0 => 'nodequeue_service.inc',
        ),
        'version' => '7.x-2.0-beta1',
        'project' => 'nodequeue',
        'datestamp' => '1316558104',
        'php' => '5.2.4',
      ),
      'project' => 'nodequeue',
      'version' => '7.x-2.0-beta1',
    ),
    'nodereference_url' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/nodereference_url/nodereference_url.module',
      'name' => 'nodereference_url',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Node Reference URL Widget',
        'description' => 'Adds an additional widget to the Node Reference field that prepopulates a reference by the URL.',
        'dependencies' => 
        array (
          0 => 'node_reference',
        ),
        'files' => 
        array (
          0 => 'nodereference_url.module',
        ),
        'package' => 'Fields',
        'core' => '7.x',
        'version' => '7.x-1.12',
        'project' => 'nodereference_url',
        'datestamp' => '1316903507',
        'php' => '5.2.4',
      ),
      'project' => 'nodereference_url',
      'version' => '7.x-1.12',
    ),
    'node_access_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/node/tests/node_access_test.module',
      'name' => 'node_access_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Node module access tests',
        'description' => 'Support module for node permission testing.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'node_reference' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/references/node_reference/node_reference.module',
      'name' => 'node_reference',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7000',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Node Reference',
        'description' => 'Defines a field type for referencing one node from another.',
        'package' => 'Fields',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'field',
          1 => 'references',
          2 => 'options',
        ),
        'files' => 
        array (
          0 => 'node_reference.test',
        ),
        'version' => '7.x-2.1',
        'project' => 'references',
        'datestamp' => '1360265821',
        'php' => '5.2.4',
      ),
      'project' => 'references',
      'version' => '7.x-2.1',
    ),
    'node_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/node/tests/node_test.module',
      'name' => 'node_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Node module tests',
        'description' => 'Support module for node related testing.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'node_test_exception' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/node/tests/node_test_exception.module',
      'name' => 'node_test_exception',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Node module exception tests',
        'description' => 'Support module for node related exception testing.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'number' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/field/modules/number/number.module',
      'name' => 'number',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Number',
        'description' => 'Defines numeric field types.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'field',
        ),
        'files' => 
        array (
          0 => 'number.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'oauth_common' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/oauth/oauth_common.module',
      'name' => 'oauth_common',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'OAuth',
        'description' => 'Provides OAuth functionality',
        'configure' => 'admin/config/services/oauth',
        'package' => 'OAuth',
        'recommends' => 
        array (
          0 => 'ctools',
        ),
        'suggests' => 
        array (
          0 => 'inputstream',
        ),
        'core' => '7.x',
        'files' => 
        array (
          0 => 'lib/OAuth.php',
          1 => 'includes/DrupalOAuthServer.inc',
          2 => 'includes/DrupalOAuthDataStore.inc',
          3 => 'includes/DrupalOAuthRequest.inc',
          4 => 'includes/DrupalOAuthToken.inc',
          5 => 'includes/DrupalOAuthConsumer.inc',
          6 => 'includes/DrupalOAuthClient.inc',
          7 => 'includes/OAuthSignatureMethod_HMAC.inc',
        ),
        'version' => '7.x-3.1',
        'project' => 'oauth',
        'datestamp' => '1359936562',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'oauth',
      'version' => '7.x-3.1',
    ),
    'oauth_common_providerui' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/oauth/oauth_common_providerui.module',
      'name' => 'oauth_common_providerui',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'OAuth Provider UI',
        'description' => 'Provides a UI for when OAuth is acting as a provider.',
        'package' => 'OAuth',
        'dependencies' => 
        array (
          0 => 'oauth_common',
        ),
        'core' => '7.x',
        'version' => '7.x-3.1',
        'project' => 'oauth',
        'datestamp' => '1359936562',
        'php' => '5.2.4',
      ),
      'project' => 'oauth',
      'version' => '7.x-3.1',
    ),
    'openid' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/openid/openid.module',
      'name' => 'openid',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'OpenID',
        'description' => 'Allows users to log into your site using OpenID.',
        'version' => '7.23',
        'package' => 'Core',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'openid.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'openid_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/openid/tests/openid_test.module',
      'name' => 'openid_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'OpenID dummy provider',
        'description' => 'OpenID provider used for testing.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'openid',
        ),
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'options' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/field/modules/options/options.module',
      'name' => 'options',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Options',
        'description' => 'Defines selection, check box and radio button widgets for text and numeric fields.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'field',
        ),
        'files' => 
        array (
          0 => 'options.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'overlay' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/overlay/overlay.module',
      'name' => 'overlay',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '1',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Overlay',
        'description' => 'Displays the Drupal administration interface in an overlay.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'page_manager' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib-patched/ctools/page_manager/page_manager.module',
      'name' => 'page_manager',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Page manager',
        'description' => 'Provides a UI and API to manage pages within the site.',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'package' => 'Chaos tool suite',
        'version' => '7.x-1.3',
        'project' => 'ctools',
        'datestamp' => '1365013512',
        'php' => '5.2.4',
      ),
      'project' => 'ctools',
      'version' => '7.x-1.3',
    ),
    'path' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/path/path.module',
      'name' => 'path',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Path',
        'description' => 'Allows users to rename URLs.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'path.test',
        ),
        'configure' => 'admin/config/search/path',
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'pathauto' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/pathauto/pathauto.module',
      'name' => 'pathauto',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7005',
      'weight' => '1',
      'info' => 
      array (
        'name' => 'Pathauto',
        'description' => 'Provides a mechanism for modules to automatically generate aliases for the content they manage.',
        'dependencies' => 
        array (
          0 => 'path',
          1 => 'token',
        ),
        'core' => '7.x',
        'files' => 
        array (
          0 => 'pathauto.test',
        ),
        'configure' => 'admin/config/search/path/patterns',
        'recommends' => 
        array (
          0 => 'redirect',
        ),
        'version' => '7.x-1.2',
        'project' => 'pathauto',
        'datestamp' => '1344525185',
        'php' => '5.2.4',
      ),
      'project' => 'pathauto',
      'version' => '7.x-1.2',
    ),
    'path_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/path_test.module',
      'name' => 'path_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Hook path tests',
        'description' => 'Support module for path hook testing.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'performance_event_field' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/custom/performance_event/performance_event_field.module',
      'name' => 'performance_event_field',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Performance Event Fields',
        'description' => 'Custom field(s) for MCN performance events. For/by MCN Interactive',
        'core' => '7.x',
        'package' => 'Base MCN Features',
        'dependencies' => 
        array (
          0 => 'date',
        ),
        'version' => NULL,
        'php' => '5.2.4',
      ),
      'project' => '',
      'version' => NULL,
    ),
    'photos' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/features/photos/photos.module',
      'name' => 'photos',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Photos',
        'description' => 'Adds Basic Photo Content Type and Views',
        'core' => '7.x',
        'package' => 'Base MCN Features',
        'version' => '7.x-3.0',
        'project' => 'photos',
        'dependencies' => 
        array (
          0 => 'colorbox',
          1 => 'features',
          2 => 'list',
          3 => 'mcn_base_feature',
          4 => 'media',
          5 => 'multiselect',
          6 => 'strongarm',
          7 => 'taxonomy',
          8 => 'views_feature',
        ),
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'strongarm:strongarm:1',
            1 => 'views:views_default:3.0',
          ),
          'features_api' => 
          array (
            0 => 'api:2',
          ),
          'field_base' => 
          array (
            0 => 'field_gallery',
            1 => 'field_photo',
            2 => 'field_photo_sort_order',
          ),
          'field_instance' => 
          array (
            0 => 'node-photo-field_gallery',
            1 => 'node-photo-field_photo',
            2 => 'node-photo-field_photo_sort_order',
          ),
          'node' => 
          array (
            0 => 'photo',
          ),
          'taxonomy' => 
          array (
            0 => 'gallery',
          ),
          'user_permission' => 
          array (
            0 => 'create photo content',
            1 => 'delete any photo content',
            2 => 'delete own photo content',
            3 => 'delete terms in banner_space',
            4 => 'edit any photo content',
            5 => 'edit own photo content',
            6 => 'edit terms in banner_space',
          ),
          'variable' => 
          array (
            0 => 'comment_anonymous_photo',
            1 => 'comment_default_mode_photo',
            2 => 'comment_default_per_page_photo',
            3 => 'comment_form_location_photo',
            4 => 'comment_photo',
            5 => 'comment_preview_photo',
            6 => 'comment_subject_field_photo',
            7 => 'menu_options_photo',
            8 => 'menu_parent_photo',
            9 => 'node_options_photo',
            10 => 'node_preview_photo',
            11 => 'node_submitted_photo',
            12 => 'pathauto_node_photo_pattern',
            13 => 'scheduler_publish_enable_photo',
            14 => 'scheduler_publish_touch_photo',
            15 => 'scheduler_unpublish_enable_photo',
          ),
          'views_view' => 
          array (
            0 => 'photos',
          ),
        ),
        'project status url' => 'http://features.devmcn.com/fserver',
        'php' => '5.2.4',
      ),
      'project' => 'photos',
      'version' => '7.x-3.0',
    ),
    'php' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/php/php.module',
      'name' => 'php',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'PHP filter',
        'description' => 'Allows embedded PHP code/snippets to be evaluated.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'php.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'physical_locations' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/features/physical_locations/physical_locations.module',
      'name' => 'physical_locations',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Physical Locations',
        'description' => 'Allow for a location listing and search on the site.',
        'core' => '7.x',
        'package' => 'MCN Business',
        'php' => '5.2.4',
        'version' => '7.x-0.5alpha',
        'project' => 'physical_locations',
        'dependencies' => 
        array (
          0 => 'basic_page',
          1 => 'ctools',
          2 => 'email',
          3 => 'features',
          4 => 'field_group',
          5 => 'image',
          6 => 'link',
          7 => 'location_cck',
          8 => 'media',
          9 => 'options',
          10 => 'strongarm',
          11 => 'taxonomy',
          12 => 'views',
        ),
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'field_group:field_group:1',
            1 => 'strongarm:strongarm:1',
            2 => 'views:views_default:3.0',
          ),
          'features_api' => 
          array (
            0 => 'api:2',
          ),
          'field_base' => 
          array (
            0 => 'field_pl_contact_person',
            1 => 'field_pl_email',
            2 => 'field_pl_hours',
            3 => 'field_pl_location',
            4 => 'field_pl_photo',
            5 => 'field_pl_type',
            6 => 'field_pl_website',
          ),
          'field_group' => 
          array (
            0 => 'group_pl_contact|node|physical_location|form',
            1 => 'group_pl_container|node|physical_location|form',
            2 => 'group_pl_description|node|physical_location|form',
            3 => 'group_pl_hours|node|physical_location|form',
            4 => 'group_pl_location|node|physical_location|form',
          ),
          'field_instance' => 
          array (
            0 => 'node-physical_location-body',
            1 => 'node-physical_location-field_pl_contact_person',
            2 => 'node-physical_location-field_pl_email',
            3 => 'node-physical_location-field_pl_hours',
            4 => 'node-physical_location-field_pl_location',
            5 => 'node-physical_location-field_pl_photo',
            6 => 'node-physical_location-field_pl_type',
            7 => 'node-physical_location-field_pl_website',
          ),
          'node' => 
          array (
            0 => 'physical_location',
          ),
          'taxonomy' => 
          array (
            0 => 'physical_location_type',
          ),
          'user_permission' => 
          array (
            0 => 'create physical_location content',
            1 => 'delete any physical_location content',
            2 => 'delete own physical_location content',
            3 => 'edit any physical_location content',
            4 => 'edit own physical_location content',
          ),
          'variable' => 
          array (
            0 => 'menu_options_physical_location',
            1 => 'menu_parent_physical_location',
            2 => 'node_options_physical_location',
            3 => 'node_preview_physical_location',
            4 => 'node_submitted_physical_location',
            5 => 'pathauto_node_physical_location_pattern',
          ),
          'views_view' => 
          array (
            0 => 'physical_locations',
          ),
        ),
      ),
      'project' => 'physical_locations',
      'version' => '7.x-0.5alpha',
    ),
    'plupload' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/plupload/plupload.module',
      'name' => 'plupload',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Plupload integration module',
        'description' => 'Provides a plupload element.',
        'files' => 
        array (
          0 => 'plupload.module',
        ),
        'core' => '7.x',
        'package' => 'Media',
        'version' => '7.x-1.2',
        'project' => 'plupload',
        'datestamp' => '1372434351',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'plupload',
      'version' => '7.x-1.2',
    ),
    'poll' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/poll/poll.module',
      'name' => 'poll',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Poll',
        'description' => 'Allows your site to capture votes on different topics in the form of multiple choice questions.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'poll.test',
        ),
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'poll.css',
          ),
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'print' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/print/print.module',
      'name' => 'print',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Printer-friendly pages',
        'description' => 'Adds a printer-friendly version link to content and administrative pages.',
        'core' => '7.x',
        'package' => 'Printer, email and PDF versions',
        'files' => 
        array (
          0 => 'print.module',
          1 => 'print.admin.inc',
          2 => 'print.pages.inc',
          3 => 'print.install',
          4 => 'print.views.inc',
          5 => 'print_join_page_counter.inc',
        ),
        'configure' => 'admin/config/user-interface/print',
        'version' => '7.x-1.2',
        'project' => 'print',
        'datestamp' => '1346768900',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'print',
      'version' => '7.x-1.2',
    ),
    'print_mail' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/print/print_mail/print_mail.module',
      'name' => 'print_mail',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Send by email',
        'description' => 'Provides the capability to send the web page by email',
        'core' => '7.x',
        'package' => 'Printer, email and PDF versions',
        'dependencies' => 
        array (
          0 => 'print',
        ),
        'files' => 
        array (
          0 => 'print_mail.module',
          1 => 'print_mail.inc',
          2 => 'print_mail.admin.inc',
          3 => 'print_mail.install',
          4 => 'print_mail.views.inc',
        ),
        'configure' => 'admin/config/user-interface/print/email',
        'version' => '7.x-1.2',
        'project' => 'print',
        'datestamp' => '1346768900',
        'php' => '5.2.4',
      ),
      'project' => 'print',
      'version' => '7.x-1.2',
    ),
    'print_pdf' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/print/print_pdf/print_pdf.module',
      'name' => 'print_pdf',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'PDF version',
        'description' => 'Adds the capability to export pages as PDF.',
        'core' => '7.x',
        'package' => 'Printer, email and PDF versions',
        'dependencies' => 
        array (
          0 => 'print',
        ),
        'files' => 
        array (
          0 => 'print_pdf.module',
          1 => 'print_pdf.admin.inc',
          2 => 'print_pdf.pages.inc',
          3 => 'print_pdf.install',
          4 => 'print_pdf.views.inc',
        ),
        'configure' => 'admin/config/user-interface/print/pdf',
        'version' => '7.x-1.2',
        'project' => 'print',
        'datestamp' => '1346768900',
        'php' => '5.2.4',
      ),
      'project' => 'print',
      'version' => '7.x-1.2',
    ),
    'profile' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/profile/profile.module',
      'name' => 'profile',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Profile',
        'description' => 'Supports configurable user profiles.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'profile.test',
        ),
        'configure' => 'admin/config/people/profile',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'psr_0_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/psr_0_test/psr_0_test.module',
      'name' => 'psr_0_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'PSR-0 Test cases',
        'description' => 'Test classes to be discovered by simpletest.',
        'core' => '7.x',
        'hidden' => true,
        'package' => 'Testing',
        'version' => '7.23',
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'quicktabs' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/quicktabs/quicktabs.module',
      'name' => 'quicktabs',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Quicktabs',
        'description' => 'Render content with tabs and other display styles',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'quicktabs.module',
          1 => 'quicktabs.classes.inc',
          2 => 'includes/quicktabs_style_plugin.inc',
          3 => 'tests/quicktabs.test',
        ),
        'configure' => 'admin/structure/quicktabs',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'version' => '7.x-3.4',
        'project' => 'quicktabs',
        'datestamp' => '1332980461',
        'php' => '5.2.4',
      ),
      'project' => 'quicktabs',
      'version' => '7.x-3.4',
    ),
    'quicktabs_tabstyles' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/quicktabs/quicktabs_tabstyles/quicktabs_tabstyles.module',
      'name' => 'quicktabs_tabstyles',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Quicktabs Styles',
        'description' => 'Adds predefined tab styles to choose from per Quicktabs instance.',
        'core' => '7.x',
        'configure' => 'admin/structure/quicktabs/styles',
        'dependencies' => 
        array (
          0 => 'quicktabs',
        ),
        'version' => '7.x-3.4',
        'project' => 'quicktabs',
        'datestamp' => '1332980461',
        'php' => '5.2.4',
      ),
      'project' => 'quicktabs',
      'version' => '7.x-3.4',
    ),
    'rdf' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/rdf/rdf.module',
      'name' => 'rdf',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'RDF',
        'description' => 'Enriches your content with metadata to let other applications (e.g. search engines, aggregators) better understand its relationships and attributes.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'rdf.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'rdf_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/rdf/tests/rdf_test.module',
      'name' => 'rdf_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'RDF module tests',
        'description' => 'Support module for RDF module testing.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'recaptcha' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/recaptcha/recaptcha.module',
      'name' => 'recaptcha',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'reCAPTCHA',
        'description' => 'Uses the reCAPTCHA web service to improve the CAPTCHA system.',
        'dependencies' => 
        array (
          0 => 'captcha',
        ),
        'package' => 'Spam control',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'recaptcha.admin.inc',
          1 => 'recaptcha.module',
          2 => 'recaptcha.install',
        ),
        'version' => '7.x-1.10-rc1',
        'project' => 'recaptcha',
        'datestamp' => '1376332616',
        'php' => '5.2.4',
      ),
      'project' => 'recaptcha',
      'version' => '7.x-1.10-rc1',
    ),
    'recaptcha_mailhide' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/recaptcha/recaptcha_mailhide.module',
      'name' => 'recaptcha_mailhide',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'reCAPTCHA Mailhide',
        'description' => 'Uses the reCAPTCHA web service to protect email addresses.',
        'package' => 'Spam control',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'recaptcha_mailhide.module',
        ),
        'version' => '7.x-1.10-rc1',
        'project' => 'recaptcha',
        'datestamp' => '1376332616',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'recaptcha',
      'version' => '7.x-1.10-rc1',
    ),
    'redirect' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/redirect/redirect.module',
      'name' => 'redirect',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Redirect',
        'description' => 'Allows users to redirect from old URLs to new URLs.',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'redirect.module',
          1 => 'redirect.admin.inc',
          2 => 'redirect.install',
          3 => 'redirect.test',
          4 => 'views/redirect.views.inc',
          5 => 'views/redirect_handler_filter_redirect_type.inc',
          6 => 'views/redirect_handler_field_redirect_source.inc',
          7 => 'views/redirect_handler_field_redirect_redirect.inc',
          8 => 'views/redirect_handler_field_redirect_operations.inc',
          9 => 'views/redirect_handler_field_redirect_link_edit.inc',
          10 => 'views/redirect_handler_field_redirect_link_delete.inc',
        ),
        'configure' => 'admin/config/search/redirect/settings',
        'version' => '7.x-1.0-rc1',
        'project' => 'redirect',
        'datestamp' => '1347989995',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'redirect',
      'version' => '7.x-1.0-rc1',
    ),
    'references' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/references/references.module',
      'name' => 'references',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'References',
        'description' => 'Defines common base features for the various reference field types.',
        'package' => 'Fields',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'field',
          1 => 'options',
        ),
        'files' => 
        array (
          0 => 'views/references_handler_relationship.inc',
          1 => 'views/references_handler_argument.inc',
          2 => 'views/references_plugin_display.inc',
          3 => 'views/references_plugin_style.inc',
          4 => 'views/references_plugin_row_fields.inc',
        ),
        'version' => '7.x-2.1',
        'project' => 'references',
        'datestamp' => '1360265821',
        'php' => '5.2.4',
      ),
      'project' => 'references',
      'version' => '7.x-2.1',
    ),
    'requirements1_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/requirements1_test.module',
      'name' => 'requirements1_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Requirements 1 Test',
        'description' => 'Tests that a module is not installed when it fails hook_requirements(\'install\').',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'requirements2_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/requirements2_test.module',
      'name' => 'requirements2_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Requirements 2 Test',
        'description' => 'Tests that a module is not installed when the one it depends on fails hook_requirements(\'install).',
        'dependencies' => 
        array (
          0 => 'requirements1_test',
          1 => 'comment',
        ),
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'restaurant_menu' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/features/restaurant_menu/restaurant_menu.module',
      'name' => 'restaurant_menu',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Restaurant Menu',
        'description' => 'Allow for a restaurant menu on the site, with an optional list (vs. grid) page and a featured menu item block.',
        'core' => '7.x',
        'package' => 'MCN Business',
        'php' => '5.2.4',
        'version' => '7.x-1.0alpha',
        'project' => 'restaurant_menu',
        'dependencies' => 
        array (
          0 => 'basic_page',
          1 => 'ctools',
          2 => 'features',
          3 => 'image',
          4 => 'media',
          5 => 'number',
          6 => 'options',
          7 => 'strongarm',
          8 => 'taxonomy',
          9 => 'views',
        ),
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'strongarm:strongarm:1',
            1 => 'views:views_default:3.0',
          ),
          'features_api' => 
          array (
            0 => 'api:2',
          ),
          'field_base' => 
          array (
            0 => 'field_rmi_category',
            1 => 'field_rmi_photos',
            2 => 'field_rmi_price',
            3 => 'field_rmi_tags',
          ),
          'field_instance' => 
          array (
            0 => 'node-restaurant_menu_item-body',
            1 => 'node-restaurant_menu_item-field_rmi_category',
            2 => 'node-restaurant_menu_item-field_rmi_photos',
            3 => 'node-restaurant_menu_item-field_rmi_price',
            4 => 'node-restaurant_menu_item-field_rmi_tags',
          ),
          'node' => 
          array (
            0 => 'restaurant_menu_item',
          ),
          'taxonomy' => 
          array (
            0 => 'restaurant_menu_item_category',
            1 => 'restaurant_menu_item_tags',
          ),
          'user_permission' => 
          array (
            0 => 'create restaurant_menu_item content',
            1 => 'delete any restaurant_menu_item content',
            2 => 'delete own restaurant_menu_item content',
            3 => 'edit any restaurant_menu_item content',
            4 => 'edit own restaurant_menu_item content',
          ),
          'variable' => 
          array (
            0 => 'menu_options_restaurant_menu_item',
            1 => 'menu_parent_restaurant_menu_item',
            2 => 'node_options_restaurant_menu_item',
            3 => 'node_preview_restaurant_menu_item',
            4 => 'node_submitted_restaurant_menu_item',
            5 => 'pathauto_node_restaurant_menu_item_pattern',
          ),
          'views_view' => 
          array (
            0 => 'restaurant_menu_items',
          ),
        ),
      ),
      'project' => 'restaurant_menu',
      'version' => '7.x-1.0alpha',
    ),
    'rules' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/rules/rules.module',
      'name' => 'rules',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Rules',
        'description' => 'React on events and conditionally evaluate actions.',
        'package' => 'Rules',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'rules.features.inc',
          1 => 'tests/rules.test',
          2 => 'includes/faces.inc',
          3 => 'includes/rules.core.inc',
          4 => 'includes/rules.processor.inc',
          5 => 'includes/rules.plugins.inc',
          6 => 'includes/rules.state.inc',
          7 => 'modules/php.eval.inc',
          8 => 'modules/rules_core.eval.inc',
          9 => 'modules/system.eval.inc',
          10 => 'ui/ui.controller.inc',
          11 => 'ui/ui.core.inc',
          12 => 'ui/ui.data.inc',
          13 => 'ui/ui.plugins.inc',
        ),
        'dependencies' => 
        array (
          0 => 'entity_token',
          1 => 'entity',
        ),
        'version' => '7.x-2.3',
        'project' => 'rules',
        'datestamp' => '1364401818',
        'php' => '5.2.4',
      ),
      'project' => 'rules',
      'version' => '7.x-2.3',
    ),
    'rules_admin' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/rules/rules_admin/rules_admin.module',
      'name' => 'rules_admin',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Rules UI',
        'description' => 'Administrative interface for managing rules.',
        'package' => 'Rules',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'rules_admin.module',
          1 => 'rules_admin.inc',
        ),
        'dependencies' => 
        array (
          0 => 'rules',
        ),
        'version' => '7.x-2.3',
        'project' => 'rules',
        'datestamp' => '1364401818',
        'php' => '5.2.4',
      ),
      'project' => 'rules',
      'version' => '7.x-2.3',
    ),
    'rules_i18n' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/rules/rules_i18n/rules_i18n.module',
      'name' => 'rules_i18n',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Rules translation',
        'description' => 'Allows translating rules.',
        'dependencies' => 
        array (
          0 => 'rules',
          1 => 'i18n_string',
        ),
        'package' => 'Multilingual - Internationalization',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'rules_i18n.i18n.inc',
          1 => 'rules_i18n.rules.inc',
          2 => 'rules_i18n.test',
        ),
        'version' => '7.x-2.3',
        'project' => 'rules',
        'datestamp' => '1364401818',
        'php' => '5.2.4',
      ),
      'project' => 'rules',
      'version' => '7.x-2.3',
    ),
    'rules_scheduler' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/rules/rules_scheduler/rules_scheduler.module',
      'name' => 'rules_scheduler',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Rules Scheduler',
        'description' => 'Schedule the execution of Rules components using actions.',
        'dependencies' => 
        array (
          0 => 'rules',
        ),
        'package' => 'Rules',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'rules_scheduler.admin.inc',
          1 => 'rules_scheduler.module',
          2 => 'rules_scheduler.install',
          3 => 'rules_scheduler.rules.inc',
          4 => 'rules_scheduler.test',
          5 => 'includes/rules_scheduler.views_default.inc',
          6 => 'includes/rules_scheduler.views.inc',
          7 => 'includes/rules_scheduler_views_filter.inc',
        ),
        'version' => '7.x-2.3',
        'project' => 'rules',
        'datestamp' => '1364401818',
        'php' => '5.2.4',
      ),
      'project' => 'rules',
      'version' => '7.x-2.3',
    ),
    'rules_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/rules/tests/rules_test.module',
      'name' => 'rules_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Rules Tests',
        'description' => 'Support module for the Rules tests.',
        'package' => 'Testing',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'rules_test.rules.inc',
          1 => 'rules_test.rules_defaults.inc',
        ),
        'hidden' => true,
        'version' => '7.x-2.3',
        'project' => 'rules',
        'datestamp' => '1364401818',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'rules',
      'version' => '7.x-2.3',
    ),
    'scheduler' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/scheduler/scheduler.module',
      'name' => 'scheduler',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '6101',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Scheduler',
        'description' => 'This module allows nodes to be published and unpublished on specified dates and time.',
        'core' => '7.x',
        'configure' => 'admin/config/content/scheduler',
        'files' => 
        array (
          0 => 'scheduler.install',
          1 => 'scheduler.module',
          2 => 'scheduler.views.inc',
          3 => 'scheduler.test',
          4 => 'scheduler_handler_field_scheduler_countdown.inc',
        ),
        'version' => '7.x-1.1',
        'project' => 'scheduler',
        'datestamp' => '1366294356',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'scheduler',
      'version' => '7.x-1.1',
    ),
    'search' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/search/search.module',
      'name' => 'search',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Search',
        'description' => 'Enables site-wide keyword searching.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'search.extender.inc',
          1 => 'search.test',
        ),
        'configure' => 'admin/config/search/settings',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'search.css',
          ),
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'search_embedded_form' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/search/tests/search_embedded_form.module',
      'name' => 'search_embedded_form',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Search embedded form',
        'description' => 'Support module for search module testing of embedded forms.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'search_extra_type' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/search/tests/search_extra_type.module',
      'name' => 'search_extra_type',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Test search type',
        'description' => 'Support module for search module testing.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'securepages' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/securepages/securepages.module',
      'name' => 'securepages',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Secure Pages',
        'description' => 'Set which pages are always going to be used in secure mode (SSL) Warning: Do not enable this module without configuring your web server to handle SSL with this installation of Drupal',
        'core' => '7.x',
        'configure' => 'admin/config/system/securepages',
        'files' => 
        array (
          0 => 'securepages.test',
        ),
        'version' => '7.x-1.0-beta1',
        'project' => 'securepages',
        'datestamp' => '1353374317',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'securepages',
      'version' => '7.x-1.0-beta1',
    ),
    'session_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/session_test.module',
      'name' => 'session_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Session test',
        'description' => 'Support module for session data testing.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'shortcut' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/shortcut/shortcut.module',
      'name' => 'shortcut',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Shortcut',
        'description' => 'Allows users to manage customizable lists of shortcut links.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'shortcut.test',
        ),
        'configure' => 'admin/config/user-interface/shortcut',
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'simpletest' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/simpletest.module',
      'name' => 'simpletest',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Testing',
        'description' => 'Provides a framework for unit and functional testing.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'simpletest.test',
          1 => 'drupal_web_test_case.php',
          2 => 'tests/actions.test',
          3 => 'tests/ajax.test',
          4 => 'tests/batch.test',
          5 => 'tests/bootstrap.test',
          6 => 'tests/cache.test',
          7 => 'tests/common.test',
          8 => 'tests/database_test.test',
          9 => 'tests/entity_crud_hook_test.test',
          10 => 'tests/entity_query.test',
          11 => 'tests/error.test',
          12 => 'tests/file.test',
          13 => 'tests/filetransfer.test',
          14 => 'tests/form.test',
          15 => 'tests/graph.test',
          16 => 'tests/image.test',
          17 => 'tests/lock.test',
          18 => 'tests/mail.test',
          19 => 'tests/menu.test',
          20 => 'tests/module.test',
          21 => 'tests/pager.test',
          22 => 'tests/password.test',
          23 => 'tests/path.test',
          24 => 'tests/registry.test',
          25 => 'tests/schema.test',
          26 => 'tests/session.test',
          27 => 'tests/tablesort.test',
          28 => 'tests/theme.test',
          29 => 'tests/unicode.test',
          30 => 'tests/update.test',
          31 => 'tests/xmlrpc.test',
          32 => 'tests/upgrade/upgrade.test',
          33 => 'tests/upgrade/upgrade.comment.test',
          34 => 'tests/upgrade/upgrade.filter.test',
          35 => 'tests/upgrade/upgrade.forum.test',
          36 => 'tests/upgrade/upgrade.locale.test',
          37 => 'tests/upgrade/upgrade.menu.test',
          38 => 'tests/upgrade/upgrade.node.test',
          39 => 'tests/upgrade/upgrade.taxonomy.test',
          40 => 'tests/upgrade/upgrade.trigger.test',
          41 => 'tests/upgrade/upgrade.translatable.test',
          42 => 'tests/upgrade/upgrade.upload.test',
          43 => 'tests/upgrade/upgrade.user.test',
          44 => 'tests/upgrade/update.aggregator.test',
          45 => 'tests/upgrade/update.trigger.test',
          46 => 'tests/upgrade/update.field.test',
          47 => 'tests/upgrade/update.user.test',
        ),
        'configure' => 'admin/config/development/testing/settings',
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'simple_blog' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/features/simple_blog/simple_blog.module',
      'name' => 'simple_blog',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Simple Blog',
        'description' => 'Installs a single user blog content type, page and block.',
        'core' => '7.x',
        'package' => 'Base MCN Features',
        'php' => '5.2.4',
        'version' => '7.x-3.0',
        'project' => 'simple_blog',
        'dependencies' => 
        array (
          0 => 'basic_page',
          1 => 'context',
          2 => 'eva',
          3 => 'mcn_base_feature',
          4 => 'mcn_wysiwyg',
          5 => 'strongarm',
          6 => 'taxonomy',
          7 => 'views',
          8 => 'views_feature',
        ),
        'features' => 
        array (
          'context' => 
          array (
            0 => 'blog_block',
          ),
          'ctools' => 
          array (
            0 => 'context:context:3',
            1 => 'strongarm:strongarm:1',
            2 => 'views:views_default:3.0',
          ),
          'features_api' => 
          array (
            0 => 'api:2',
          ),
          'field_base' => 
          array (
            0 => 'field_blog_tags',
          ),
          'field_instance' => 
          array (
            0 => 'node-blog-body',
            1 => 'node-blog-field_blog_tags',
          ),
          'node' => 
          array (
            0 => 'blog',
          ),
          'user_permission' => 
          array (
            0 => 'create blog content',
            1 => 'delete any blog content',
            2 => 'delete own blog content',
            3 => 'edit any blog content',
            4 => 'edit own blog content',
          ),
          'variable' => 
          array (
            0 => 'comment_anonymous_blog',
            1 => 'comment_blog',
            2 => 'comment_default_mode_blog',
            3 => 'comment_default_per_page_blog',
            4 => 'comment_form_location_blog',
            5 => 'comment_preview_blog',
            6 => 'comment_subject_field_blog',
            7 => 'menu_options_blog',
            8 => 'menu_parent_blog',
            9 => 'node_options_blog',
            10 => 'node_preview_blog',
            11 => 'node_submitted_blog',
            12 => 'pathauto_node_blog_pattern',
            13 => 'scheduler_publish_enable_blog',
            14 => 'scheduler_publish_touch_blog',
            15 => 'scheduler_unpublish_enable_blog',
          ),
          'views_view' => 
          array (
            0 => 'blog',
          ),
        ),
        'project status url' => 'http://features.devmcn.com/fserver',
      ),
      'project' => 'simple_blog',
      'version' => '7.x-3.0',
    ),
    'smartqueue' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/nodequeue/smartqueue.module',
      'name' => 'smartqueue',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Smartqueue taxonomy',
        'description' => 'Creates a node queue for each taxonomy vocabulary',
        'package' => 'Nodequeue',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'nodequeue',
          1 => 'taxonomy',
        ),
        'version' => '7.x-2.0-beta1',
        'project' => 'nodequeue',
        'datestamp' => '1316558104',
        'php' => '5.2.4',
      ),
      'project' => 'nodequeue',
      'version' => '7.x-2.0-beta1',
    ),
    'statistics' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/statistics/statistics.module',
      'name' => 'statistics',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Statistics',
        'description' => 'Logs access statistics for your site.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'statistics.test',
        ),
        'configure' => 'admin/config/system/statistics',
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'strongarm' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/strongarm/strongarm.module',
      'name' => 'strongarm',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7201',
      'weight' => '-1000',
      'info' => 
      array (
        'name' => 'Strongarm',
        'description' => 'Enforces variable values defined by modules that need settings set to operate properly.',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'files' => 
        array (
          0 => 'strongarm.admin.inc',
          1 => 'strongarm.install',
          2 => 'strongarm.module',
        ),
        'version' => '7.x-2.0',
        'project' => 'strongarm',
        'datestamp' => '1339604214',
        'php' => '5.2.4',
      ),
      'project' => 'strongarm',
      'version' => '7.x-2.0',
    ),
    'styles' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/styles/styles.module',
      'name' => 'styles',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Styles',
        'description' => 'Bundles similar display formatters together.',
        'package' => 'Styles',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'styles.module',
          1 => 'includes/Styles.inc',
        ),
        'version' => '7.x-2.0-alpha8',
        'project' => 'styles',
        'datestamp' => '1306964517',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'styles',
      'version' => '7.x-2.0-alpha8',
    ),
    'styles_ui' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/styles/contrib/styles_ui/styles_ui.module',
      'name' => 'styles_ui',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Styles UI',
        'description' => 'Allows administration of the Styles modules.',
        'package' => 'Styles',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'styles_ui.admin.inc',
        ),
        'dependencies' => 
        array (
          0 => 'styles',
        ),
        'version' => '7.x-2.0-alpha8',
        'project' => 'styles',
        'datestamp' => '1306964517',
        'php' => '5.2.4',
      ),
      'project' => 'styles',
      'version' => '7.x-2.0-alpha8',
    ),
    'stylizer' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib-patched/ctools/stylizer/stylizer.module',
      'name' => 'stylizer',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Stylizer',
        'description' => 'Create custom styles for applications such as Panels.',
        'core' => '7.x',
        'package' => 'Chaos tool suite',
        'dependencies' => 
        array (
          0 => 'ctools',
          1 => 'color',
        ),
        'version' => '7.x-1.3',
        'project' => 'ctools',
        'datestamp' => '1365013512',
        'php' => '5.2.4',
      ),
      'project' => 'ctools',
      'version' => '7.x-1.3',
    ),
    'superfish' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/superfish/superfish.module',
      'name' => 'superfish',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Superfish',
        'description' => 'jQuery Superfish plugin for your Drupal menus.',
        'package' => 'User interface',
        'dependencies' => 
        array (
          0 => 'menu',
        ),
        'configure' => 'admin/config/user-interface/superfish',
        'core' => '7.x',
        'version' => '7.x-1.9',
        'project' => 'superfish',
        'datestamp' => '1367054112',
        'php' => '5.2.4',
      ),
      'project' => 'superfish',
      'version' => '7.x-1.9',
    ),
    'syslog' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/syslog/syslog.module',
      'name' => 'syslog',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Syslog',
        'description' => 'Logs and records system events to syslog.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'syslog.test',
        ),
        'configure' => 'admin/config/development/logging',
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'system' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/system/system.module',
      'name' => 'system',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7078',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'System',
        'description' => 'Handles general site configuration for administrators.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'system.archiver.inc',
          1 => 'system.mail.inc',
          2 => 'system.queue.inc',
          3 => 'system.tar.inc',
          4 => 'system.updater.inc',
          5 => 'system.test',
        ),
        'required' => true,
        'configure' => 'admin/config/system',
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'system_dependencies_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/system_dependencies_test.module',
      'name' => 'system_dependencies_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'System dependency test',
        'description' => 'Support module for testing system dependencies.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'dependencies' => 
        array (
          0 => '_missing_dependency',
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'system_incompatible_core_version_dependencies_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/system_incompatible_core_version_dependencies_test.module',
      'name' => 'system_incompatible_core_version_dependencies_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'System incompatible core version dependencies test',
        'description' => 'Support module for testing system dependencies.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'dependencies' => 
        array (
          0 => 'system_incompatible_core_version_test',
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'system_incompatible_core_version_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/system_incompatible_core_version_test.module',
      'name' => 'system_incompatible_core_version_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'System incompatible core version test',
        'description' => 'Support module for testing system dependencies.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '5.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'system_incompatible_module_version_dependencies_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/system_incompatible_module_version_dependencies_test.module',
      'name' => 'system_incompatible_module_version_dependencies_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'System incompatible module version dependencies test',
        'description' => 'Support module for testing system dependencies.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'dependencies' => 
        array (
          0 => 'system_incompatible_module_version_test (>2.0)',
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'system_incompatible_module_version_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/system_incompatible_module_version_test.module',
      'name' => 'system_incompatible_module_version_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'System incompatible module version test',
        'description' => 'Support module for testing system dependencies.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'system_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/system_test.module',
      'name' => 'system_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'System test',
        'description' => 'Support module for system testing.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'system_test.module',
        ),
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'taxonomy' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/taxonomy/taxonomy.module',
      'name' => 'taxonomy',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7010',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Taxonomy',
        'description' => 'Enables the categorization of content.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'options',
        ),
        'files' => 
        array (
          0 => 'taxonomy.module',
          1 => 'taxonomy.test',
        ),
        'configure' => 'admin/structure/taxonomy',
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'taxonomy_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/taxonomy_test.module',
      'name' => 'taxonomy_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Taxonomy test module',
        'description' => '"Tests functions and hooks not used in core".',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'dependencies' => 
        array (
          0 => 'taxonomy',
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'testimonials' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/features/testimonials/testimonials.module',
      'name' => 'testimonials',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Testimonials',
        'description' => 'Allow for a testimonial listing on the site.',
        'core' => '7.x',
        'package' => 'MCN Business',
        'php' => '5.2.4',
        'version' => '7.x-1.0alpha',
        'project' => 'testimonials',
        'dependencies' => 
        array (
          0 => 'auto_nodetitle',
          1 => 'basic_page',
          2 => 'ctools',
          3 => 'features',
          4 => 'image',
          5 => 'media',
          6 => 'strongarm',
          7 => 'views',
        ),
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'strongarm:strongarm:1',
            1 => 'views:views_default:3.0',
          ),
          'features_api' => 
          array (
            0 => 'api:2',
          ),
          'field_base' => 
          array (
            0 => 'field_t_affiliation',
            1 => 'field_t_name',
            2 => 'field_t_photo',
          ),
          'field_instance' => 
          array (
            0 => 'node-testimonial-body',
            1 => 'node-testimonial-field_t_affiliation',
            2 => 'node-testimonial-field_t_name',
            3 => 'node-testimonial-field_t_photo',
          ),
          'node' => 
          array (
            0 => 'testimonial',
          ),
          'user_permission' => 
          array (
            0 => 'create testimonial content',
            1 => 'delete any testimonial content',
            2 => 'delete own testimonial content',
            3 => 'edit any testimonial content',
            4 => 'edit own testimonial content',
          ),
          'variable' => 
          array (
            0 => 'ant_pattern_testimonial',
            1 => 'ant_php_testimonial',
            2 => 'ant_testimonial',
            3 => 'menu_options_testimonial',
            4 => 'menu_parent_testimonial',
            5 => 'node_options_testimonial',
            6 => 'node_preview_testimonial',
            7 => 'node_submitted_testimonial',
            8 => 'pathauto_node_testimonial_pattern',
          ),
          'views_view' => 
          array (
            0 => 'testimonials',
          ),
        ),
      ),
      'project' => 'testimonials',
      'version' => '7.x-1.0alpha',
    ),
    'text' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/field/modules/text/text.module',
      'name' => 'text',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7000',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Text',
        'description' => 'Defines simple text field types.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'field',
        ),
        'files' => 
        array (
          0 => 'text.test',
        ),
        'required' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'theme_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/theme_test.module',
      'name' => 'theme_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Theme test',
        'description' => 'Support module for theme system testing.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'token' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib-patched/token/token.module',
      'name' => 'token',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7001',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Token',
        'description' => 'Provides a user interface for the Token API and some missing core tokens.',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'token.test',
        ),
        'version' => '7.x-1.5',
        'project' => 'token',
        'datestamp' => '1361665026',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'token',
      'version' => '7.x-1.5',
    ),
    'token_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib-patched/token/tests/token_test.module',
      'name' => 'token_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Token Test',
        'description' => 'Testing module for token functionality.',
        'package' => 'Testing',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'token_test.module',
        ),
        'hidden' => true,
        'version' => '7.x-1.5',
        'project' => 'token',
        'datestamp' => '1361665026',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'token',
      'version' => '7.x-1.5',
    ),
    'toolbar' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/toolbar/toolbar.module',
      'name' => 'toolbar',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Toolbar',
        'description' => 'Provides a toolbar that shows the top-level administration menu items and links from other modules.',
        'core' => '7.x',
        'package' => 'Core',
        'version' => '7.23',
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'tracker' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/tracker/tracker.module',
      'name' => 'tracker',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Tracker',
        'description' => 'Enables tracking of recent content for users.',
        'dependencies' => 
        array (
          0 => 'comment',
        ),
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'tracker.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'translation' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/translation/translation.module',
      'name' => 'translation',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Content translation',
        'description' => 'Allows content to be translated into different languages.',
        'dependencies' => 
        array (
          0 => 'locale',
        ),
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'translation.test',
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'translation_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/translation/tests/translation_test.module',
      'name' => 'translation_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Content Translation Test',
        'description' => 'Support module for the content translation tests.',
        'core' => '7.x',
        'package' => 'Testing',
        'version' => '7.23',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'transliteration' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/transliteration/transliteration.module',
      'name' => 'transliteration',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '0',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Transliteration',
        'description' => 'Converts non-latin text to US-ASCII and sanitizes file names.',
        'core' => '7.x',
        'configure' => 'admin/config/media/file-system',
        'version' => '7.x-3.1',
        'project' => 'transliteration',
        'datestamp' => '1338540713',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'transliteration',
      'version' => '7.x-3.1',
    ),
    'trigger' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/trigger/trigger.module',
      'name' => 'trigger',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Trigger',
        'description' => 'Enables actions to be fired on certain system events, such as when new content is created.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'trigger.test',
        ),
        'configure' => 'admin/structure/trigger',
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'trigger_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/trigger/tests/trigger_test.module',
      'name' => 'trigger_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Trigger Test',
        'description' => 'Support module for Trigger tests.',
        'package' => 'Testing',
        'core' => '7.x',
        'hidden' => true,
        'version' => '7.23',
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'tweetbutton' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/tweetbutton/tweetbutton.module',
      'name' => 'tweetbutton',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Tweetbutton',
        'description' => 'Add tweet button to  your website without having to leave the page',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'token',
        ),
        'configure' => 'admin/config/services/tweetbutton',
        'version' => '7.x-2.0-beta1',
        'project' => 'tweetbutton',
        'datestamp' => '1324327545',
        'php' => '5.2.4',
      ),
      'project' => 'tweetbutton',
      'version' => '7.x-2.0-beta1',
    ),
    'twitter' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/twitter/twitter.module',
      'name' => 'twitter',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Twitter',
        'description' => 'Adds integration with the Twitter microblogging service.',
        'php' => '5.1',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'twitter_views_field_handlers.inc',
          1 => 'twitter.lib.php',
          2 => 'tests/core.test',
          3 => 'tests/input_filters.test',
        ),
        'dependencies' => 
        array (
          0 => 'oauth_common',
          1 => 'views',
        ),
        'configure' => 'admin/config/services/twitter',
        'version' => '7.x-5.8',
        'project' => 'twitter',
        'datestamp' => '1370303463',
      ),
      'project' => 'twitter',
      'version' => '7.x-5.8',
    ),
    'twitter_actions' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/twitter/twitter_actions/twitter_actions.module',
      'name' => 'twitter_actions',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Twitter actions',
        'description' => 'Exposes Drupal actions to send Twitter messages.',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'twitter',
        ),
        'version' => '7.x-5.8',
        'project' => 'twitter',
        'datestamp' => '1370303463',
        'php' => '5.2.4',
      ),
      'project' => 'twitter',
      'version' => '7.x-5.8',
    ),
    'twitter_block' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/twitter_block/twitter_block.module',
      'name' => 'twitter_block',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Twitter Block',
        'description' => 'Provides configurable blocks for a Twitter feed.',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'twitter_block.class.php',
          1 => 'tests/twitter_block.test',
        ),
        'dependencies' => 
        array (
          0 => 'block',
        ),
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'twitter_block.css',
          ),
        ),
        'version' => '7.x-1.0+24-dev',
        'project' => 'twitter_block',
        'datestamp' => '1357785146',
        'php' => '5.2.4',
      ),
      'project' => 'twitter_block',
      'version' => '7.x-1.0+24-dev',
    ),
    'twitter_mock' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/twitter/tests/twitter_mock.module',
      'name' => 'twitter_mock',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Twitter Mock',
        'description' => 'Sets up a mock web server to simulate API calls on tests',
        'core' => '7.x',
        'hidden' => true,
        'dependencies' => 
        array (
          0 => 'twitter',
          1 => 'simpletest',
        ),
        'version' => '7.x-5.8',
        'project' => 'twitter',
        'datestamp' => '1370303463',
        'php' => '5.2.4',
      ),
      'project' => 'twitter',
      'version' => '7.x-5.8',
    ),
    'twitter_post' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/twitter/twitter_post/twitter_post.module',
      'name' => 'twitter_post',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Twitter Post',
        'description' => 'Enables posting to twitter',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'twitter',
        ),
        'configure' => 'admin/config/services/twitter/post',
        'version' => '7.x-5.8',
        'project' => 'twitter',
        'datestamp' => '1370303463',
        'php' => '5.2.4',
      ),
      'project' => 'twitter',
      'version' => '7.x-5.8',
    ),
    'twitter_signin' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/twitter/twitter_signin/twitter_signin.module',
      'name' => 'twitter_signin',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Twitter Signin',
        'description' => 'Adds support for "Sign in with Twitter"',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'twitter',
        ),
        'configure' => 'admin/config/services/twitter/signin',
        'version' => '7.x-5.8',
        'project' => 'twitter',
        'datestamp' => '1370303463',
        'php' => '5.2.4',
      ),
      'project' => 'twitter',
      'version' => '7.x-5.8',
    ),
    'typekit_api' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/fontyourface/modules/typekit_api/typekit_api.module',
      'name' => 'typekit_api',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Typekit API',
        'description' => '@font-your-face provider with Typekit.com fonts.',
        'dependencies' => 
        array (
          0 => 'fontyourface',
        ),
        'package' => '@font-your-face',
        'core' => '7.x',
        'php' => '5.2.0',
        'version' => '7.x-2.8',
        'project' => 'fontyourface',
        'datestamp' => '1367195415',
      ),
      'project' => 'fontyourface',
      'version' => '7.x-2.8',
    ),
    'update' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/update/update.module',
      'name' => 'update',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7001',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Update manager',
        'description' => 'Checks for available updates, and can securely install or update modules and themes via a web interface.',
        'version' => '7.23',
        'package' => 'Core',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'update.test',
        ),
        'configure' => 'admin/reports/updates/settings',
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'update_script_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/update_script_test.module',
      'name' => 'update_script_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Update script test',
        'description' => 'Support module for update script testing.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'update_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/update/tests/update_test.module',
      'name' => 'update_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Update test',
        'description' => 'Support module for update module testing.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'update_test_1' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/update_test_1.module',
      'name' => 'update_test_1',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Update test',
        'description' => 'Support module for update testing.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'update_test_2' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/update_test_2.module',
      'name' => 'update_test_2',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Update test',
        'description' => 'Support module for update testing.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'update_test_3' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/update_test_3.module',
      'name' => 'update_test_3',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Update test',
        'description' => 'Support module for update testing.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'url_alter_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/url_alter_test.module',
      'name' => 'url_alter_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Url_alter tests',
        'description' => 'A support modules for url_alter hook testing.',
        'core' => '7.x',
        'package' => 'Testing',
        'version' => '7.23',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'user' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/user/user.module',
      'name' => 'user',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7018',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'User',
        'description' => 'Manages the user registration and login system.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'user.module',
          1 => 'user.test',
        ),
        'required' => true,
        'configure' => 'admin/config/people',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'user.css',
          ),
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'user_form_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/user/tests/user_form_test.module',
      'name' => 'user_form_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'User module form tests',
        'description' => 'Support module for user form testing.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'user_reference' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/references/user_reference/user_reference.module',
      'name' => 'user_reference',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'User Reference',
        'description' => 'Defines a field type for referencing a user from a node.',
        'package' => 'Fields',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'field',
          1 => 'references',
          2 => 'options',
        ),
        'version' => '7.x-2.1',
        'project' => 'references',
        'datestamp' => '1360265821',
        'php' => '5.2.4',
      ),
      'project' => 'references',
      'version' => '7.x-2.1',
    ),
    'varnish' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/varnish/varnish.module',
      'name' => 'varnish',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Varnish',
        'description' => 'Provides integration with the Varnish HTTP accelerator.',
        'core' => '7.x',
        'configure' => 'admin/config/development/varnish',
        'package' => 'Caching',
        'files' => 
        array (
          0 => 'varnish.admin.inc',
          1 => 'varnish.cache.inc',
          2 => 'varnish.test',
        ),
        'version' => '7.x-1.0-beta2',
        'project' => 'varnish',
        'datestamp' => '1361391598',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'varnish',
      'version' => '7.x-1.0-beta2',
    ),
    'videos' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/features/videos/videos.module',
      'name' => 'videos',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Videos',
        'description' => 'Adds Basic Video Content Type and Views',
        'core' => '7.x',
        'package' => 'Base MCN Features',
        'php' => '5.2.4',
        'version' => '7.x-3.0',
        'project' => 'videos',
        'dependencies' => 
        array (
          0 => 'eva',
          1 => 'features',
          2 => 'list',
          3 => 'mcn_base_feature',
          4 => 'media_internet',
          5 => 'media_vimeo',
          6 => 'media_youtube',
          7 => 'mediafield',
          8 => 'strongarm',
          9 => 'views_feature',
        ),
        'features' => 
        array (
          'ctools' => 
          array (
            0 => 'strongarm:strongarm:1',
            1 => 'views:views_default:3.0',
          ),
          'features_api' => 
          array (
            0 => 'api:2',
          ),
          'field_base' => 
          array (
            0 => 'field_video_embed',
            1 => 'field_video_sort',
          ),
          'field_instance' => 
          array (
            0 => 'node-video-field_video_embed',
            1 => 'node-video-field_video_sort',
          ),
          'node' => 
          array (
            0 => 'video',
          ),
          'user_permission' => 
          array (
            0 => 'add media from remote sources',
            1 => 'create video content',
            2 => 'delete any video content',
            3 => 'delete own video content',
            4 => 'edit any video content',
            5 => 'edit own video content',
          ),
          'variable' => 
          array (
            0 => 'comment_anonymous_video',
            1 => 'comment_default_mode_video',
            2 => 'comment_default_per_page_video',
            3 => 'comment_form_location_video',
            4 => 'comment_preview_video',
            5 => 'comment_subject_field_video',
            6 => 'comment_video',
            7 => 'menu_options_video',
            8 => 'menu_parent_video',
            9 => 'node_options_video',
            10 => 'node_preview_video',
            11 => 'node_submitted_video',
            12 => 'pathauto_node_video_pattern',
            13 => 'scheduler_publish_enable_video',
            14 => 'scheduler_publish_touch_video',
            15 => 'scheduler_unpublish_enable_video',
          ),
          'views_view' => 
          array (
            0 => 'videos',
          ),
        ),
        'project status url' => 'http://features.devmcn.com/fserver',
      ),
      'project' => 'videos',
      'version' => '7.x-3.0',
    ),
    'views' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/views/views.module',
      'name' => 'views',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7301',
      'weight' => '10',
      'info' => 
      array (
        'name' => 'Views',
        'description' => 'Create customized lists and queries from your database.',
        'package' => 'Views',
        'core' => '7.x',
        'php' => '5.2',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'css/views.css',
          ),
        ),
        'dependencies' => 
        array (
          0 => 'ctools',
        ),
        'files' => 
        array (
          0 => 'handlers/views_handler_area.inc',
          1 => 'handlers/views_handler_area_result.inc',
          2 => 'handlers/views_handler_area_text.inc',
          3 => 'handlers/views_handler_area_text_custom.inc',
          4 => 'handlers/views_handler_area_view.inc',
          5 => 'handlers/views_handler_argument.inc',
          6 => 'handlers/views_handler_argument_date.inc',
          7 => 'handlers/views_handler_argument_formula.inc',
          8 => 'handlers/views_handler_argument_many_to_one.inc',
          9 => 'handlers/views_handler_argument_null.inc',
          10 => 'handlers/views_handler_argument_numeric.inc',
          11 => 'handlers/views_handler_argument_string.inc',
          12 => 'handlers/views_handler_argument_group_by_numeric.inc',
          13 => 'handlers/views_handler_field.inc',
          14 => 'handlers/views_handler_field_counter.inc',
          15 => 'handlers/views_handler_field_boolean.inc',
          16 => 'handlers/views_handler_field_contextual_links.inc',
          17 => 'handlers/views_handler_field_custom.inc',
          18 => 'handlers/views_handler_field_date.inc',
          19 => 'handlers/views_handler_field_entity.inc',
          20 => 'handlers/views_handler_field_markup.inc',
          21 => 'handlers/views_handler_field_math.inc',
          22 => 'handlers/views_handler_field_numeric.inc',
          23 => 'handlers/views_handler_field_prerender_list.inc',
          24 => 'handlers/views_handler_field_time_interval.inc',
          25 => 'handlers/views_handler_field_serialized.inc',
          26 => 'handlers/views_handler_field_machine_name.inc',
          27 => 'handlers/views_handler_field_url.inc',
          28 => 'handlers/views_handler_filter.inc',
          29 => 'handlers/views_handler_filter_boolean_operator.inc',
          30 => 'handlers/views_handler_filter_boolean_operator_string.inc',
          31 => 'handlers/views_handler_filter_combine.inc',
          32 => 'handlers/views_handler_filter_date.inc',
          33 => 'handlers/views_handler_filter_equality.inc',
          34 => 'handlers/views_handler_filter_entity_bundle.inc',
          35 => 'handlers/views_handler_filter_group_by_numeric.inc',
          36 => 'handlers/views_handler_filter_in_operator.inc',
          37 => 'handlers/views_handler_filter_many_to_one.inc',
          38 => 'handlers/views_handler_filter_numeric.inc',
          39 => 'handlers/views_handler_filter_string.inc',
          40 => 'handlers/views_handler_filter_fields_compare.inc',
          41 => 'handlers/views_handler_relationship.inc',
          42 => 'handlers/views_handler_relationship_groupwise_max.inc',
          43 => 'handlers/views_handler_sort.inc',
          44 => 'handlers/views_handler_sort_date.inc',
          45 => 'handlers/views_handler_sort_formula.inc',
          46 => 'handlers/views_handler_sort_group_by_numeric.inc',
          47 => 'handlers/views_handler_sort_menu_hierarchy.inc',
          48 => 'handlers/views_handler_sort_random.inc',
          49 => 'includes/base.inc',
          50 => 'includes/handlers.inc',
          51 => 'includes/plugins.inc',
          52 => 'includes/view.inc',
          53 => 'modules/aggregator/views_handler_argument_aggregator_fid.inc',
          54 => 'modules/aggregator/views_handler_argument_aggregator_iid.inc',
          55 => 'modules/aggregator/views_handler_argument_aggregator_category_cid.inc',
          56 => 'modules/aggregator/views_handler_field_aggregator_title_link.inc',
          57 => 'modules/aggregator/views_handler_field_aggregator_category.inc',
          58 => 'modules/aggregator/views_handler_field_aggregator_item_description.inc',
          59 => 'modules/aggregator/views_handler_field_aggregator_xss.inc',
          60 => 'modules/aggregator/views_handler_filter_aggregator_category_cid.inc',
          61 => 'modules/aggregator/views_plugin_row_aggregator_rss.inc',
          62 => 'modules/book/views_plugin_argument_default_book_root.inc',
          63 => 'modules/comment/views_handler_argument_comment_user_uid.inc',
          64 => 'modules/comment/views_handler_field_comment.inc',
          65 => 'modules/comment/views_handler_field_comment_depth.inc',
          66 => 'modules/comment/views_handler_field_comment_link.inc',
          67 => 'modules/comment/views_handler_field_comment_link_approve.inc',
          68 => 'modules/comment/views_handler_field_comment_link_delete.inc',
          69 => 'modules/comment/views_handler_field_comment_link_edit.inc',
          70 => 'modules/comment/views_handler_field_comment_link_reply.inc',
          71 => 'modules/comment/views_handler_field_comment_node_link.inc',
          72 => 'modules/comment/views_handler_field_comment_username.inc',
          73 => 'modules/comment/views_handler_field_ncs_last_comment_name.inc',
          74 => 'modules/comment/views_handler_field_ncs_last_updated.inc',
          75 => 'modules/comment/views_handler_field_node_comment.inc',
          76 => 'modules/comment/views_handler_field_node_new_comments.inc',
          77 => 'modules/comment/views_handler_field_last_comment_timestamp.inc',
          78 => 'modules/comment/views_handler_filter_comment_user_uid.inc',
          79 => 'modules/comment/views_handler_filter_ncs_last_updated.inc',
          80 => 'modules/comment/views_handler_filter_node_comment.inc',
          81 => 'modules/comment/views_handler_sort_comment_thread.inc',
          82 => 'modules/comment/views_handler_sort_ncs_last_comment_name.inc',
          83 => 'modules/comment/views_handler_sort_ncs_last_updated.inc',
          84 => 'modules/comment/views_plugin_row_comment_rss.inc',
          85 => 'modules/comment/views_plugin_row_comment_view.inc',
          86 => 'modules/contact/views_handler_field_contact_link.inc',
          87 => 'modules/field/views_handler_field_field.inc',
          88 => 'modules/field/views_handler_relationship_entity_reverse.inc',
          89 => 'modules/field/views_handler_argument_field_list.inc',
          90 => 'modules/field/views_handler_argument_field_list_string.inc',
          91 => 'modules/field/views_handler_filter_field_list.inc',
          92 => 'modules/filter/views_handler_field_filter_format_name.inc',
          93 => 'modules/locale/views_handler_field_node_language.inc',
          94 => 'modules/locale/views_handler_filter_node_language.inc',
          95 => 'modules/locale/views_handler_argument_locale_group.inc',
          96 => 'modules/locale/views_handler_argument_locale_language.inc',
          97 => 'modules/locale/views_handler_field_locale_group.inc',
          98 => 'modules/locale/views_handler_field_locale_language.inc',
          99 => 'modules/locale/views_handler_field_locale_link_edit.inc',
          100 => 'modules/locale/views_handler_filter_locale_group.inc',
          101 => 'modules/locale/views_handler_filter_locale_language.inc',
          102 => 'modules/locale/views_handler_filter_locale_version.inc',
          103 => 'modules/node/views_handler_argument_dates_various.inc',
          104 => 'modules/node/views_handler_argument_node_language.inc',
          105 => 'modules/node/views_handler_argument_node_nid.inc',
          106 => 'modules/node/views_handler_argument_node_type.inc',
          107 => 'modules/node/views_handler_argument_node_vid.inc',
          108 => 'modules/node/views_handler_argument_node_uid_revision.inc',
          109 => 'modules/node/views_handler_field_history_user_timestamp.inc',
          110 => 'modules/node/views_handler_field_node.inc',
          111 => 'modules/node/views_handler_field_node_link.inc',
          112 => 'modules/node/views_handler_field_node_link_delete.inc',
          113 => 'modules/node/views_handler_field_node_link_edit.inc',
          114 => 'modules/node/views_handler_field_node_revision.inc',
          115 => 'modules/node/views_handler_field_node_revision_link.inc',
          116 => 'modules/node/views_handler_field_node_revision_link_delete.inc',
          117 => 'modules/node/views_handler_field_node_revision_link_revert.inc',
          118 => 'modules/node/views_handler_field_node_path.inc',
          119 => 'modules/node/views_handler_field_node_type.inc',
          120 => 'modules/node/views_handler_filter_history_user_timestamp.inc',
          121 => 'modules/node/views_handler_filter_node_access.inc',
          122 => 'modules/node/views_handler_filter_node_status.inc',
          123 => 'modules/node/views_handler_filter_node_type.inc',
          124 => 'modules/node/views_handler_filter_node_uid_revision.inc',
          125 => 'modules/node/views_plugin_argument_default_node.inc',
          126 => 'modules/node/views_plugin_argument_validate_node.inc',
          127 => 'modules/node/views_plugin_row_node_rss.inc',
          128 => 'modules/node/views_plugin_row_node_view.inc',
          129 => 'modules/profile/views_handler_field_profile_date.inc',
          130 => 'modules/profile/views_handler_field_profile_list.inc',
          131 => 'modules/profile/views_handler_filter_profile_selection.inc',
          132 => 'modules/search/views_handler_argument_search.inc',
          133 => 'modules/search/views_handler_field_search_score.inc',
          134 => 'modules/search/views_handler_filter_search.inc',
          135 => 'modules/search/views_handler_sort_search_score.inc',
          136 => 'modules/search/views_plugin_row_search_view.inc',
          137 => 'modules/statistics/views_handler_field_accesslog_path.inc',
          138 => 'modules/system/views_handler_argument_file_fid.inc',
          139 => 'modules/system/views_handler_field_file.inc',
          140 => 'modules/system/views_handler_field_file_extension.inc',
          141 => 'modules/system/views_handler_field_file_filemime.inc',
          142 => 'modules/system/views_handler_field_file_uri.inc',
          143 => 'modules/system/views_handler_field_file_status.inc',
          144 => 'modules/system/views_handler_filter_file_status.inc',
          145 => 'modules/taxonomy/views_handler_argument_taxonomy.inc',
          146 => 'modules/taxonomy/views_handler_argument_term_node_tid.inc',
          147 => 'modules/taxonomy/views_handler_argument_term_node_tid_depth.inc',
          148 => 'modules/taxonomy/views_handler_argument_term_node_tid_depth_modifier.inc',
          149 => 'modules/taxonomy/views_handler_argument_vocabulary_vid.inc',
          150 => 'modules/taxonomy/views_handler_argument_vocabulary_machine_name.inc',
          151 => 'modules/taxonomy/views_handler_field_taxonomy.inc',
          152 => 'modules/taxonomy/views_handler_field_term_node_tid.inc',
          153 => 'modules/taxonomy/views_handler_field_term_link_edit.inc',
          154 => 'modules/taxonomy/views_handler_filter_term_node_tid.inc',
          155 => 'modules/taxonomy/views_handler_filter_term_node_tid_depth.inc',
          156 => 'modules/taxonomy/views_handler_filter_vocabulary_vid.inc',
          157 => 'modules/taxonomy/views_handler_filter_vocabulary_machine_name.inc',
          158 => 'modules/taxonomy/views_handler_relationship_node_term_data.inc',
          159 => 'modules/taxonomy/views_plugin_argument_validate_taxonomy_term.inc',
          160 => 'modules/taxonomy/views_plugin_argument_default_taxonomy_tid.inc',
          161 => 'modules/tracker/views_handler_argument_tracker_comment_user_uid.inc',
          162 => 'modules/tracker/views_handler_filter_tracker_comment_user_uid.inc',
          163 => 'modules/tracker/views_handler_filter_tracker_boolean_operator.inc',
          164 => 'modules/system/views_handler_filter_system_type.inc',
          165 => 'modules/translation/views_handler_argument_node_tnid.inc',
          166 => 'modules/translation/views_handler_field_node_link_translate.inc',
          167 => 'modules/translation/views_handler_field_node_translation_link.inc',
          168 => 'modules/translation/views_handler_filter_node_tnid.inc',
          169 => 'modules/translation/views_handler_filter_node_tnid_child.inc',
          170 => 'modules/translation/views_handler_relationship_translation.inc',
          171 => 'modules/user/views_handler_argument_user_uid.inc',
          172 => 'modules/user/views_handler_argument_users_roles_rid.inc',
          173 => 'modules/user/views_handler_field_user.inc',
          174 => 'modules/user/views_handler_field_user_language.inc',
          175 => 'modules/user/views_handler_field_user_link.inc',
          176 => 'modules/user/views_handler_field_user_link_cancel.inc',
          177 => 'modules/user/views_handler_field_user_link_edit.inc',
          178 => 'modules/user/views_handler_field_user_mail.inc',
          179 => 'modules/user/views_handler_field_user_name.inc',
          180 => 'modules/user/views_handler_field_user_permissions.inc',
          181 => 'modules/user/views_handler_field_user_picture.inc',
          182 => 'modules/user/views_handler_field_user_roles.inc',
          183 => 'modules/user/views_handler_filter_user_current.inc',
          184 => 'modules/user/views_handler_filter_user_name.inc',
          185 => 'modules/user/views_handler_filter_user_permissions.inc',
          186 => 'modules/user/views_handler_filter_user_roles.inc',
          187 => 'modules/user/views_plugin_argument_default_current_user.inc',
          188 => 'modules/user/views_plugin_argument_default_user.inc',
          189 => 'modules/user/views_plugin_argument_validate_user.inc',
          190 => 'modules/user/views_plugin_row_user_view.inc',
          191 => 'plugins/views_plugin_access.inc',
          192 => 'plugins/views_plugin_access_none.inc',
          193 => 'plugins/views_plugin_access_perm.inc',
          194 => 'plugins/views_plugin_access_role.inc',
          195 => 'plugins/views_plugin_argument_default.inc',
          196 => 'plugins/views_plugin_argument_default_php.inc',
          197 => 'plugins/views_plugin_argument_default_fixed.inc',
          198 => 'plugins/views_plugin_argument_default_raw.inc',
          199 => 'plugins/views_plugin_argument_validate.inc',
          200 => 'plugins/views_plugin_argument_validate_numeric.inc',
          201 => 'plugins/views_plugin_argument_validate_php.inc',
          202 => 'plugins/views_plugin_cache.inc',
          203 => 'plugins/views_plugin_cache_none.inc',
          204 => 'plugins/views_plugin_cache_time.inc',
          205 => 'plugins/views_plugin_display.inc',
          206 => 'plugins/views_plugin_display_attachment.inc',
          207 => 'plugins/views_plugin_display_block.inc',
          208 => 'plugins/views_plugin_display_default.inc',
          209 => 'plugins/views_plugin_display_embed.inc',
          210 => 'plugins/views_plugin_display_extender.inc',
          211 => 'plugins/views_plugin_display_feed.inc',
          212 => 'plugins/views_plugin_display_page.inc',
          213 => 'plugins/views_plugin_exposed_form_basic.inc',
          214 => 'plugins/views_plugin_exposed_form.inc',
          215 => 'plugins/views_plugin_exposed_form_input_required.inc',
          216 => 'plugins/views_plugin_localization_core.inc',
          217 => 'plugins/views_plugin_localization.inc',
          218 => 'plugins/views_plugin_localization_none.inc',
          219 => 'plugins/views_plugin_pager.inc',
          220 => 'plugins/views_plugin_pager_full.inc',
          221 => 'plugins/views_plugin_pager_mini.inc',
          222 => 'plugins/views_plugin_pager_none.inc',
          223 => 'plugins/views_plugin_pager_some.inc',
          224 => 'plugins/views_plugin_query.inc',
          225 => 'plugins/views_plugin_query_default.inc',
          226 => 'plugins/views_plugin_row.inc',
          227 => 'plugins/views_plugin_row_fields.inc',
          228 => 'plugins/views_plugin_row_rss_fields.inc',
          229 => 'plugins/views_plugin_style.inc',
          230 => 'plugins/views_plugin_style_default.inc',
          231 => 'plugins/views_plugin_style_grid.inc',
          232 => 'plugins/views_plugin_style_list.inc',
          233 => 'plugins/views_plugin_style_jump_menu.inc',
          234 => 'plugins/views_plugin_style_mapping.inc',
          235 => 'plugins/views_plugin_style_rss.inc',
          236 => 'plugins/views_plugin_style_summary.inc',
          237 => 'plugins/views_plugin_style_summary_jump_menu.inc',
          238 => 'plugins/views_plugin_style_summary_unformatted.inc',
          239 => 'plugins/views_plugin_style_table.inc',
          240 => 'tests/handlers/views_handler_area_text.test',
          241 => 'tests/handlers/views_handler_argument_null.test',
          242 => 'tests/handlers/views_handler_argument_string.test',
          243 => 'tests/handlers/views_handler_field.test',
          244 => 'tests/handlers/views_handler_field_boolean.test',
          245 => 'tests/handlers/views_handler_field_custom.test',
          246 => 'tests/handlers/views_handler_field_counter.test',
          247 => 'tests/handlers/views_handler_field_date.test',
          248 => 'tests/handlers/views_handler_field_file_extension.test',
          249 => 'tests/handlers/views_handler_field_file_size.test',
          250 => 'tests/handlers/views_handler_field_math.test',
          251 => 'tests/handlers/views_handler_field_url.test',
          252 => 'tests/handlers/views_handler_field_xss.test',
          253 => 'tests/handlers/views_handler_filter_combine.test',
          254 => 'tests/handlers/views_handler_filter_date.test',
          255 => 'tests/handlers/views_handler_filter_equality.test',
          256 => 'tests/handlers/views_handler_filter_in_operator.test',
          257 => 'tests/handlers/views_handler_filter_numeric.test',
          258 => 'tests/handlers/views_handler_filter_string.test',
          259 => 'tests/handlers/views_handler_sort_random.test',
          260 => 'tests/handlers/views_handler_sort_date.test',
          261 => 'tests/handlers/views_handler_sort.test',
          262 => 'tests/test_plugins/views_test_plugin_access_test_dynamic.inc',
          263 => 'tests/test_plugins/views_test_plugin_access_test_static.inc',
          264 => 'tests/test_plugins/views_test_plugin_style_test_mapping.inc',
          265 => 'tests/plugins/views_plugin_display.test',
          266 => 'tests/styles/views_plugin_style_jump_menu.test',
          267 => 'tests/styles/views_plugin_style.test',
          268 => 'tests/styles/views_plugin_style_base.test',
          269 => 'tests/styles/views_plugin_style_mapping.test',
          270 => 'tests/styles/views_plugin_style_unformatted.test',
          271 => 'tests/views_access.test',
          272 => 'tests/views_analyze.test',
          273 => 'tests/views_basic.test',
          274 => 'tests/views_argument_default.test',
          275 => 'tests/views_argument_validator.test',
          276 => 'tests/views_exposed_form.test',
          277 => 'tests/field/views_fieldapi.test',
          278 => 'tests/views_glossary.test',
          279 => 'tests/views_groupby.test',
          280 => 'tests/views_handlers.test',
          281 => 'tests/views_module.test',
          282 => 'tests/views_pager.test',
          283 => 'tests/views_plugin_localization_test.inc',
          284 => 'tests/views_translatable.test',
          285 => 'tests/views_query.test',
          286 => 'tests/views_upgrade.test',
          287 => 'tests/views_test.views_default.inc',
          288 => 'tests/comment/views_handler_argument_comment_user_uid.test',
          289 => 'tests/comment/views_handler_filter_comment_user_uid.test',
          290 => 'tests/node/views_node_revision_relations.test',
          291 => 'tests/taxonomy/views_handler_relationship_node_term_data.test',
          292 => 'tests/user/views_handler_field_user_name.test',
          293 => 'tests/user/views_user_argument_default.test',
          294 => 'tests/user/views_user_argument_validate.test',
          295 => 'tests/user/views_user.test',
          296 => 'tests/views_cache.test',
          297 => 'tests/views_view.test',
          298 => 'tests/views_ui.test',
        ),
        'version' => '7.x-3.7+10-dev',
        'project' => 'views',
        'datestamp' => '1373486873',
      ),
      'project' => 'views',
      'version' => '7.x-3.7+10-dev',
    ),
    'views_accordion' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/views_accordion/views_accordion.module',
      'name' => 'views_accordion',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views Accordion',
        'description' => 'Provides an accordion views display plugin.',
        'package' => 'Views',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'views',
        ),
        'files' => 
        array (
          0 => 'views_accordion.views.inc',
          1 => 'views_accordion_style_plugin.inc',
        ),
        'version' => '7.x-1.0-rc2',
        'project' => 'views_accordion',
        'datestamp' => '1342687617',
        'php' => '5.2.4',
      ),
      'project' => 'views_accordion',
      'version' => '7.x-1.0-rc2',
    ),
    'views_boxes' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/views_boxes/views_boxes.module',
      'name' => 'views_boxes',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views Boxes',
        'description' => 'Provides configurable views through boxes.',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'ctools',
          1 => 'boxes',
          2 => 'views',
          3 => 'entity_autocomplete',
        ),
        'files' => 
        array (
          0 => 'views_boxes.module',
          1 => 'plugins/views_boxes_context_list.inc',
          2 => 'handlers/views_boxes_handler_argument_limit.inc',
          3 => 'handlers/views_boxes_handler_filter_sort.inc',
        ),
        'version' => '7.x-1.0-beta8',
        'project' => 'views_boxes',
        'datestamp' => '1329171345',
        'php' => '5.2.4',
      ),
      'project' => 'views_boxes',
      'version' => '7.x-1.0-beta8',
    ),
    'views_bulk_operations' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/views_bulk_operations/views_bulk_operations.module',
      'name' => 'views_bulk_operations',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views Bulk Operations',
        'description' => 'Provides a way of selecting multiple rows and applying operations to them.',
        'dependencies' => 
        array (
          0 => 'entity',
          1 => 'views',
        ),
        'package' => 'Views',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'plugins/operation_types/base.class.php',
          1 => 'views/views_bulk_operations_handler_field_operations.inc',
        ),
        'version' => '7.x-3.1',
        'project' => 'views_bulk_operations',
        'datestamp' => '1354500015',
        'php' => '5.2.4',
      ),
      'project' => 'views_bulk_operations',
      'version' => '7.x-3.1',
    ),
    'views_content' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib-patched/ctools/views_content/views_content.module',
      'name' => 'views_content',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views content panes',
        'description' => 'Allows Views content to be used in Panels, Dashboard and other modules which use the CTools Content API.',
        'package' => 'Chaos tool suite',
        'dependencies' => 
        array (
          0 => 'ctools',
          1 => 'views',
        ),
        'core' => '7.x',
        'files' => 
        array (
          0 => 'plugins/views/views_content_plugin_display_ctools_context.inc',
          1 => 'plugins/views/views_content_plugin_display_panel_pane.inc',
          2 => 'plugins/views/views_content_plugin_style_ctools_context.inc',
        ),
        'version' => '7.x-1.3',
        'project' => 'ctools',
        'datestamp' => '1365013512',
        'php' => '5.2.4',
      ),
      'project' => 'ctools',
      'version' => '7.x-1.3',
    ),
    'views_feature' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/features/views_feature/views_feature.module',
      'name' => 'views_feature',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views Feature',
        'description' => 'Installs basic Views elements and permissions.',
        'core' => '7.x',
        'package' => 'Features',
        'php' => '5.2.4',
        'version' => '7.x-1.0-beta2',
        'project' => 'views_feature',
        'dependencies' => 
        array (
          0 => 'features',
          1 => 'views',
          2 => 'views_ui',
        ),
        'features' => 
        array (
          'features_api' => 
          array (
            0 => 'api:2',
          ),
          'user_permission' => 
          array (
            0 => 'access all views',
            1 => 'administer views',
          ),
        ),
        'project status url' => 'http://features.devmcn.com/fserver',
      ),
      'project' => 'views_feature',
      'version' => '7.x-1.0-beta2',
    ),
    'views_galleriffic' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/views_galleriffic/views_galleriffic.module',
      'name' => 'views_galleriffic',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views Galleriffic',
        'description' => 'Use Views to make JQuery Galleriffic image gallery',
        'core' => '7.x',
        'package' => 'Views',
        'dependencies' => 
        array (
          0 => 'views',
        ),
        'files' => 
        array (
          0 => 'views_plugin_style_views_galleriffic.inc',
          1 => 'views_galleriffic_plugin_row_gallerifficrows.inc',
        ),
        'version' => '7.x-1.1',
        'project' => 'views_galleriffic',
        'datestamp' => '1337097692',
        'php' => '5.2.4',
      ),
      'project' => 'views_galleriffic',
      'version' => '7.x-1.1',
    ),
    'views_slideshow' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/views_slideshow/views_slideshow.module',
      'name' => 'views_slideshow',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views Slideshow',
        'description' => 'Provides a View style that displays rows as a jQuery slideshow.  This is an API and requires Views Slideshow Cycle or another module that supports the API.',
        'dependencies' => 
        array (
          0 => 'views',
        ),
        'package' => 'Views',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'views_slideshow.module',
          1 => 'theme/views_slideshow.theme.inc',
          2 => 'views_slideshow.views.inc',
          3 => 'views_slideshow_plugin_style_slideshow.inc',
        ),
        'version' => '7.x-3.0',
        'project' => 'views_slideshow',
        'datestamp' => '1319589616',
        'php' => '5.2.4',
      ),
      'project' => 'views_slideshow',
      'version' => '7.x-3.0',
    ),
    'views_slideshow_cycle' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/views_slideshow/contrib/views_slideshow_cycle/views_slideshow_cycle.module',
      'name' => 'views_slideshow_cycle',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views Slideshow: Cycle',
        'description' => 'Adds a Rotating slideshow mode to Views Slideshow.',
        'dependencies' => 
        array (
          0 => 'views_slideshow',
          1 => 'libraries',
        ),
        'package' => 'Views',
        'core' => '7.x',
        'files' => 
        array (
          0 => 'views_slideshow_cycle.module',
          1 => 'views_slideshow_cycle.views_slideshow.inc',
          2 => 'theme/views_slideshow_cycle.theme.inc',
        ),
        'version' => '7.x-3.0',
        'project' => 'views_slideshow',
        'datestamp' => '1319589616',
        'php' => '5.2.4',
      ),
      'project' => 'views_slideshow',
      'version' => '7.x-3.0',
    ),
    'views_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/views/tests/views_test.module',
      'name' => 'views_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views Test',
        'description' => 'Test module for Views.',
        'package' => 'Views',
        'core' => '7.x',
        'dependencies' => 
        array (
          0 => 'views',
        ),
        'hidden' => true,
        'version' => '7.x-3.7+10-dev',
        'project' => 'views',
        'datestamp' => '1373486873',
        'php' => '5.2.4',
      ),
      'project' => 'views',
      'version' => '7.x-3.7+10-dev',
    ),
    'views_ui' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/views/views_ui.module',
      'name' => 'views_ui',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Views UI',
        'description' => 'Administrative interface to views. Without this module, you cannot create or edit your views.',
        'package' => 'Views',
        'core' => '7.x',
        'configure' => 'admin/structure/views',
        'dependencies' => 
        array (
          0 => 'views',
        ),
        'files' => 
        array (
          0 => 'views_ui.module',
          1 => 'plugins/views_wizard/views_ui_base_views_wizard.class.php',
        ),
        'version' => '7.x-3.7+10-dev',
        'project' => 'views',
        'datestamp' => '1373486873',
        'php' => '5.2.4',
      ),
      'project' => 'views',
      'version' => '7.x-3.7+10-dev',
    ),
    'webform' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/webform/webform.module',
      'name' => 'webform',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Webform',
        'description' => 'Enables the creation of forms and questionnaires.',
        'core' => '7.x',
        'package' => 'Webform',
        'configure' => 'admin/config/content/webform',
        'files' => 
        array (
          0 => 'includes/webform.export.inc',
          1 => 'views/webform_handler_field_form_body.inc',
          2 => 'views/webform_handler_field_is_draft.inc',
          3 => 'views/webform_handler_field_node_link_edit.inc',
          4 => 'views/webform_handler_field_node_link_results.inc',
          5 => 'views/webform_handler_field_submission_count.inc',
          6 => 'views/webform_handler_field_submission_link.inc',
          7 => 'views/webform_handler_field_webform_status.inc',
          8 => 'views/webform_handler_filter_is_draft.inc',
          9 => 'views/webform_handler_filter_webform_status.inc',
          10 => 'views/webform.views.inc',
          11 => 'tests/components.test',
          12 => 'tests/permissions.test',
          13 => 'tests/submission.test',
          14 => 'tests/webform.test',
        ),
        'version' => '7.x-3.19',
        'project' => 'webform',
        'datestamp' => '1369860079',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'webform',
      'version' => '7.x-3.19',
    ),
    'wysiwyg' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/wysiwyg/wysiwyg.module',
      'name' => 'wysiwyg',
      'type' => 'module',
      'owner' => '',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => '7200',
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Wysiwyg',
        'description' => 'Allows to edit content with client-side editors.',
        'package' => 'User interface',
        'core' => '7.x',
        'configure' => 'admin/config/content/wysiwyg',
        'files' => 
        array (
          0 => 'wysiwyg.module',
          1 => 'tests/wysiwyg.test',
        ),
        'version' => '7.x-2.2',
        'project' => 'wysiwyg',
        'datestamp' => '1349213776',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'wysiwyg',
      'version' => '7.x-2.2',
    ),
    'wysiwyg_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/sites/all/modules/contrib/wysiwyg/tests/wysiwyg_test.module',
      'name' => 'wysiwyg_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Wysiwyg testing',
        'description' => 'Tests Wysiwyg module functionality. Do not enable.',
        'core' => '7.x',
        'package' => 'Testing',
        'hidden' => true,
        'dependencies' => 
        array (
          0 => 'wysiwyg',
        ),
        'files' => 
        array (
          0 => 'wysiwyg_test.module',
        ),
        'version' => '7.x-2.2',
        'project' => 'wysiwyg',
        'datestamp' => '1349213776',
        'php' => '5.2.4',
      ),
      'project' => 'wysiwyg',
      'version' => '7.x-2.2',
    ),
    'xmlrpc_test' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/modules/simpletest/tests/xmlrpc_test.module',
      'name' => 'xmlrpc_test',
      'type' => 'module',
      'owner' => '',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'XML-RPC Test',
        'description' => 'Support module for XML-RPC tests according to the validator1 specification.',
        'package' => 'Testing',
        'version' => '7.23',
        'core' => '7.x',
        'hidden' => true,
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
  ),
  'themes' => 
  array (
    'bartik' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/themes/bartik/bartik.info',
      'name' => 'bartik',
      'type' => 'theme',
      'owner' => 'themes/engines/phptemplate/phptemplate.engine',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Bartik',
        'description' => 'A flexible, recolorable theme with many regions.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'css/layout.css',
            1 => 'css/style.css',
            2 => 'css/colors.css',
          ),
          'print' => 
          array (
            0 => 'css/print.css',
          ),
        ),
        'regions' => 
        array (
          'header' => 'Header',
          'help' => 'Help',
          'page_top' => 'Page top',
          'page_bottom' => 'Page bottom',
          'highlighted' => 'Highlighted',
          'featured' => 'Featured',
          'content' => 'Content',
          'sidebar_first' => 'Sidebar first',
          'sidebar_second' => 'Sidebar second',
          'triptych_first' => 'Triptych first',
          'triptych_middle' => 'Triptych middle',
          'triptych_last' => 'Triptych last',
          'footer_firstcolumn' => 'Footer first column',
          'footer_secondcolumn' => 'Footer second column',
          'footer_thirdcolumn' => 'Footer third column',
          'footer_fourthcolumn' => 'Footer fourth column',
          'footer' => 'Footer',
        ),
        'settings' => 
        array (
          'shortcut_module_link' => '0',
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'garland' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/themes/garland/garland.info',
      'name' => 'garland',
      'type' => 'theme',
      'owner' => 'themes/engines/phptemplate/phptemplate.engine',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Garland',
        'description' => 'A multi-column theme which can be configured to modify colors and switch between fixed and fluid width layouts.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'style.css',
          ),
          'print' => 
          array (
            0 => 'print.css',
          ),
        ),
        'settings' => 
        array (
          'garland_width' => 'fluid',
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'seven' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/themes/seven/seven.info',
      'name' => 'seven',
      'type' => 'theme',
      'owner' => 'themes/engines/phptemplate/phptemplate.engine',
      'status' => '1',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Seven',
        'description' => 'A simple one-column, tableless, fluid width administration theme.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'stylesheets' => 
        array (
          'screen' => 
          array (
            0 => 'reset.css',
            1 => 'style.css',
          ),
        ),
        'settings' => 
        array (
          'shortcut_module_link' => '1',
        ),
        'regions' => 
        array (
          'content' => 'Content',
          'help' => 'Help',
          'page_top' => 'Page top',
          'page_bottom' => 'Page bottom',
          'sidebar_first' => 'First sidebar',
        ),
        'regions_hidden' => 
        array (
          0 => 'sidebar_first',
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
    'stark' => 
    array (
      'filename' => '/data/aegir/platforms/base-130814/themes/stark/stark.info',
      'name' => 'stark',
      'type' => 'theme',
      'owner' => 'themes/engines/phptemplate/phptemplate.engine',
      'status' => '0',
      'bootstrap' => '0',
      'schema_version' => 0,
      'weight' => '0',
      'info' => 
      array (
        'name' => 'Stark',
        'description' => 'This theme demonstrates Drupal\'s default HTML markup and CSS styles. To learn how to build your own theme and override Drupal\'s default code, see the <a href="http://drupal.org/theme-guide">Theming Guide</a>.',
        'package' => 'Core',
        'version' => '7.23',
        'core' => '7.x',
        'stylesheets' => 
        array (
          'all' => 
          array (
            0 => 'layout.css',
          ),
        ),
        'project' => 'drupal',
        'datestamp' => '1375928238',
        'dependencies' => 
        array (
        ),
        'php' => '5.2.4',
      ),
      'project' => 'drupal',
      'version' => '7.23',
    ),
  ),
);
$options['redirection'] = '1';
$options['ssl'] = '0';

$_SERVER['db_type'] = 'mysql';
$_SERVER['db_host'] = 'vs27';
$_SERVER['db_user'] = 'site_3180';
$_SERVER['db_passwd'] = '5ycd9DJyCA';
$_SERVER['db_name'] = 'site_3180';
